#include "TLatex.h"
#include "TLine.h"

void ATLAS_LABEL(Double_t x,Double_t y,Color_t color=1) {
  // plots ATLAS label
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);
  l.DrawLatex(x,y,"ATLAS");
}


void myText(Double_t x,Double_t y,Color_t color,const char *text,Double_t tsize=0.06) {
  // plot a Text
  //
  //Double_t tsize=0.05;
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}
 

void myBoxText(Double_t x, Double_t y,Double_t boxsize,Int_t mcolor,const char *text,Int_t lcol=1,Int_t lstyle=1,
	       Double_t tsize=0.05) {
   // plot a Box and a Text
   //
  const char *name="**myBoxText";  
  const bool debug=false;
  //Double_t tsize=0.06;

  TLatex l; l.SetTextAlign(12); //l.SetTextSize(tsize); 
  l.SetNDC(); l.SetTextSize(tsize);
  l.DrawLatex(x,y,text);

  Double_t y1=y-boxsize/2.;
  Double_t y2=y+boxsize/2.;
  Double_t x2=x-0.3*tsize;
  Double_t x1=x2-boxsize;

  if (debug) printf("%s x1= %f x2= %f y1= %f y2= %f \n",name,x1,x2,y1,y2);

  TPave *mbox= new TPave(x1,y1,x2,y2,0,"NDC");

  mbox->SetFillColor(mcolor);
  mbox->SetFillStyle(1001);
  mbox->Draw();

  TLine mline;
  mline.SetLineWidth(2);
  mline.SetLineColor(lcol);
  mline.SetLineStyle(lstyle);
  //Double_t ym=(y1+y2)/2.;
  //mline.DrawLineNDC(x1,y,x2,ym);

}

