#include<stdio.h>
#include<iostream>
#include<sstream>
#include "Riostream.h"
#include "TROOT.h"
#include<TH1.h>
#include<TH2F.h>
#include<TCanvas.h>
#include<math.h>
#include<TGraph.h>
#include<TFile.h>
#include<TKey.h>
#include<TObjString.h>
#include<TGraphErrors.h>
#include<TVectorD.h>
#include<TString.h>
#include<iostream>
#include<TStyle.h>
#include<TLegend.h>
#include<TColor.h>


#include "utils.h"
#include "AtlasStyle.C"
#include "AtlasLabels.C"

   // checks if file with name "fin" is already open, and if not opens one
   TFile* OpenFile( const TString& fin )
   {
      TFile* file = gDirectory->GetFile();
      if (file==0 || fin != file->GetName()) {
         if (file != 0) {
            gROOT->cd();
            file->Close();
         }
         cout << "--- Opening root file \"" << fin << "\" in read mode" << endl;
         file = TFile::Open( fin, "READ" );
      }
      else {
         file = gDirectory->GetFile();
      }

      file->cd();
      return file;
   }

UInt_t NC = 1;
TCanvas* c[30];
TLegend* legend[30];

void PlotLabelEnergyLumi( Double_t x, Double_t y ){

   ATLASLabel( x, y,1,kBlack);
   myText( x, y-0.06,kBlack,"#sqrt{s} = 13 TeV, 27 fb^{-1}",0.05);

}

// plot the auto-analysis chi2 graph of the ComplexMerger

void chi2Distribution( TString EM_LC_  = "LC" , TString R_  = "4", Double_t Ymin = 0., Double_t Ymax = 4.0, Double_t Emin = 17., Double_t Emax = 2160. ){

   SetAtlasStyle();

   TString EM_LCW_;
   if( EM_LC_.Contains("LC") ){
      EM_LCW_ = EM_LC_ + "W";
   }
   else{
      EM_LCW_ = EM_LC_;
   }

   TString channelName = EM_LCW_ + "JES_R" + R_ + "_points";
   TString fin = "../../../exe/ResultsCombined_JES/Results_2016/testapp_" + EM_LC_ + "JES_R" + R_ + "_MPF_ZJ-DB_GJ_MJB" + /*_MJB*/ "_TSpline2_100toys_WithSplineExt_Bins_MaxSystNoShift_WeightsNoCorrel.root";

  // checks if file with name "fin" is already open, and if not opens one
  TFile* file = OpenFile( fin );

  // check if exists
  TDirectory* channelDir = (TDirectory*)file->Get( channelName );
  if (!channelDir) {
    cout << "*** Could not locate directory: \"" << channelName << "\"" 
         << " in ROOT file \"" << fin << "\"" << endl;
    return;
  }
  TString channelTitle     = channelDir->GetTitle();
  cout << "--- New channel title: \"" << channelTitle << "\"" << endl;

  // move to directory
  channelDir->cd();

   // create canvas
  c[NC] = new TCanvas( Form( "c%i",NC ), "Chi2 plot", 0 + NC*30, 0 + NC*20, 700, 500 ); 

  // get auto-analysis chi2 graph created by the ComplexMerger
  TString anName = "Chi2Plot";
  cout << "--- looking for graph: " << anName << endl;
  TGraph* graph = (TGraph*)(gDirectory->Get( anName ));
  if (graph) {
    cout << "--- found graph: " << anName << endl;
    // now plot ... build the frame first
    Int_t nb = 500;

    TH2F* frame = new TH2F( channelName, channelTitle, nb, /*graph->GetXaxis()->GetXmin()*/ Emin, 
                            /*graph->GetXaxis()->GetXmax()*/ Emax, nb, Ymin, /*graph->GetYaxis()->GetXmax()*/ Ymax );
    frame->GetXaxis()->SetTitle( "p_{T}^{jet} [GeV]" );
    frame->GetYaxis()->SetTitle( "#sqrt{#chi^{2}/Ndof}" );
    frame->GetXaxis()->SetMoreLogLabels();
    frame->Draw(); 

    frame->GetYaxis()->SetTitleOffset( 1.1 );

    // correct canvas margins
    
    //    c[NC]->SetLeftMargin  ( 0.140 );
    //c[NC]->SetRightMargin ( 0.025 );
    //c[NC]->SetBottomMargin( 0.150 );
    // c[NC]->SetTopMargin( 0.015 );
    c[NC]->SetLogx();

    graph->Draw("same");  
  }

  TLine* line = new TLine(Emin,1,Emax,1);
  line->SetLineColor( TColor::GetColor( "#aaaaaa" ) );
  line->SetLineWidth(1);
  line->Draw();

   myText(0.18,0.9,1,"anti-k_{t} R=0." + R_ + ", " + EM_LC_ + "+JES",0.05);
   myText(0.18,0.83,1,"Data 2016",0.05);
   PlotLabelEnergyLumi( 0.62, 0.9 );
   c[NC]->Print( "./Results_" + EM_LC_ + "JES_R" + R_ + "_MPF_ZJ_DB_GJ_MJB" + /*_MJB*/ "_12_18_TSpline2_100toys_WithSplineExt_NuisanceParam_Bins/Plots/chi2distribution_" + EM_LC_ + "JES_R" + R_ + ".eps", "eps" );

}
