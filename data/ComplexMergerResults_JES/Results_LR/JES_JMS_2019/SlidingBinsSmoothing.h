
void EvaluateSignificance( TH1D *responseH_, TMatrixD *covMatTot_, TMatrixD* &significanceMatrix_,  Double_t minRespDiff_, TMatrixD* &significanceMatrixLD_   /*Double_t maxSignificance_, TH1D* &maxDistLowH_, TH1D* &maxDistHighH_*/ ){

   if( responseH_->GetNbinsX()!=covMatTot_->GetNrows() || responseH_->GetNbinsX()!=covMatTot_->GetNcols() ){
      cout << "Problem in the size of the entries !!!" << endl;
      exit(1);
   }

   significanceMatrix_ = new TMatrixD( *covMatTot_ );
   significanceMatrixLD_ = new TMatrixD( *covMatTot_ );
   for( Int_t i=0; i<responseH_->GetNbinsX(); i++ ){
      for( Int_t j=0; j<responseH_->GetNbinsX(); j++ ){
         if( (*covMatTot_)[i][i] + (*covMatTot_)[j][j] - 2*(*covMatTot_)[i][j] != 0. ){
            (*significanceMatrix_)[i][j] = fabs( responseH_->GetBinContent(i+1) - responseH_->GetBinContent(j+1) ) / sqrt( (*covMatTot_)[i][i] + (*covMatTot_)[j][j] - 2*(*covMatTot_)[i][j] );
         }
         else{
            (*significanceMatrix_)[i][j] = 0.;
         }

         if( fabs(responseH_->GetBinContent(i+1) - responseH_->GetBinContent(j+1)) > minRespDiff_ ){
            (*significanceMatrixLD_)[i][j] = (*significanceMatrix_)[i][j];
         }
         else{
            (*significanceMatrixLD_)[i][j] = 0.;
         }
      }
   }

   /*
   maxDistLowH_ = new TH1D(*responseH_);
   maxDistHighH_ = new TH1D(*responseH_);
   for( Int_t i=0; i<responseH_->GetNbinsX(); i++ ){
      maxDistLowH_->SetBinContent( i+1, 0. );
      maxDistHighH_->SetBinContent( i+1, 0. );
      for( Int_t j=i-1; j>=0; j-- ){
         if( (*significanceMatrix_)[i][j] > maxSignificance_ ){ break; }
         maxDistLowH_->SetBinContent( i+1, responseH_->GetBinCenter(i+1)-responseH_->GetBinCenter(j+1) );
      }
      for( Int_t j=i+1; j<responseH_->GetNbinsX(); j++ ){
         if( (*significanceMatrix_)[i][j] > maxSignificance_ ){ break; }
         maxDistHighH_->SetBinContent( i+1, responseH_->GetBinCenter(j+1)-responseH_->GetBinCenter(i+1) );
      }
   }
   */
}

void IncrementWeightedSum( TH1D *systH_, TMatrixD *significanceMatrix_, Double_t a_, Int_t i, Int_t j, Double_t &dji_, Double_t &weight ){

   dji_ += (*significanceMatrix_)[i][j] * systH_->GetBinWidth(j+1) * fabs(systH_->GetBinCenter(j+1) - systH_->GetBinCenter(i+1)) / pow( systH_->GetBinCenter(systH_->GetNbinsX()) - systH_->GetBinCenter(1), 2 );

   weight = exp(-pow( a_ * dji_, 2 )) * systH_->GetBinWidth(j+1);
}
TH1D* SignificanceSmoothing( TH1D *systH_, TMatrixD *significanceMatrix_, Int_t version_ ){

   cout << " Sliding bins over " << systH_->GetNbinsX() << " small bins " << endl;

   Double_t a=0.;
   if( version_ == 1 ){
      a = 1000000.;
   }
   else{
      if( version_ == 2 ){
         a = 1.;
      }
      else{
         if( version_ == 3 ){
            a = 50.;
         }
         else{
            cout << "Problem in smoothing version!" << endl;
            exit(1);
         }
      }
   }

   TH1D *systSmoothH_ = new TH1D( *systH_ );
   for( Int_t i=0; i<systH_->GetNbinsX(); i++ ){
      Double_t weight = 0., sum = 0., totWeightAv = 0.;

      Double_t dji = 0.;
      for( Int_t j=i; j>=0; j-- ){
         IncrementWeightedSum( systH_, significanceMatrix_, a, i, j, dji, weight );
         sum += systH_->GetBinContent( j+1 ) * weight;
         totWeightAv += weight;
      }
      dji = 0.;
      for( Int_t j=i+1; j<systH_->GetNbinsX(); j++ ){
         IncrementWeightedSum( systH_, significanceMatrix_, a, i, j, dji, weight );
         sum += systH_->GetBinContent( j+1 ) * weight;
         totWeightAv += weight;
      }
      if( totWeightAv != 0. ){ systSmoothH_->SetBinContent( i+1, sum/totWeightAv ); }
   }

   return systSmoothH_;
}

/*
// this method introduced discontinuities due to fast variations in the "maxDist" histograms
TH1D* SignificanceSmoothing( TH1D *systH_, TH1D* maxDistLowH_, TH1D* maxDistHighH_, Int_t version_ ){

   cout << " Sliding bins over " << systH_->GetNbinsX() << " small bins " << endl;

   Double_t a=0.;
   if( version_ == 1 ){
      a = 0.3;
   }
   else{
      if( version_ == 2 ){
         a = 0.5;
      }
      else{
         if( version_ == 3 ){
            a = 1.;
         }
         else{
            cout << "Problem in smoothing version!" << endl;
            exit(1);
         }
      }
   }

   TH1D *systSmoothH_ = new TH1D( *systH_ );
   for( Int_t i=0; i<systSmoothH_->GetNbinsX(); i++ ){
      Double_t sum = 0., xi = 0.;
      xi = systH_->GetBinCenter( i+1 );
      Double_t totWeightAv = 0;

      for( Int_t j=0; j <systSmoothH_->GetNbinsX(); j++ ){
         Double_t xp = 0., yp = 0.;
         xp = systH_->GetBinCenter( j+1 );
         yp = systH_->GetBinContent( j+1 );

         Double_t maxDist = ( (xp<xi) ? maxDistLowH_->GetBinContent(i+1) : maxDistHighH_->GetBinContent(i+1) );
         Double_t width = a * maxDist;

         Double_t weight = 0;
         if( fabs(xp-xi) < maxDist  &&  width != 0. ){
            weight = exp( -pow( (xp-xi)/width, 2 ) / 2. );
         }
         sum += yp * weight;
         totWeightAv += weight;
      }
      if( totWeightAv != 0. ){ systSmoothH_->SetBinContent( i+1, sum/totWeightAv ); }
   }

   return systSmoothH_;
}
*/

// perform average in sliding bins: [ i-nSBm1d2; i+nSBm1d2 ] 
// perform average with Gaussian kernel
TH1D* SlidingBinsSmoothing( TH1D *systH_, Int_t version_ ){

   cout << " Sliding bins over " << systH_->GetNbinsX() << " small bins " << endl;
   Double_t xMin = 17.5;

   Double_t a=0., b=0.;
   if( version_ == 1 ){
      //      a = 15.; b = 0.;
      a = 20.; b = 0.80;
   }
   else{
      if( version_ == 2 ){
         a = 20.; b = 0.60;
      }
      else{
         if( version_ == 3 ){
            a = 15.; b = 0.2;
         }
         else{
            cout << "Problem in smoothing version!" << endl;
            exit(1);
         }
      }
   }

   TH1D *systSmoothH_ = new TH1D( *systH_ );
   for( Int_t i=0; i<systSmoothH_->GetNbinsX(); i++ ){
      Double_t sum = 0., xi = 0.;
      xi = systH_->GetBinCenter( i+1 );
      Double_t totWeightAv = 0;

      Double_t width = a + b*xi;  // 15 + 0.*xi;  // 20 + 0.60*xi;  // 15 + 0.2*xi;
      //      Double_t width = 4 + 0.25*xi; // 0.25; 0.45; 0.65; 0.95

      for( Int_t j=0; j <systSmoothH_->GetNbinsX(); j++ ){
         Double_t xp = 0., yp = 0.;
         xp = systH_->GetBinCenter( j+1 );
         yp = systH_->GetBinContent( j+1 );

         Double_t weight = exp( -pow( (xp-xi)/(0.5*width), 2 ) );
         sum += ( ( /* yp>1e-5 && */ xp>xMin) ? yp : systH_->GetBinContent( i+1 ) ) *weight;
         totWeightAv += weight;
      }
      if( totWeightAv != 0. ){ systSmoothH_->SetBinContent( i+1, sum/totWeightAv ); }
   }

   return systSmoothH_;
}

