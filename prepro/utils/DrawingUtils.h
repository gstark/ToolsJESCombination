#include <TCanvas.h>
#include <TLatex.h>
#include <TMarker.h>
#include <TLine.h>
#include <TGraphErrors.h>
#include <TGraph2DErrors.h>

typedef TGraphErrors Graph;
typedef TGraph2DErrors Graph2D;

double TEXTSIZE=0.04;
double FIGURE2_RATIO = 0.36;
double SUBFIGURE_MARGIN = 0.06; // white space between figures
double _maxDev=0.25;

int cols[] = { kBlack, kRed, kBlue, kViolet+2, kGreen+1, kOrange+7, kAzure-1, kGray+1};
int markers[] = {  20,  25,  23,  26,  33,  28,  24 };
double ms[]   = { 0.8, 0.6, 0.8, 1.0, 1.6, 0.8, 1.2 };

TLatex *_tex;
TCanvas *_can;

int _jetR;
Str _calibration, _methodDesc, _name;


// min/max of the histos
double FIGURE1MIN=0.775;
double FIGURE1MAX=1.2;
double FIGURE2MAX=1.2;
double FIGURE2MIN=0.8;

// Helper functions
TFile *Open(Str fn) { TFile *f=TFile::Open(fn); if (f==NULL) error("Can't open "+fn); return f; } 
void FormatHisto(TH1 *h, int mstyle, int col, double msize, Str xtit="", Str ytit="");
void FormatLineHisto(TH1 *h, int col, int line) { h->SetLineColor(col); h->SetLineStyle(line); }
void FormatHisto(TH1 *h, int style, Str xtit="", Str ytit="");
void FormatGraph(Graph *h, int mstyle, int col, double msize);
void FormatGraph(Graph *h, int style);

void PrintEtaBinInfo(double ptmin, double ptmax);
void DrawText(TString txt, int col=kBlack, double y=0.88, double x=0.145, double size = -1);
void DrawTextR(TString txt, int col=kBlack, double y=0.88, double x=0.92);
void DrawLabel(TString txt, int style, double x, double y);
void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize);
void DrawLineLabel(TString txt, double x, double y, int lstyle, int col, double size = -1);

void FormatHisto(TH1 *h, int style, Str xtit, Str ytit)
{
  int s=style%7;
  FormatHisto(h,markers[s],cols[s],ms[s],xtit,ytit);
  h->GetXaxis()->SetTitleOffset(0.95); h->GetYaxis()->SetTitleOffset(1.2); 
  //h->GetYaxis()->SetNdivisions(505);
  h->GetXaxis()->SetMoreLogLabels();
}

void FormatHisto(TH1 *h, int mstyle, int col, double msize, Str xtit, Str ytit) {
  //h->SetMinimum(FIGURE1MIN); h->SetMaximum(FIGURE1MAX);
  h->SetMarkerStyle(mstyle); h->SetMarkerSize(msize); h->SetLineStyle(1);
  h->SetMarkerColor(col); h->SetLineColor(col);
  if (xtit!="") h->SetXTitle(xtit);
  if (ytit!="") h->SetYTitle(ytit);
}

void FormatGraph(Graph *h, int style)
{
  int s=style%7;
  FormatGraph(h,markers[s],cols[s],ms[s]);
  /*
  if      (style==1) FormatGraph(h,20,kBlack,0.8); // data
  else if (style==2) FormatGraph(h,21,kRed,0.6);
  else if (style==3) FormatGraph(h,23,kBlue,0.8);
  else if (style==4) FormatGraph(h,26,kOrange+7,1.2);
  else if (style==5) FormatGraph(h,33,kGreen+2,1.6);
  else if (style==6) FormatGraph(h,28,kGray+2,1.2);
  else if (style==10) FormatGraph(h,24,kBlack,1.2);
  else if (style==11) FormatGraph(h,24,kBlack,1.2);
  else if (style==12) FormatGraph(h,25,kRed,0.8);
  else if (style==13) FormatGraph(h,27,kBlue,0.8);
  */
}

void FormatGraph(Graph *h, int mstyle, int col, double msize) {
  h->SetMarkerStyle(mstyle); h->SetMarkerSize(msize);
  h->SetMarkerColor(col); h->SetLineColor(col); h->SetLineWidth(2);
}

void PrintInfo() {
  DrawText(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration+", "+_methodDesc,kBlack,0.97,0.12);
  DrawTextR(_name,kBlack,0.97);
}

void PrintEtaBinInfo(double ptmin, double ptmax) {
  DrawText(Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),1,0.875);
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration,kBlack,0.93);
  DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

void PrintPtBinInfo(double etamin, double etamax) {
  DrawText(Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),1,0.875);
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration,kBlack,0.93);
  DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

// Draw text to the left
void DrawText(TString txt, int col, double y, double x, double size)
{ static TLatex *tex = new TLatex(); tex->SetNDC();
    if (size > 0) tex->SetTextSize(size);
    else tex->SetTextSize(0.04);
  tex->SetTextAlign(11); tex->SetTextColor(col); tex->DrawLatex(x,y,txt); }

// Draw text in the upper right corner
void DrawTextR(TString txt, int col, double y, double x)
{ static TLatex *tex = new TLatex(); tex->SetNDC(); tex->SetTextSize(0.04);
  tex->SetTextAlign(31); tex->SetTextColor(col); tex->DrawLatex(x,y,txt); 
  tex->SetTextAlign(11); tex->SetTextColor(kBlack); }

void DrawGuideLineSyst(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2); 
  line->SetLineWidth(1); line->SetLineColor(kRed);
  line->DrawLine(min,0.05,max,0.05);
  line->SetLineColor(kBlue);
  line->DrawLine(min,0.02,max,0.02);
}

void DrawGuideLines(double min, double max) {
  static TLine *line = new TLine();
  // Draw "guide-lines"
  line->SetLineStyle(2); 
  line->SetLineWidth(1);
  line->SetLineColor(kGray+1);
  line->DrawLine(-1,0.95,-1,1.05);
  line->DrawLine(1,0.95,1,1.05);

  line->SetLineWidth(1); line->SetLineColor(kBlack);
  line->DrawLine(min,1.0,max,1.0);
  line->SetLineWidth(1);
  line->SetLineColor(kRed);
  line->DrawLine(min,1.05,max,1.05);
  line->DrawLine(min,0.95,max,0.95);

  line->SetLineColor(kBlue);
  line->DrawLine(min,1.02,max,1.02);
  line->DrawLine(min,0.98,max,0.98);
}

void DrawCenterLine(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2); line->SetLineWidth(1);
  line->SetLineColor(kBlack);
  line->DrawLine(min,1.0,max,1.0);
}

void DrawGuideLinesVsEta(double etaMax=-4.5) {
  DrawGuideLines(-etaMax,etaMax);
}

void DrawGuideLinesVsPt() {
  DrawGuideLines(22,1500);
}

void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize) {
  TMarker *m = new TMarker(x,y,mstyle);
  m->SetNDC(); m->SetMarkerSize(msize); m->SetMarkerColor(col);
  TLine *l = new TLine(); l->SetLineWidth(2); l->SetLineColor(col);
  l->DrawLineNDC(x-0.02,y,x-0.005,y); l->DrawLineNDC(x+0.005,y,x+0.02,y); m->Draw();
  _tex->SetTextSize(0.04); _tex->SetTextAlign(12); _tex->SetTextColor(col);
  _tex->DrawLatex(x+0.04,y,txt);
  _tex->SetTextSize(TEXTSIZE);
}

void DrawLineLabel(TString txt, double x, double y, int lstyle, int col, double size) {
  TLine *l = new TLine(); l->SetLineWidth(2); l->SetLineColor(col); l->SetLineStyle(lstyle);
  l->DrawLineNDC(x-0.02,y+0.01,x+0.02,y+0.01);
  DrawText(txt,kBlack,y,x+0.04, size);
}


void DrawLabel(TString txt, int style, double x, double y) {
  if (style==1) DrawLabel(txt,x,y,20,kBlack,1.2);
  else if (style==2) DrawLabel(txt,x,y,21,kRed,1.0);
  else if (style==3) DrawLabel(txt,x,y,23,kBlue,1.2);
  else if (style==4) DrawLabel(txt,x,y,26,kOrange+7,1.2);
  else if (style==5) DrawLabel(txt,x,y,33,kGreen+2,1.6);
  else if (style==6) DrawLabel(txt,x,y,28,kGray+2,1.2);
  else if (style==10) DrawLabel(txt,x,y,24,kBlack,1.2);
  else if (style==11) DrawLabel(txt,x,y,24,kBlack,1.2);
  else if (style==12) DrawLabel(txt,x,y,25,kRed,0.8);
  else if (style==13) DrawLabel(txt,x,y,27,kBlue,0.8);
}

void PrintDataMC(bool isTruth=false) {
  if (!isTruth) DrawLabel("Data 2011",1,0.4,0.47); DrawLabel("Pythia",2,0.4,0.42); 
  DrawLabel("Herwig++",3,0.6,0.47); DrawLabel("MC average",6,0.6,0.42);
  if (isTruth) DrawLabel("MC average",1,0.4,0.47);
}

void DrawSubPad() {
  _can->SetBottomMargin(FIGURE2_RATIO);
  // create new pad, fullsize to have equal font-sizes in both plots
  TPad *p = new TPad( "p_test", "", 0, 0, 1, 1.0 - SUBFIGURE_MARGIN, 0, 0, 0);
  p->SetTopMargin(1.0 - FIGURE2_RATIO); p->SetFillStyle(0);
  p->SetMargin(0.12,0.04,0.1,1.0 - FIGURE2_RATIO);
  p->Draw(); p->cd(); //p->SetGridy(kTRUE);
}

void DrawPtEtaPoints(Graph2D *graph, VecD ptBins, VecD etaBins, Str xtit="p_{T}^{probe} [GeV]") {
  TLine *line = new TLine(); line->SetLineColor(kGray);
  Graph *bin_xy = new Graph(), *avg_xy = new Graph();
  double min = ptBins.front(), max = ptBins.back();
  TH2F *h = new TH2F(xtit,"",1,17.5,max,1,-4.5,4.5);
  h->GetXaxis()->SetMoreLogLabels(); h->GetYaxis()->SetTitleOffset(0.8);
  h->SetXTitle("p_{T}^{avg}"); h->SetYTitle("jet #eta_{det}");
  h->SetXTitle(xtit);
  h->Draw();

  for (unsigned int ieta=0;ieta<etaBins.size()-1;++ieta) {
    double etal = etaBins[ieta];
    double eta = 0.5*(etal+etaBins[ieta+1]);
    line->DrawLine(min,etal,max,etal);
    if (ieta==etaBins.size()-2) line->DrawLine(min,etaBins[ieta+1],max,etaBins[ieta+1]);
    for (unsigned int ipt=0;ipt<ptBins.size()-1;++ipt) {
      double ptl = ptBins[ipt];
      double pt  = 0.5*(ptl+ptBins[ipt+1]);
      line->DrawLine(ptl,-4.5,ptl,4.5);
      if (ipt==ptBins.size()-2) line->DrawLine(ptBins[ipt+1],-4.5,ptBins[ipt+1],4.5);
      int n=bin_xy->GetN();
      bin_xy->SetPoint(n,pt,eta);
    }
  }
  for (int i=0;i<graph->GetN();++i) {
    avg_xy->SetPoint(i,graph->GetX()[i],graph->GetY()[i]);
    avg_xy->SetPointError(i,graph->GetEX()[i],graph->GetEY()[i]);
  }
  //bin_xy->SetMarkerColor(kOrange); bin_xy->Draw("P");
  avg_xy->SetMarkerSize(0.4); avg_xy->Draw("P");

  line->SetLineColor(kRed+1); line->SetLineWidth(2);
  //double minPt=jetR==4?22:30;
  //line->DrawLine(minPt,-2.8,minPt,2.8);
  ////double pt1=jetR==4?100:105;
  //line->DrawLine(minPt,-2.8,1500,-2.8);
  //line->DrawLine(minPt,2.8,1500,2.8);
  
  //PrintInfo();
}

void Symmetrize(TH1 *h) {
  int Nbins=h->GetNbinsX();
  for (int bin=1;bin<=Nbins/2;++bin) {
    int bin2=Nbins-bin+1;
    double y1=h->GetBinContent(bin), y2=h->GetBinContent(bin2);
    double e1=h->GetBinError(bin), e2=h->GetBinError(bin2);
    if (e1>0.00001&&e2>0.00001) {
      double y=(y1/e1/e1+y2/e2/e2)/(1.0/e1/e1+1.0/e2/e2);
      double e=1.0/sqrt((1.0/e1/e1+1.0/e2/e2));
      h->SetBinContent(bin,y); h->SetBinContent(bin2,y);
      h->SetBinError(bin,e); h->SetBinError(bin2,e);
    } else {
      h->SetBinContent(bin,(y1+y2)/2); h->SetBinContent(bin2,(y1+y2)/2);
      h->SetBinError(bin,0); h->SetBinError(bin2,0);
    }
  }
}
