// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <sstream>

#include "Riostream.h"
#include "TObjArray.h"

#include "Variable.h"
#include "TString.h"
#include "StringList.h"

Variable::Variable( const TString& name, const TString& value )
   : GBase( "Variable" ),
     m_name ( name ),
     m_value( value ),
     m_b( kFALSE ), 
     m_i( 0 ), 
     m_f( 0 ), 
     m_s( "" ),
     m_sl( 0 )
{
   std::stringstream sstr;
   sstr << m_value;
   
   // parse type
   sstr >> m_i;
   if (sstr.eof()) m_type = Variable::Int;
   else {
      sstr >> m_f;
      m_f += m_i;
      if (sstr.eof()) m_type = Variable::Double;
      else {
         if (m_value == "T" || m_value == "F") {
            m_type = Variable::Bool;
            m_b    = (m_value == "T") ? kTRUE : kFALSE;
         }
         else {
            m_type = Variable::String;
            m_s    = m_value;
            m_sl   = new StringList( m_value ); // at least one entry in list

            // a list of string components separated by ':' ?
            if      (m_sl->GetEntries() >= 2) m_type = Variable::ListOfStrings;
            else if (m_sl->GetEntries() == 0) m_logger << kFATAL << "Troubles !!!" << Endl;
         }
      }
   }            
}

Variable::~Variable()
{
   if (m_sl) delete m_sl;
}

TString Variable::GetStrType() const 
{ 
   switch (m_type) {
   case Bool:          return "Bool         ";
   case Int:           return "Int          ";
   case Double:        return "Double       ";
   case String:        return "String       ";
   case ListOfStrings: return "ListOfStrings";
   default:
      m_logger << kFATAL << "Unknown type" << Endl;
   }
   
   return "";
}      

Bool_t Variable::GetBoolValue() const
{
   // sanity check
   if (m_type != Variable::Bool) {
      m_logger << kFATAL << "Variable: \"" << m_name << "\" is not Bool, but: " << m_type << Endl;
   }
   return m_b;
}

Int_t Variable::GetIntValue() const
{
   // sanity check
   if (m_type != Variable::Int) {
      m_logger << kFATAL << "Variable: \"" << m_name << "\" is not Int, but: " << m_type << Endl;
   }
   return m_i;
}

Double_t Variable::GetDoubleValue() const
{
   // sanity check
   if (m_type != Variable::Double) {
      m_logger << kFATAL << "Variable: \"" << m_name << "\" is not Double, but: " << m_type << Endl;
   }
   return m_f;
}

const TString& Variable::GetStringValue() const
{
   // sanity check
   if (m_type != Variable::String && m_type != Variable::ListOfStrings) {
      m_logger << kFATAL << "Variable: \"" << m_name << "\" is not String and not ListOfStrings, but: " 
               << m_type << Endl;
   }
   return m_s;
}

const StringList& Variable::GetStringList() const
{
   // sanity check
   if (m_type != Variable::String && m_type != Variable::ListOfStrings) {
      m_logger << kFATAL << "Variable: \"" << m_name << "\" is not StringList, but: " << m_type << Endl;
   }
   return *m_sl;
}

void Variable::Print( std::ostream& os ) const
{
   os << "\"" << GetVariableName() << "\" : " << GetValue() << " (type: " << GetType() << ")" << std::endl;
}
