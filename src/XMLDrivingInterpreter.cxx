// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TObjArray.h"
#include "TObjString.h"
#include "TDOMParser.h"
#include "TXMLNode.h"
#include "TXMLDocument.h"
#include "TXMLAttr.h"

#include "XMLDrivingInterpreter.h"
#include "GStore.h"
#include "UnitMeasurement.h"
#include "Action.h"

XMLDrivingInterpreter::XMLDrivingInterpreter( const TString& xmlFileName )
   : XMLInterpreter( xmlFileName )
{
   SetName( "XMLDrivingInterpreter" );
}

XMLDrivingInterpreter::~XMLDrivingInterpreter()
{}

Bool_t XMLDrivingInterpreter::Interpret() const
{
   // --------------- xml read
   m_logger << kINFO << "Read xml data card: \"" << GetXMLFileName() << "\"" << Endl;
  
   TDOMParser* xmlparser = new TDOMParser();
      
   Int_t parseCode = xmlparser->ParseFile( GetXMLFileName() );

   m_logger << kINFO << "XML parser returned code: " << parseCode << Endl;
   if (parseCode != 0) m_logger << kFATAL << "loading of xml document failed" << Endl;
  
   // --------------- parse JobConfiguration

   TXMLDocument* xmldoc = xmlparser->GetXMLDocument();

   TXMLNode* jobConfig_node = xmldoc->GetRootNode();
   TXMLNode* jobConfig_elem = jobConfig_node->GetChildren();
  
   while (jobConfig_elem != 0) {
      // interpret child node
      ReadAttribs( jobConfig_elem );

      // crawl on...
      jobConfig_elem = jobConfig_elem->GetNextNode();
   }

   m_logger << kINFO << "Interpretation of driving card done!" << Endl;

   return kTRUE;
}

void XMLDrivingInterpreter::ReadAttribs( TXMLNode* node ) const
{
   if (!node->HasAttributes()) return;
   TListIter attribIt( node->GetAttributes() );
   TXMLAttr* curAttr( 0 );
   while ((curAttr = (TXMLAttr*)attribIt()) != 0) {

      m_logger << kVERBOSE << node->GetNodeName() << ": " << curAttr->GetName() 
               << "  =  \"" << curAttr->GetValue() << "\"" << Endl;

      // add to gStore
      // special treatment for Actions: here, the attributs can have several tokens
      if (TString(node->GetNodeName()) == "Actions") {
         // parse string         
         Action* action = new Action( (TString)curAttr->GetName(), (TString)curAttr->GetValue() );
         if (action->IsActive()) gStore().AddAction( action );
      }
      else {
         gStore().AddVariable( TString(node->GetNodeName()) + "::" + (TString)curAttr->GetName(), curAttr->GetValue() );
      }
   }
}

