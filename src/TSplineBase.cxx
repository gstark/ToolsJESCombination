// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//_______________________________________________________________________
//                                                                      
// Linear interpolation of a TGraph
//_______________________________________________________________________

#include "TSplineBase.h"
#include "MsgLogger.h"
#include "GStore.h"

MsgLogger*                 TSplineBase::m_logger = 0;

TSplineBase::TSplineBase( TString title, TGraph* theGraph, Bool_t isHistogram, std::vector<Double_t> energySize )
  : fGraph( theGraph )
{
  // create output logger
  if (m_logger == 0) CreateMsgLogger();

  // set the type of the data
  hasBins_ = isHistogram;

  gStore().AssertVariable( "ComplexMerger::ConserveBinIntegral" );
  conserveBinIntegral_ = gStore().GetVariable( "ComplexMerger::ConserveBinIntegral" )->GetBoolValue();

  // if it's a histogram then copy the energy limits vector of the initial data
  if(hasBins_)
    {
      // sanity check
      if( (Int_t)theGraph->GetN() != (Int_t)energySize.size() )
        {
          *m_logger << kFATAL << "*** Not the same number of entries in theGraph: " << theGraph->GetN() << " and energySize vector: " << energySize.size() << Endl;
        }

      eBinSize = energySize;

      //compute the energy limits of the bins
      for( Int_t i=0; i<theGraph->GetN(); i++ )
        {
           eBinLimits.push_back( theGraph->GetX()[i] - eBinSize.at(i)/2. );
        }
      eBinLimits.push_back( theGraph->GetX()[theGraph->GetN()-1] + eBinSize.at(theGraph->GetN()-1)/2. );
    }
  else
    {// sanity check
      if( energySize.size()!=0 )
        *m_logger << kFATAL << "A NULL energyLimits vector was expected here, but its size is: " << energySize.size() << Endl;
    }

   // constructor from TGraph
  fGraph = new TGraph( *theGraph );
   // TSpline is a TNamed object
   SetNameTitle( title, title );  

}

TSplineBase::~TSplineBase( void )
{
   // destructor
   if (NULL != fGraph) delete fGraph;
   eBinSize.clear();
   eBinLimits.clear();
}

void TSplineBase::CreateMsgLogger()
{
   m_logger = new MsgLogger( "TSplineBase" );
}

