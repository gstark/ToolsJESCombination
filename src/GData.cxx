// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "GData.h"
#include "GStore.h"
#include "Action.h"

GData* GData::m_instance = 0;

// global accessor function
GData& gData() 
{ 
   return GData::Instance(); 
}

GData::GData()
   : GBase( "GData" )
{
   m_channels.clear();
}

GData::~GData()
{}

void GData::AddChannel( Channel* channel )
{
   if (channel == 0) m_logger << kFATAL << "<AddChannel> Zero pointer" << Endl;

   // add only active channels
   // (NOTE: inactive channels are already inercepted during XML parsing, so this should be always true)
   if (channel->IsActive()) m_channels.push_back( channel );
}

Channel* GData::FindChannel( const TString& channelName )
{
   std::vector<Channel*>::iterator it = m_channels.begin();
   for (; it != m_channels.end(); it++) {
      if ((*it)->GetName() == channelName) return *it;
   }

   return 0;   
}

void GData::CreateGraphs( TFile& file )
{
   std::vector<Channel*>::iterator it = GetChannelVec().begin();
   for (; it != GetChannelVec().end(); it++) (*it)->CreateGraphs( file );  
}

void GData::MergeMeasurements()
{ 
   std::vector<Channel*>::iterator it = GetChannelVec().begin();
   for (; it != GetChannelVec().end(); it++) 
     { // for each channel, one should use here all the merging methods given as arguments of the action "MergeMeasurement"

       const Action* action = gStore().GetAction( "MergeMeasurements" );
       if (!action) m_logger << kFATAL << "Huge troubles in <MergeMeasurements>" << Endl;

       // iterate over the arguments of the action "MergeMeasurements" and merge using different mergers
       std::vector<TString>::iterator itArgs = action->GetArgs().begin();
       for( ; itArgs!=action->GetArgs().end(); itArgs++ )
         {
           (*it)->MergeMeasurements( kTRUE, *itArgs );
         }

     }
}

void GData::Print( std::ostream& os ) 
{   
   os << std::endl;
   os << "---------------------------------------------------------------------------------------" << std::endl;
   os << "                                      G D a t a" << std::endl;
   os << "---------------------------------------------------------------------------------------" << std::endl;
   os << std::endl;

   // iterate over all channels and print them
   std::vector<Channel*>::iterator it = GetChannelVec().begin();
   for (; it != GetChannelVec().end(); it++) (*it)->Print( os );
   os << "---------------------------------------------------------------------------------------" << std::endl;
   os << std::endl;
}
