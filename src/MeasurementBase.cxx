// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iostream>
#include <stdio.h>

#include "TGraph.h"
#include "TGraphErrors.h"
#include "TRandom3.h"
#include "TDatime.h"

#include "MeasurementBase.h"
#include "GStore.h"

#include "TVectorD.h"
#include "StringList.h"

using std::cout;
using std::endl;

MeasurementBase::MeasurementBase( const TString& channelName, 
                          const TString& alias,
                          const TString& experiment )
   : GBase( "MeasurementBase" ),
     m_expId          ( 0 ),
     m_covMatXSec     ( 0 ),
     m_covMatXSecStat ( 0 ),
     m_covMatXSecSyst ( 0 ),
     m_Chi2_graph     ( 0 ),
     m_channelName( channelName ),    
     m_alias      ( alias ),
     m_experiment ( experiment ),
     m_XsecGraph  ( 0 ),
     m_CEnergyInterval(0)
{
  GetDataVec().clear();
  GetSystErrNamesAll().clear();
  // Most of the measurements can use splines. Reset this parameter if necessary
  SetUseSplines( kTRUE );
  SetOnlyBins( kTRUE );

  m_Hist.clear();
  m_autoHist_large.clear();

  gStore().AssertVariable( "ComplexMerger::DistanceRelExpMeas" );
  m_maxRelExpansion = gStore().GetVariable( "ComplexMerger::DistanceRelExpMeas" )->GetDoubleValue();

  gStore().AssertVariable( "ComplexMerger::dMaxInterpolation" );
  m_dMax = gStore().GetVariable( "ComplexMerger::dMaxInterpolation" )->GetDoubleValue();
}

MeasurementBase::~MeasurementBase()
{
  if (m_XsecGraph)  { delete m_XsecGraph; m_XsecGraph = 0; }
  if (m_Chi2_graph) { delete m_Chi2_graph; m_Chi2_graph = 0; }
  if (m_covMatXSec)     { delete m_covMatXSec; m_covMatXSec = 0; }
  if (m_covMatXSecStat) { delete m_covMatXSecStat; m_covMatXSecStat = 0; }
  if (m_covMatXSecSyst) { delete m_covMatXSecSyst; m_covMatXSecSyst = 0; }
  if ( m_systErrNamesAll.size() ) { m_systErrNamesAll.clear(); }

  std::vector<Data*>::iterator it;
  for(it=m_data.begin(); it!=m_data.end(); it++){
    delete *it;
  }
  m_data.clear();

  std::vector< GenObjectBase<TH1>* >::iterator itH;
  for(itH=m_Hist.begin(); itH!=m_Hist.end(); itH++){
    delete *itH;
  }
  m_Hist.clear();

  std::vector< TH1* >::iterator itAH;
  for(itAH=m_autoHist_large.begin(); itAH!=m_autoHist_large.end(); itAH++){
    delete *itAH;
  }
  m_autoHist_large.clear();

  if( m_CEnergyInterval ){ delete m_CEnergyInterval; }
}

void MeasurementBase::AddSystErrName( TString* errName )
{
  // test if  m_systErrNamesAll contains errName. If so return kFALSE
  if( m_systErrNamesAll.size() > 0 ){
    std::vector<TString*>::iterator it = m_systErrNamesAll.begin(); 
    for( ; it != m_systErrNamesAll.end(); it++ )
      if( (**it) == (*errName) )
        m_logger << kFATAL << "the same systematic error name was encountered twice in the measurement !!! " <<Endl;
    }

  // if m_systErrNamesAll doesn't contain errName: add it an return kTRUE
  m_systErrNamesAll.push_back( new TString(*errName) );

}

TGraph* MeasurementBase::CreateGraph( Bool_t writeToFile )
{
   // fill TGraph with data points

   // first initialise vectors
   const Int_t n = (const Int_t)GetDataVec().size();
   Double_t x[n], y[n], ex[n], ey[n];   
   for (Int_t i=0; i<n; i++) {
      const Data *data = GetDataVec().at(i);

      // sanity check
      if( GetSystErrNamesAll().size() != data->GeteXsecSyst().size() ){
         m_logger << kFATAL << "Problem in the components of the systematic uncertainty " << GetSystErrNamesAll().size() << "  " << data->GeteXsecSyst().size() << Endl;
      }

      x [i] = (Double_t)data->Eave();
      ex[i] = (Double_t)data->deltaE()*0.5;

      y [i] = (Double_t)data->Xsec();
      ey[i] = (Double_t)data->eXsecTot();
   }

   m_XsecGraph = new TGraphErrors( n, x, y, ex, ey );
   m_XsecGraph->SetNameTitle( GetExperiment() + "_Xsec", GetExperiment() + " " + GetAlias()); 

   // save to target file
   if (writeToFile) 
     {
       m_XsecGraph->Write();

       if( m_Chi2_graph &&  (GetExperiment()).Contains("Merged") ){
         m_Chi2_graph->Write();
       }
       if( m_Hist.size()>0 ){
         for( UInt_t iH = 0; iH< m_Hist.size(); iH++ )
            m_Hist.at(iH)->Write();
       }
       if( m_autoHist_large.size()>0 ){
         for( UInt_t iH = 0; iH< m_autoHist_large.size(); iH++ )
            m_autoHist_large.at(iH)->Write();
       }
     }

   return m_XsecGraph;
}

void MeasurementBase::UpdateData()
{
   // update exp IDs of data
   std::vector<Data*>::iterator it;
   for (it = GetDataVec().begin(); it != GetDataVec().end(); it++) (*it)->SetExpId( GetExpId() );
}

void MeasurementBase::Print( std::ostream& os ) 
{   
   os << Form( "MeasurementBase: Exp = \"%s\" (\"%s\") [ID: %i]",
               GetExperiment().Data(), GetAlias().Data(), GetExpId() )
      << std::endl;
   os << std::endl;
   
   // iterate over all data and print them
   std::vector<Data*>::iterator it;
   for (it = GetDataVec().begin(); it != GetDataVec().end(); it++) {
      os << "      "; (*it)->Print( os );
   }
   os << std::endl;
}

// GetCEnergyInterval() is special for the ComplexMerger
const Interval& MeasurementBase::GetCEnergyInterval()
{ 
   Double_t eps = 1e-7; // used to avoid border effects
   if( (m_data.back()->Emax()-m_data.front()->Emin()) < 2*eps ){
      eps = 0.;
   }

  if( !m_CEnergyInterval ){
     gStore().AssertVariable( "ComplexMerger::EminExpMeas" );
     Double_t EminExpMeas = gStore().GetVariable( "ComplexMerger::EminExpMeas" )->GetDoubleValue();

     Double_t lowExpansion = ( m_data.front()->Emin() < EminExpMeas ? 0. : m_data.front()->Emin() * m_maxRelExpansion );
     Double_t highExpansion = ( m_data.back()->Emax() < EminExpMeas ? 0. : m_data.back()->Emax() * m_maxRelExpansion );

     m_CEnergyInterval = new Interval( m_data.front()->Emin()-lowExpansion + eps, m_data.back()->Emax()+highExpansion - eps );
  }

  return *m_CEnergyInterval;
}
