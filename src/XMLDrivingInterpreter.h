// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef XMLDrivingInterpreter_h
#define XMLDrivingInterpreter_h

#include "XMLInterpreter.h"

class TXMLNode;

class XMLDrivingInterpreter : public XMLInterpreter {
   
 public:
   
   XMLDrivingInterpreter( const TString& xmlFileName );
   ~XMLDrivingInterpreter();
   
   Bool_t Interpret() const; 
   
 private:
   
   void ReadAttribs( TXMLNode* node ) const;
};

#endif
