// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//
//_______________________________________________________________________
//                                                                      
// Quadratic interpolation of a TGraph
//_______________________________________________________________________

#ifndef TSpline2_h
#define TSpline2_h

#include "TSplineBase.h"

#include "Utils.h"

class TGraph;
class TString;

class TSpline2 : public TSplineBase {
   
public:
   
   TSpline2( TString title, TGraph* theGraph, Bool_t isHistogram, const std::vector<Double_t>& energySize );
   virtual ~TSpline2( void );

  // evaluators
  //returns the value of the spline at x ( NO vertical displacement of splines is considered in this function )
  virtual Double_t Eval( Double_t x ) const;

  //this function should consider vertical displacements of splines, if necessary (for histograms)
  virtual Double_t EvalIntegral( Double_t xmin, Double_t xmax ) const;

  // accessors
  const TString GetType() const { return "TSpline2"; }
  const TSplineBase::ESplineType  GetESplineType( ) const { return TSplineBase::kTSpline2; }
  
   // dummy implementations of virtual base class functions
   virtual void BuildCoeff( void ) {}                                 // no coefficients
   virtual void GetKnot( Int_t i, Double_t& x, Double_t& y ) const {} // no knots
   
private:

  // Quadratic interpolation   
   Double_t Quadrax( Double_t dm, Double_t dm1,
                     Double_t dm2, Double_t dm3,
                     Double_t cos1, Double_t cos2, 
                     Double_t cos3 ) const;

  // computes the primitive of a quadratic interpolation
   Double_t QuadraxPrimitive( Double_t dm, Double_t dm1,
                              Double_t dm2, Double_t dm3,
                              Double_t cos1, Double_t cos2, 
                              Double_t cos3 ) const;

  // this function should NOT consider vertical displacements of splines ( for a GRAPH of points )
  Double_t EvalIntegralPoints( Double_t xmin, Double_t xmax ) const;

  // this function should consider vertical displacements of splines ( for a HISTOGRAM )
  Double_t EvalIntegralBins( Double_t xmin, Double_t xmax ) const;

  ClassDef(TSpline2,0);

};

#endif 


