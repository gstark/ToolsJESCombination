// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//_______________________________________________________________________
//                                                                      
// Linear interpolation of a TGraph
//_______________________________________________________________________
//
// NOTE: all integrals use s [GeV^2] as integration variable
// BUT : all intervals, plots and splines are given in Sqrt(s) [GeV] !
//
#ifndef TSplineBase_h
#define TSplineBase_h

#include <vector>

#include "TSpline.h"

class MsgLogger;

class TSplineBase : public TSpline {

 public:

  TSplineBase( TString title, TGraph* theGraph, Bool_t isHistogram, std::vector<Double_t> energySize);
  virtual ~TSplineBase();

  // testers 
  enum ESplineType { kTSpline1 = 0, 
                     kTSpline2, 
                     kNumCases };       

  // type identifiers
  virtual const TString GetType() const = 0; 
  virtual const ESplineType GetESplineType() const = 0; // kTSpline1 or kTSpline2

  // evaluators
  //returns the value of the spline at x ( NO vertical displacement of splines is considered in this function )
  virtual Double_t Eval( Double_t x ) const = 0;

  //this function should consider vertical displacements of splines, if necessary (for histograms)
  virtual Double_t EvalIntegral( Double_t xmin, Double_t xmax ) const = 0;

 protected:
   
  TGraph *fGraph;  // graph that is splined

  Bool_t hasBins_;  // this should be kTRUE for a histogram
                   //                kFALSE for a list of points
  Bool_t conserveBinIntegral_;  // this should be kTRUE if the integral of the bins should be conserved by the splines, and kFALSE otherwise

  std::vector< Double_t > eBinSize; // energy size of bins, if histogram
  std::vector< Double_t > eBinLimits; // energy limits of the bins (size = ndata+1)



  static MsgLogger* m_logger;

 private:

  static void CreateMsgLogger();

  ClassDef(TSplineBase,0);

};

#endif
