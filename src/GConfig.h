// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//
// Singleton class for global configuration settings 
//

#ifndef Config_h
#define Config_h

#include "Rtypes.h"
#include "MsgLogger.h"

// global accessor
class GConfig;
GConfig& gConfig();

class GConfig {
               
 public:
   
   static GConfig& Instance() { return m_gConfigPtr ? *m_gConfigPtr : *(m_gConfigPtr = new GConfig()); }
   virtual ~GConfig();
   
   Bool_t UseColor() { return m_useColoredConsole; }
   void   SetUseColor( Bool_t uc ) { m_useColoredConsole = uc; }

 public:

      
 private:
   
   // private constructor
   GConfig();
   static GConfig* m_gConfigPtr;
   
 private:
   
   Bool_t m_useColoredConsole;
   
   mutable MsgLogger m_logger;   // message logger
   
   ClassDef(GConfig,0); // Singleton class for global configuration settings
};

#endif
