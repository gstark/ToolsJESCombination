// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//
// pure static factory class
//

#ifndef TSplineFactory_h
#define TSplineFactory_h

#include <vector>

#include "TString.h"
#include "TSpline1.h"
#include "TSpline2.h"

class TSplineBase;
class MsgLogger;

class TSplineFactory {

public:

   // create new TSpline
   static TSplineBase* CreateTSpline( TString title, TGraph* theGraph, const TString& spline_type, Bool_t isHistogram, const std::vector<Double_t>& energySize );
  
private:
  
   static void CreateMsgLogger();
   static MsgLogger* m_logger;

};

#endif
