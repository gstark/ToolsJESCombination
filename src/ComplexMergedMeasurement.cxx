// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iostream>
#include <vector>

#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1F.h"
#include "TObjArray.h"

#include "Action.h"
#include "ComplexMergedMeasurement.h"
#include "Data.h"
#include "Utils.h"
#include "GStore.h"
#include "UnitMeasurement.h"
#include "Channel.h"
#include "GData.h"
#include "TDatime.h"
#include "TMinuit.h"

#include "RootClassFactory.h"

class Interval;

using std::cout;
using std::endl;

TMatrixDSym *m_invCovMatrixXsecValues_NOcorrel_st, *m_invCovMatrixXsecValues_WITHcorrel_st;
std::vector<Double_t> m_toyXsecValuesFit_EminEmax_st;
std::vector<Double_t> m_toyXsecBinCenters_EminEmax_st;
Int_t useCorrelInChi2;

//constructor to be used for the merging of all the channels
ComplexMergedMeasurement::ComplexMergedMeasurement( const  Int_t splineType = 1 )
   : MeasurementBase( *(new TString("AllMerged")), "", "ComplexMerged" )
{
  // security check
  if( (splineType!=1) && (splineType!=2))
    m_logger << kFATAL << "Unknown spline procedure in the global merger: " << splineType << Endl;

  SetName( GetType().Data() );

  Init();

  m_splineType = splineType;
  m_NfilledBinsSmall = 0;
  m_NfilledBinsLarge = 0;

  m_autoHist_large.push_back( new TH1F( "ComplexMergerAutoAnalysis_AllNeg", "ComplexMergerAutoAnalysis_AllNeg", 1001, -1., 0.001 ) );
  m_autoHist_large.push_back( new TH1F( "ComplexMergerAutoAnalysis_MostNeg", "ComplexMergerAutoAnalysis_MostNeg", 1001, -1., 0.001 ) );

  m_NNeg_large = 0;
  m_NmostNeg_large = 0;

  m_covMatrixXsecValues = 0;

  gStore().AssertVariable( "ComplexMerger::PerformFit" );
  m_performFit = gStore().GetVariable( "ComplexMerger::PerformFit" )->GetBoolValue();

  m_namesParFit = 0;
}

// constructor to be used for the merging inside one single channel only
ComplexMergedMeasurement::ComplexMergedMeasurement( const TString& channelName, const Int_t splineType = 1 )
   : MeasurementBase( channelName, "", "Merged" )
{
  // security check
  if( (splineType!=1) && (splineType!=2))
    m_logger << kFATAL << "Unknown spline procedure in channel merger: " << splineType << Endl;

  SetName( GetType().Data() );

  Init();

  m_splineType = splineType;
  m_NfilledBinsSmall = 0;
  m_NfilledBinsLarge = 0;

  m_autoHist_large.push_back( new TH1F( "ComplexMergerAutoAnalysis_AllNeg", "ComplexMergerAutoAnalysis_AllNeg", 1001, -1., 0.001 ) );
  m_autoHist_large.push_back( new TH1F( "ComplexMergerAutoAnalysis_MostNeg", "ComplexMergerAutoAnalysis_MostNeg", 1001, -1., 0.001 ) );

  m_NNeg_large = 0;
  m_NmostNeg_large = 0;

  m_covMatrixXsecValues = 0;

  gStore().AssertVariable( "ComplexMerger::PerformFit" );
  m_performFit = gStore().GetVariable( "ComplexMerger::PerformFit" )->GetBoolValue();
}

ComplexMergedMeasurement::~ComplexMergedMeasurement()
{
   // destroy data vector (data points are true copies)
   std::vector<Data*>::iterator it = GetDataVec().begin();
   for (; it != GetDataVec().end(); it++){ 
     delete *it;
   }
   GetDataVec().clear();

   // destroy the contributors vectors
   m_nContributorsSmall.clear();
   m_nContributorsLarge.clear();
   for( UInt_t i=0; i<m_contributionSmall.size(); i++ ){
      (m_contributionSmall.at(i)).clear();
      (m_contributionSmallCoeff.at(i)).clear();
   }
   for( UInt_t i=0; i<m_contributionLarge.size(); i++ ){
      (m_contributionLarge.at(i)).clear();
   }

   for( UInt_t i=0; i<m_smallBinsEnergies.size(); i++ ){
      delete m_InvCov_matrix_large.at(i);
      delete m_InvCov_matrix_small.at(i);
      delete m_smallBinsEnergies.at(i);
   }
   m_InvCov_matrix_large.clear();
   m_InvCov_matrix_small.clear();
   m_smallBinsEnergies.clear();

   for( UInt_t i=0; i<m_largeBinsEnergies.size(); i++ ){
      delete m_largeBinsEnergies.at(i);
   }
   m_largeBinsEnergies.clear();

   if(m_covMatrixXsecValues){ delete m_covMatrixXsecValues; }
}

void ComplexMergedMeasurement::Init()
{
   // base class init (for initialisation flags)
   GBase::Init();

   // graph with chi2 values
   m_Chi2_graph = new TGraph( GetBinningSmall().size() );
   m_Chi2_graph->SetTitle( "Chi2Plot" );
   m_Chi2_graph->SetName( "Chi2Plot" );

   gStore().AssertVariable( "ComplexMerger::PrintDetails" );
   m_print_details = gStore().GetVariable( "ComplexMerger::PrintDetails" )->GetBoolValue();
}

void ComplexMergedMeasurement::SetBinnings( Channel* _channel )
{
  //clear the m_smallBinsEnergies and m_largeBinsEnergies vectors if necessary
  if( m_smallBinsEnergies.size()!=0 ){
    m_smallBinsEnergies.clear();
  }
  if( m_largeBinsEnergies.size()!=0 ){
    m_largeBinsEnergies.clear();
  }

  // get average limits
  gStore().AssertVariable( "ComplexMerger::AverageInterval" );
  TString lmtString = gStore().GetVariable( "ComplexMerger::AverageInterval" )->GetStringValue();

  // remove brackets and transform to double 
  lmtString.ReplaceAll("[",""); lmtString.ReplaceAll("]","");
  TObjArray* lminmax = lmtString.Tokenize( ";" );
  if (lminmax->GetEntries() != 2) {
    m_logger << kFATAL << "Mismatch when tokenizing limits string: \"" << lmtString << "\": "
             << "length of token list != 2: " << lminmax->GetEntries() << Endl;
  }

  Double_t min = 0., max = 0.;
  if( ((TObjString*)lminmax->At(0))->GetString().IsFloat() 
      && ((TObjString*)lminmax->At(1))->GetString().IsFloat() ){
    min = ((TObjString*)lminmax->At(0))->GetString().Atof();
    max = ((TObjString*)lminmax->At(1))->GetString().Atof();
  }
  else{
    m_logger << kFATAL << "Problem " << ((TObjString*)lminmax->At(0))->GetString() << " or " << ((TObjString*)lminmax->At(1))->GetString() << " is not a real number." << Endl;
  }

  // get the number of bins for the average
  gStore().AssertVariable( "ComplexMerger::AverageFinalBinsN" );
  UInt_t NBins = gStore().GetVariable( "ComplexMerger::AverageFinalBinsN" )->GetIntValue();

  m_logger << kINFO << "The complex average will be computed in the interval [ " << min << " ; " << max << " ] with " << NBins << " bins." << Endl;

  Double_t binSizeUnit = (max-min)/NBins;
  Double_t binSizeSmall = binSizeUnit;

  // set the vectors carrying the information on the size of the small bins

  for(UInt_t i=0; i<NBins; i++ ){
     m_smallBinsEnergies.push_back( new Interval( min +i*binSizeSmall, min+(i+1)*binSizeSmall ) ); 
  }


  // binning for JES 2012 merging
  /*
  NBins = 1783;
  for(UInt_t i=0; i<NBins; i++ ){
     m_smallBinsEnergies.push_back( new Interval( 17.0 +i*1.0, 17.0+(i+1)*1.0 ) ); 
  }
  */

  Double_t binSizeLarge = binSizeUnit, EminIlarge = min;

  // set the vectors carrying the information on the size of the large bins
  for(UInt_t i=0; i<NBins; i++ ){

    binSizeLarge = binSizeUnit;
    // iterate over all measurements and increase the bin until it is large enough
    Bool_t ok = kFALSE;
    std::vector<MeasurementBase*>::iterator it = _channel->GetMeasurementVec().begin();
    for (; it != _channel->GetMeasurementVec().end(); it += (ok?1:0) )
      {
        ok = kTRUE;
        // iterate over the data of a measurement
        for( UInt_t itD = 0; itD < (*it)->GetDataVec().size() && EminIlarge + binSizeLarge < max; itD++ ){
	  if( (( ((*it)->GetDataVec().at(itD))->Emin() <= EminIlarge )
	       && ( ((*it)->GetDataVec().at( ((itD<(*it)->GetDataVec().size()-1) && (((*it)->GetDataVec().at(itD))->Eave() <= EminIlarge))?(itD+1):(itD)))->Emax() >= EminIlarge + binSizeLarge ))
	      ){ // used for JES combination
            binSizeLarge += binSizeUnit;
            i++;
            ok = kFALSE;
            break;
          }
        }
      }
    Interval *iLargeBin = new Interval( EminIlarge, EminIlarge + binSizeLarge );
    Bool_t isContribution = kFALSE;
    it = _channel->GetMeasurementVec().begin();
    for (; it != _channel->GetMeasurementVec().end(); it += 1 ){
      if( ((UnitMeasurement*)(*it))->HasContribution(iLargeBin) ){
        isContribution = kTRUE;
      }
    }
    if( isContribution ){
       m_largeBinsEnergies.push_back( iLargeBin );
    }
    EminIlarge += binSizeLarge;
  }

  /*
  for(UInt_t i=0; i<NBins; i++ ){
    m_largeBinsEnergies.push_back( new Interval( min +i*binSizeLarge, min+(i+1)*binSizeLarge ) ); 
  }
  */
}

// test the consistency of the large binning with respect to the points/bins of the experiments
///no large bin should be contained by an interval of two consecutive points/bin centers of the measurements in the channel
void ComplexMergedMeasurement::TestLargeBinningConsistency( Channel* _channel ){

  // iterate over the large bins of the ComplexMerger
  for( UInt_t itLB = 0; itLB < GetBinningLarge().size(); itLB++ ){
    // iterate over all measurements and test the binning
    std::vector<MeasurementBase*>::iterator it = _channel->GetMeasurementVec().begin();
    for (; it != _channel->GetMeasurementVec().end(); it++)
      {
        // iterate over the data of a measurement
        for( UInt_t itD = 0; itD < (*it)->GetDataVec().size()-1; itD++ ){
           if( ((GetBinningLarge().at(itLB))->GetFracInterv( ((*it)->GetDataVec().at(itD))->Eave(), ((*it)->GetDataVec().at(itD+1))->Eave() )==1.)
              /* && ( ((*it)->GetDataVec().at(itD))->Eave() >= (GetBinningLarge().at(0))->GetMin() )
                 && ( ((*it)->GetDataVec().at(itD+1))->Eave() <= (GetBinningLarge().at(GetBinningLarge().size()-1))->GetMax() )*/ ){
            if( itLB == 0 || itLB == GetBinningLarge().size()-1 )
               m_logger << kWARNING << "POTENTIAL Large complex binning problem for the bin (first/last bin): " << *( GetBinningLarge().at(itLB) ) << " and the measurement " << (*it)->GetExperiment() << " E1: " << ((*it)->GetDataVec().at(itD))->Eave() << " E2: " << ((*it)->GetDataVec().at(itD+1))->Eave() << Endl;
            else
               m_logger << kFATAL << "Large complex binning problem for the bin: " << *( GetBinningLarge().at(itLB) ) << " and the measurement " << (*it)->GetExperiment() << " E1: " << ((*it)->GetDataVec().at(itD))->Eave() << " E2: " << ((*it)->GetDataVec().at(itD+1))->Eave() << Endl;
          }
        }
      }
  }
}

void ComplexMergedMeasurement::AddData( Data* newData )
{
   // here, a new data point is assumed to be added at the end of the data vector

   if (newData == 0) m_logger << kFATAL << "<AddData> Zero pointer" << Endl;

   // newData must be the last element in the vector
   GetDataVec().push_back( newData );
}


// generates a matrix of toy correlated values. The Xsec values are returned only for the Emin-Emax range.
void ComplexMergedMeasurement::GenerateToys( std::vector<TMatrixD*>& toyMValues, std::vector< std::vector<Double_t> >& toyXsecValues_EminEmax_, Double_t Emin_, Double_t Emax_,  UInt_t nToys, Channel* channel, const std::vector<TString*>& syst_names, TRandom3* RG, Timer& timer, UInt_t activStat, UInt_t useIntermLargeBins, UInt_t activSyst, Int_t activMeasurementSyst=-1 ){

  // sanity check(s) 
  if( useIntermLargeBins && ( (!activStat) || (!activSyst) ) )
     m_logger << kFATAL << " Intermediate large bins can be used only with complete fluctuations (to get the average coefficients) !!!" << Endl;
  if( (activStat || activSyst) && (activMeasurementSyst != -1) )
     m_logger << kFATAL << "Cannot measure the effect of one single systematic error when another error is fluctuated !!!" << Endl; 

  // random Gaussian coefficients for all the systematics of the channel
  TArrayD coeff( syst_names.size() );

  for( UInt_t i=0; i<nToys; i++ )
    {
      timer.DrawProgressBar( i );

      // add a new matrix of values to the vector
      toyMValues.push_back( new TMatrixD( channel->GetMeasurementVec().size(), GetBinningSmall().size() ) );
      // generate the coefficients for all the systematic errors
      if( activSyst ){
        Utils::GenGaussRnd( coeff, syst_names.size(), *RG );
      }
      else{
        for( UInt_t j=0; j<syst_names.size(); j++ ){
          coeff[j] = 0.; 
        }
        if( activMeasurementSyst != -1 ){ 
          coeff[activMeasurementSyst] = 1.;
        } 
      }

      // fill bin centers only if vector is empty in first toy
      Int_t fillBinSize_ = 0;
      if( m_toyXsecBinCenters_EminEmax_st.size() == 0 ){ 
         fillBinSize_ = 1;
         m_logger << "Bin centers of measurements used for fit: " ;  // for fitJES()
      }

      // iterate on the measurements and evaluate fluctuated ones
      std::vector<Double_t> toyXsecValuesMeasurements;
      UInt_t _fluctSyst = 0; 
      if( activSyst || (activMeasurementSyst != -1) ) { _fluctSyst = 1; } 
      for( UInt_t j=0; j<channel->GetMeasurementVec().size(); j++ )
         {
            // find the vector of useful coeff for systematics of the measurement
            TArrayD* coeffMeasurement = new TArrayD( (channel->GetMeasurementVec().at(j)->GetSystErrNamesAll()).size() );
            Utils::SelectCoeff( *coeffMeasurement, channel->GetMeasurementVec().at(j)->GetSystErrNamesAll(), coeff, syst_names );
            
            // evaluate the fluctuated measurement on all the bins
	    TVectorD *toyXSec;
            ((UnitMeasurement*)channel->GetMeasurementVec().at(j))->GenerateMeasurementValues( GetSplineType(), *RG, *coeffMeasurement, m_contributionSmall.at(j), GetBinningSmall(), m_contributionLarge.at(j), GetBinningLarge(), toyMValues.back(), j, toyXSec, activStat, useIntermLargeBins, _fluctSyst );

            for( UInt_t k=0; k < (channel->GetMeasurementVec().at(j))->GetDataVec().size(); k++ ){
               if( ((channel->GetMeasurementVec().at(j))->GetDataVec().at(k)->Eave() >= Emin_) && ((channel->GetMeasurementVec().at(j))->GetDataVec().at(k)->Eave() <= Emax_) ){
                  toyXsecValuesMeasurements.push_back( (*toyXSec)[k] );

                  if( fillBinSize_ ){
                     m_toyXsecBinCenters_EminEmax_st.push_back( (channel->GetMeasurementVec().at(j))->GetDataVec().at(k)->Eave() );
                     m_logger << m_toyXsecBinCenters_EminEmax_st.back() << " " ;
                  }
               }
            }

            delete coeffMeasurement;
            delete toyXSec;
         }
      if( fillBinSize_ ){ m_logger << Endl; }  // for fitJES()
      toyXsecValues_EminEmax_.push_back( toyXsecValuesMeasurements );
    }
}

void ComplexMergedMeasurement::MergeChannel( TRandom3 *RGen, Channel* channel ){

  m_logger << kINFO << " A number of " << channel->GetMeasurementVec().size() << " measurements were found in the channel " << channel->GetName() << Endl;

  // find the names of all the systematics of the channel
  std::vector<TString*> syst_namesChannel;
  channel->GetAllSystErrNamesChannel( syst_namesChannel ); 

   // vector carrying the information on the size of the bins
   SetBinnings( channel );

  // check the large binning of the ComplexMerger
  ////TestLargeBinningConsistency( channel );

  for( UInt_t i=0; i<channel->GetMeasurementVec().size(); i++ ){
    // init the contribution vector for the small bin
    std::vector<Bool_t> tmp1;
    m_contributionSmall.push_back( tmp1 );
    // init the contribution coefficients vector for the small bin(to be computed later)
    std::vector<Double_t> tmp2;
    m_contributionSmallCoeff.push_back( tmp2 );

    // init the contribution vector for the large bin
    std::vector<Bool_t> tmp3;
    m_contributionLarge.push_back( tmp3 );
  }

  // compute the matrix of contributions to the bins and count the number of contributors per bin
  //iterate on the bins
  for( UInt_t j=0; j != GetBinningSmall().size(); j++ )
    {
      m_nContributorsSmall.push_back( 0 );
      //iterate on the measurements
      for( UInt_t i=0; i<channel->GetMeasurementVec().size(); i++ ){
         (m_contributionSmall.at(i)).push_back( ((UnitMeasurement*)channel->GetMeasurementVec().at(i))->HasContribution( GetBinningSmall().at(j) ) );
         if( (m_contributionSmall.at(i)).back() ){
          m_nContributorsSmall.back() += 1;
        }
      }
    }
  for( UInt_t j=0; j != GetBinningLarge().size(); j++ )
    {
      m_nContributorsLarge.push_back( 0 );
      //iterate on the measurements
      for( UInt_t i=0; i<channel->GetMeasurementVec().size(); i++ ){
         m_contributionLarge.at(i).push_back( ((UnitMeasurement*)channel->GetMeasurementVec().at(i))->HasContribution( GetBinningLarge().at(j) ) );
         if( m_contributionLarge.at(i).back() ){
          m_nContributorsLarge.back() += 1;
        }
      }
    }
       
  // iterate on the bins ,compute the covariance matrix between the measurements contributing at the bin and then compute the average coefficients
  m_logger << kINFO << " The complex merging will compute the complex merged measurement average coefficients for each bin" << Endl;

  // generate toys 
  gStore().AssertVariable( "ComplexMerger::Ntoy" );
  UInt_t NToys = gStore().GetVariable( "ComplexMerger::Ntoy" )->GetIntValue();

  m_logger << kINFO << "The ComplexMerger will use series of " << NToys << " toys." << Endl;

  // contributions of the toy measurements to the bins
  std::vector<TMatrixD*> toyValuesC;
  std::vector< std::vector<Double_t> > toyXsecValues;

  // time the toy generating loop
  Timer time( NToys, " Timer" );

  // find the possible correlations between measurements
  std::vector< std::vector<Bool_t> > mCorrMeasurementsChannel;
  channel->GetCorrMeasurementsChannel( mCorrMeasurementsChannel ); 

  // generate the toy contributions to the bins (with complete fluctuations and intermediate large splines)
  GenerateToys( toyValuesC, toyXsecValues, 15., 10000., NToys, channel, syst_namesChannel, RGen, time, 1, 1, 1 );
  m_logger << Endl;

  // compute covariance matrix and inverse for fits
  ComputeXsecCovMat( NToys, toyXsecValues );
  toyXsecValues.clear();
  /*
    m_covMatrixXsecValues->Print("all");
    m_invCovMatrixXsecValues_NOcorrel_st->Print("all");
    m_invCovMatrixXsecValues_WITHcorrel_st->Print("all");
    exit(1);
  */

  // compute the coefficients for the average
  ComputeCoeff( NToys, toyValuesC, time, mCorrMeasurementsChannel );

  m_logger << Endl;
  m_logger << "Done! Elapsed time: " << time.GetElapsedTime() << Endl;

  // delete the toyValuesC matrix
  for( UInt_t i=0; i<toyValuesC.size(); i++ ){
     delete toyValuesC.at(i);
  }
  toyValuesC.clear();

  //gROOT->Reset(); 

  // compute the average for the merged measurement
  ComputeComplexAverage( NToys, channel, syst_namesChannel, RGen, mCorrMeasurementsChannel );

  // create the high bin density histogram
  CreateHBDHistogram( channel );

  PrintMergeStatistics();

     FILE *f;
     f = fopen( "../data/ComplexMergerResults_JES/BinningSmall_" + GetChannelName() + ".txt", "w" );
     fprintf( f, "%d\n", (int)GetBinningSmall().size() );
     for( UInt_t i=0; i < GetBinningSmall().size(); i++ ){
        fprintf( f, "%lf      %lf\n", (GetBinningSmall().at(i))->GetMin(), (GetBinningSmall().at(i))->GetMax() );
     }
     fclose(f);
     
     f = fopen( "../data/ComplexMergerResults_JES/BinningLarge_" + GetChannelName() + ".txt", "w" );
     fprintf( f, "%d\n", (int)GetBinningLarge().size() );
     for( UInt_t i=0; i < GetBinningLarge().size(); i++ ){
        fprintf( f, "%lf      %lf\n", (GetBinningLarge().at(i))->GetMin(), (GetBinningLarge().at(i))->GetMax() );
     }
     fclose(f);
     
     for( UInt_t j=0; j<(channel->GetMeasurementVec()).size(); j++ ){
        f = fopen( "../data/ComplexMergerResults_JES/Coeff"+(channel->GetMeasurementVec().at(j))->GetExperiment()+"_" + GetChannelName() + ".txt", "w" );
        for( UInt_t i=0; i < GetBinningSmall().size(); i++ ){
           fprintf( f, "%lf \n", (m_contributionSmallCoeff.at(j)).at(i) );
        }
        fclose(f);
     }
}

void ComplexMergedMeasurement::PrintMergeStatistics() const
{
  
   TString txt = "... Statistics: ";
   m_logger << kINFO << txt << " Number of filled small bins= "<< m_NfilledBinsSmall 
            << " Number of filled large bins= "<< m_NfilledBinsLarge 
            << " Total number of small bins:" << m_smallBinsEnergies.size()
            << " Total number of large bins:" << m_largeBinsEnergies.size()
            << " ..."<< Endl;
   m_logger << kINFO << txt << " Number of all negative elements of correlation matrix:"<< m_NNeg_large <<Endl
            << " Number of correlation matrix with at least one negative element:" << m_NmostNeg_large 
            << " ..."<< Endl;
}


// compute covariance matrix of the Xsec values of all the measurements in the channel
void ComplexMergedMeasurement::ComputeXsecCovMat( UInt_t nToys, const std::vector< std::vector<Double_t> > toyXsecValues )
{
   // sanity check
   if( nToys != toyXsecValues.size() ){
      m_logger << kFATAL << "There were " << nToys << " generated toys, but the size of the toyXsecValues vector is" 
               << toyXsecValues.size() <<Endl;
   }

   m_logger << kINFO << " Computing covariance matrix of Xsec values... " << Endl;
   // init the covariance matrix 
   m_covMatrixXsecValues = new TMatrixDSym( toyXsecValues.at(0).size() );
   m_invCovMatrixXsecValues_NOcorrel_st = new TMatrixDSym( toyXsecValues.at(0).size() );
   m_invCovMatrixXsecValues_WITHcorrel_st = new TMatrixDSym( toyXsecValues.at(0).size() );

   // iterate on the measurements and, compute their mean value
   std::vector<Double_t> meanMeasurements;
   for( UInt_t i=0; i<toyXsecValues.at(0).size(); i++ )
      {
         meanMeasurements.push_back( 0. );
         for(UInt_t t=0; t<nToys; t++ ){
            meanMeasurements.back() += (toyXsecValues.at(t)).at(i)/nToys;
         }
         if( m_print_details ) m_logger << kINFO << " mean: " << meanMeasurements.back() << " ";
      }
   if( m_print_details ) m_logger << Endl;

   // iterate on the measurements and fill the covariance matrix
   for( UInt_t j=0; j<toyXsecValues.at(0).size(); j++ )
      {
         for( UInt_t k=j; k<toyXsecValues.at(0).size(); k++ )
            {
               (*m_covMatrixXsecValues)( j, k ) = 0.;
               for(UInt_t t=0; t<nToys; t++ ){
                  (*m_covMatrixXsecValues)( j, k ) += ((toyXsecValues.at(t)).at(j)-meanMeasurements.at(j))*((toyXsecValues.at(t)).at(k)-meanMeasurements.at(k)) /nToys;
               }
               (*m_covMatrixXsecValues)( k, j ) = (*m_covMatrixXsecValues)( j, k );

               (*m_invCovMatrixXsecValues_WITHcorrel_st)( j, k ) = (*m_covMatrixXsecValues)( j, k );
               (*m_invCovMatrixXsecValues_WITHcorrel_st)( k, j ) = (*m_covMatrixXsecValues)( j, k );
               if( j==k ){ (*m_invCovMatrixXsecValues_NOcorrel_st)( j, k ) = (*m_covMatrixXsecValues)( j, k ); }
               else{ (*m_invCovMatrixXsecValues_NOcorrel_st)( j, k ) = 0.; }
            }
      }

   // sanity check
   if( !(m_covMatrixXsecValues->IsSymmetric()) || !(m_invCovMatrixXsecValues_WITHcorrel_st->IsSymmetric()) || !(m_invCovMatrixXsecValues_NOcorrel_st->IsSymmetric()) ){
      m_logger << kFATAL << "Problems in the computation of the covariance matrix" <<Endl;
   }

   // sanity check
   if( m_covMatrixXsecValues->Determinant() < 0. || m_invCovMatrixXsecValues_WITHcorrel_st->Determinant() < 0. || m_invCovMatrixXsecValues_NOcorrel_st->Determinant() < 0. ){
      m_logger << " Invalid value for the determinant of the covariance matrix: " << Endl;
   }

   m_invCovMatrixXsecValues_WITHcorrel_st->Invert();
   m_invCovMatrixXsecValues_NOcorrel_st->Invert();

   if( m_print_details ){
      for( UInt_t L=0; L<toyXsecValues.at(0).size(); L++ ){
         for( UInt_t C=0; C<toyXsecValues.at(0).size(); C++ ){
            m_logger << " Cov(" << L << "," << C << ")=" <<(*m_covMatrixXsecValues)( L, C ) ;
         }
         m_logger << Endl;
      }
   }
}


void ComplexMergedMeasurement::ComputeCoeff( UInt_t nToys, const std::vector<TMatrixD*>& toyValues, Timer& timer, const std::vector< std::vector<Bool_t> >& _mCorrMeasurementsChannel )
{
  Bool_t exceptionalPrint = kFALSE;
  gStore().AssertVariable( "ComplexMerger::MaxRelNegCorr" );
  Double_t maxRelNegCorr = gStore().GetVariable( "ComplexMerger::MaxRelNegCorr" )->GetDoubleValue();

  // sanity check
  if( nToys != toyValues.size() ){
    m_logger << kFATAL << "There were " << nToys << " generated toys, but the size of the toyValues vector is" 
             << toyValues.size() <<Endl;
  }

  //iterate on the bins
  for( UInt_t i=0; i < GetBinningSmall().size(); i++ )
    {
      exceptionalPrint = kFALSE;

      if( m_print_details ) m_logger << Endl << "********************* Bin: " << *(GetBinningSmall().at(i)) << " ***********************" << Endl;

      // init the covariance matrix for each bin, even if there is no contribution or only one contributor (its size is equal to the number of contributors to the bin)
      m_InvCov_matrix_large.push_back(new TMatrixDSym( m_nContributorsSmall.at(i) ));

      if( m_nContributorsSmall.at(i)>0 ){ m_NfilledBinsSmall++; }

      if( m_nContributorsSmall.at(i)>1 )
        {
          // iterate on the measurements and, if they contribute, compute their mean value
          std::vector<Double_t> meanMeasurements;
          for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
            {
              meanMeasurements.push_back( 0. ); // the final value should be useful for contributing measurements only!!!
              if( (m_contributionSmall.at(j)).at(i) ){
                for(UInt_t t=0; t<nToys; t++ ){
                   meanMeasurements.back() += (*toyValues.at(t))(j,i)/nToys;
                }

                if( m_print_details ) m_logger << " mean: " << meanMeasurements.back() << " ";

              }

            }

          if( m_print_details ) m_logger << "  " << m_nContributorsSmall.at(i) << Endl;

          // iterate on the measurements and, if(they contribute and are correlated) fill the covariance matrix
          // one must use splines on large bins to get the toys used here !!!
          gStore().AssertVariable( "ComplexMerger::UseCorrelInWeightDetermination" );
          Bool_t useCorrelInWeightDetermination = gStore().GetVariable( "ComplexMerger::UseCorrelInWeightDetermination" )->GetBoolValue();
          UInt_t l_no = 0;
          for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
            {
               if( (m_contributionSmall.at(j)).at(i) )
                {
                   (* m_InvCov_matrix_large.at(i))( l_no, l_no ) = 0.;
                  for(UInt_t t=0; t<nToys; t++ ){
                     (* m_InvCov_matrix_large.at(i))( l_no, l_no ) += TMath::Power( ((*toyValues.at(t))(j,i)-meanMeasurements.at(j)), 2 ) /nToys;
                  }

                  if( useCorrelInWeightDetermination ){
                     UInt_t c_no = l_no +1;
                     for( UInt_t k=j+1; k<m_contributionSmall.size(); k++ )
                        {
                           if( (m_contributionSmall.at(k)).at(i) )
                              {
                                 (* m_InvCov_matrix_large.at(i))( l_no, c_no ) = 0.;
                                 if( (_mCorrMeasurementsChannel.at(j)).at(k) ){
                                    for(UInt_t t=0; t<nToys; t++ ){
                                       (* m_InvCov_matrix_large.at(i))( l_no, c_no ) += ((*toyValues.at(t))(j,i)-meanMeasurements.at(j))*((*toyValues.at(t))(k,i)-meanMeasurements.at(k)) /nToys;
                                    }
                                 }
                                 (* m_InvCov_matrix_large.at(i))( c_no, l_no ) = (* m_InvCov_matrix_large.at(i))( l_no, c_no );
                                 
                                 c_no++;
                              }
                        }
                  }

                  l_no++;
                }
            }
          if( m_print_details ) m_logger << Endl;

          // sanity check
          if( !( m_InvCov_matrix_large.at(i)->IsSymmetric()) ){
            m_logger << kFATAL << "Problems in the computation of the covariance matrix" <<Endl;
          }

          // "most negative" element of the correlation matrix
          Double_t mostN_large = 1.;
          // fill the histogram with all the negative element(s) of a correlation matrix taken once (we don't add its symetric too)
          for( UInt_t L=1; L<m_nContributorsSmall.at(i)-1; L++ ){
             for( UInt_t C=L+1; C<m_nContributorsSmall.at(i); C++ ){
               if( (* m_InvCov_matrix_large.at(i))( L, C )<0. ){
                for( UInt_t iAHist=0; iAHist<m_autoHist_large.size(); iAHist++ ){
                  if( ((TString)m_autoHist_large[iAHist]->GetTitle()).Contains( "ComplexMergerAutoAnalysis_AllNeg" ) ){
                     m_autoHist_large[iAHist]->Fill( (* m_InvCov_matrix_large.at(i))( L, C )/TMath::Sqrt( (* m_InvCov_matrix_large.at(i))( L, L )*(* m_InvCov_matrix_large.at(i))( C, C ) ) );
                    m_NNeg_large++;
                  }
                }
                mostN_large = ( mostN_large > (* m_InvCov_matrix_large.at(i))( L, C )/TMath::Sqrt( (* m_InvCov_matrix_large.at(i))( L, L )*(* m_InvCov_matrix_large.at(i))( C, C ) ) ) ?
                   (* m_InvCov_matrix_large.at(i))( L, C )/TMath::Sqrt( (* m_InvCov_matrix_large.at(i))( L, L )*(* m_InvCov_matrix_large.at(i))( C, C ) ) : mostN_large;

              }
                  
              // sanity check
               if( /* m_print_details && */ ((* m_InvCov_matrix_large.at(i))( L, C )<0. ) 
                   && ( TMath::Abs((* m_InvCov_matrix_large.at(i))( L, C )/TMath::Sqrt( (* m_InvCov_matrix_large.at(i))( L, L )*(* m_InvCov_matrix_large.at(i))( C, C ) ) ) > maxRelNegCorr ) ){
                  m_logger << Endl << "********************* Bin: " << *(GetBinningSmall().at(i)) << " ***********************" << Endl;
                m_logger << kWARNING << "Important negative value of an element of a covariance (tested with correlation) matrix:"
                         << (* m_InvCov_matrix_large.at(i))( L, C ) <<". Check NToys and/or data base !!!" << Endl;
                exceptionalPrint = kTRUE;
              }
            }
          }

          // fill the histogram with "most negative" element of a correlation matrix 
          for( UInt_t iAHist=0; iAHist<m_autoHist_large.size(); iAHist++ ){
            if( ((TString)m_autoHist_large[iAHist]->GetTitle()).Contains( "ComplexMergerAutoAnalysis_MostNeg" ) ){
              if( mostN_large<0.001 ){
                m_autoHist_large[iAHist]->Fill( mostN_large );
                m_NmostNeg_large++;

              }
            }
          }

          // sanity check
          Double_t det_cov = m_InvCov_matrix_large.at(i)->Determinant();
          if( det_cov <0. ){ 
            m_logger << " Invalid value for the determinant of the covariance matrix: " << det_cov << Endl;
          }

          if( m_print_details || exceptionalPrint ) m_logger << " det_cov: " << det_cov << Endl;

          if( m_print_details|| exceptionalPrint ){
             for( UInt_t L=0; L<m_nContributorsSmall.at(i); L++ ){
                for( UInt_t C=0; C<m_nContributorsSmall.at(i); C++ ){
                 m_logger << " Cov(" << L << "," << C << ")=" <<(* m_InvCov_matrix_large.at(i))( L, C ) ;
              }
              m_logger << Endl;
            }
          }

          // invert the covariance matrix and compute the average coefficients 
          m_InvCov_matrix_large.at(i)->Invert();

          if( m_print_details || exceptionalPrint ){
             m_logger << " det_invCov: " <<  m_InvCov_matrix_large.at(i)->Determinant() << "   " <<  m_InvCov_matrix_large.at(i)->GetNrows()  <<Endl;
             for( UInt_t L=0; L<m_nContributorsSmall.at(i); L++ ){
                for( UInt_t C=0; C<m_nContributorsSmall.at(i); C++ ){
                 m_logger << " invCov(" << L << "," << C << ")=" <<(* m_InvCov_matrix_large.at(i))( L, C ) ;
              }
              m_logger << Endl;
            }
          }

          // init two useful vectors
          TMatrixD* lineMOne = new TMatrixD( 1, m_nContributorsSmall.at(i) );
          TMatrixD* columnMOne = new TMatrixD( m_nContributorsSmall.at(i), 1 );
          for( UInt_t a=0; a<m_nContributorsSmall.at(i); a++ )
            {
              (*lineMOne)( 0, a ) = 1.;
              (*columnMOne)( a, 0 ) = 1.;
            }

          // compute the numerators of the coefficients fractions
          TMatrixD *coeffV = new TMatrixD( 1, m_nContributorsSmall.at(i) );
          coeffV->Mult( *lineMOne, * m_InvCov_matrix_large.at(i) );

          // compute the denominator of the coefficient fraction
          TMatrixD *DC = new TMatrixD( m_nContributorsSmall.at(i), 1 );
          TMatrixD *D  = new TMatrixD( 1, 1 );
          DC->Mult( * m_InvCov_matrix_large.at(i), *columnMOne );
          D->Mult( *lineMOne, *DC );

          // compute the vector of coefficients
          (*coeffV) *= 1/( (*D)(0,0) );

          // sanity check
          if( D->GetNcols()!=1 || D->GetNrows()!=1 || (UInt_t)coeffV->GetNcols()!=m_nContributorsSmall.at(i) || coeffV->GetNrows()!=1 ){
            m_logger << kFATAL << "Problems in the computation of the coefficients" << Endl;
          }

          if( m_print_details || exceptionalPrint ) m_logger << " coeffs: ";
          Double_t sumc=0.;
          for(UInt_t cno=0; cno<((UInt_t)coeffV->GetNcols()); cno ++){
            if( m_print_details ) m_logger << (*coeffV)(0, cno) << "  ";
            sumc += (*coeffV)(0, cno);
          }
          if( m_print_details || exceptionalPrint ) m_logger << "Sum:" << sumc << Endl;

          // sanity check (the sum of the average coefficients for a bin should be equal to 1 )
          if( TMath::Abs(sumc - 1.) > 1e-14 )
            m_logger << kFATAL << " Problem with the sum of the average coefficients: " << sumc << Endl;

          // adjust the size of the contribution cefficients vector and save it (just add 0 when there is no contribution)
          UInt_t c=0;
          for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
            {
               if( (m_contributionSmall.at(j)).at(i) ){

                //sanity check
                if( (*coeffV)( 0, c )<0. || (*coeffV)( 0, c )>1. ){
                  m_logger << kWARNING << "Invalid value for an average coefficient " << (*coeffV)( 0, c ) << Endl;
                }

                (m_contributionSmallCoeff.at(j)).push_back( (*coeffV)( 0, c ) );
                c++;
              }
              else{
                 (m_contributionSmallCoeff.at(j)).push_back( 0. );
              }
            }

          meanMeasurements.clear();
          delete coeffV;
          delete DC;
          delete D;
          delete lineMOne;
          delete columnMOne;

        }
      else
        {
           if( m_nContributorsSmall.at(i)==1 )
            {
              // we have only one contribution coefficient equal to 1.
              for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
                {
                   if( (m_contributionSmall.at(j)).at(i) ){
                      (m_contributionSmallCoeff.at(j)).push_back( 1. );
                   }
                   else{
                      (m_contributionSmallCoeff.at(j)).push_back( 0. );
                   }
                }
            }
          else
            {
               if( m_nContributorsSmall.at(i)==0 )
                {
                  // all the contribution coefficients are 0.
                  for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
                    {
                       (m_contributionSmallCoeff.at(j)).push_back( 0. );
                    }
                }
              else{
                m_logger << kFATAL<< "the number of contributors should be >= 0 for each bin" << Endl;
              }
            }
        }
    }

  for( UInt_t i=0; i < GetBinningLarge().size(); i++ )
    {
       if( m_print_details ) m_logger << Endl << "********************* Bin: " << *(GetBinningLarge().at(i)) << " ***********************" << Endl;
       if( m_nContributorsLarge.at(i)>0 ){ m_NfilledBinsLarge++; }
    }  

}

// computes the averages using the values of the toys
void ComplexMergedMeasurement::ComputeComplexAverage( UInt_t nToys, const std::vector<TMatrixD*>& toyValues, Timer& timer, TVectorD *errorCorrection_bin, UInt_t activeStat, UInt_t activeSyst, const std::vector< std::vector<Bool_t> >& _mCorrMeasurementsChannel, const std::vector<TString*>& syst_names, Int_t activMeasurementSyst=-1 ){

  if( (activeStat || activeSyst) && (activMeasurementSyst != -1) )
     m_logger << kFATAL << "Cannot measure the effect of one single systematic error when another error is fluctuated !!!" << Endl; 

  // matrix with the mean of all experiments in a bin for a given toy
  TMatrixD *bin_toy_mean = new TMatrixD( GetBinningSmall().size(), nToys );
  TVectorD *mean_bin = new TVectorD( GetBinningSmall().size() ); 
  TVectorD *error_bin = new TVectorD( GetBinningSmall().size() ); 
  Int_t iFilled = -1;  

  FILE *f1 = 0; 
  if( activMeasurementSyst!=-1 ){ 
     f1 = fopen( "../data/ComplexMergerResults_JES/SystError_" + (*(syst_names.at(activMeasurementSyst))) + ".txt", "w" ); 
  } 

  //iterate on the bins
  for( UInt_t i=0; i < GetBinningSmall().size(); i++ )
    {
       // init the covariance matrix for each bin(splines on small bins), even if there is no contribution or only one contributor (its size is equal to the number of contributors to the bin)
       m_InvCov_matrix_small.push_back(new TMatrixDSym( m_nContributorsSmall.at(i) ));

       if( m_nContributorsSmall.at(i)>0 )
        {
          // this bin was filled. Increment the number of filled bins
          iFilled ++;

          // those vectors will be used to adjust errors with a chi2 method
          std::vector<Double_t> mean_bin_meas; // vector with the averages of the contributions of different measurements
          mean_bin_meas.clear();

          // iterate on the measurements and compute the mean of the contributing measurements
          (*error_bin)[i] = 0.;
          (*mean_bin)[i] = 0.;
          for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
            {
               if( (m_contributionSmall.at(j)).at(i) )
                {
                  mean_bin_meas.push_back( 0. ); // only the mean of contributing measurements should be contained in this vector

                  for(UInt_t t=0; t<nToys; t++ ){
                     mean_bin_meas.back() += (*toyValues.at(t))(j,i)/nToys;
                     (*mean_bin)[i] += (m_contributionSmallCoeff.at(j)).at(i) * (*toyValues.at(t))(j,i)/nToys; //(complex average)
                  }
                }
            }

          if( m_print_details ) m_logger << " meanBin " << (*mean_bin)[i] << "   " <<m_nContributorsSmall.at(i) << Endl;

          // compute the error of the mean
          for(UInt_t t=0; t<nToys; t++ ){
           (*bin_toy_mean)[i][t] = 0.;
            for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
              {
                 if( (m_contributionSmall.at(j)).at(i) ){
                    (*bin_toy_mean)[i][t] += (m_contributionSmallCoeff.at(j)).at(i) * (*toyValues.at(t))(j,i); //(complex average)
                }
              }
            (*error_bin)[i] += ((*bin_toy_mean)[i][t] - (*mean_bin)[i]) * ((*bin_toy_mean)[i][t] - (*mean_bin)[i])/nToys;
          }

          (*error_bin)[i] = TMath::Sqrt( (*error_bin)[i] );

          // compute chi2 using the inverted covariance matrix in each bin
          if( activeStat && activeSyst ){

            // iterate on the measurements and, if(they contribute and are correlated) fill the covariance matrix
            // one must use splines on small bins to get the toys used here !!!
            UInt_t l_no = 0;
            for( UInt_t j=0; j<m_contributionSmall.size(); j++ )
              {

                 if( (m_contributionSmall.at(j)).at(i) )
                  {
                     (* m_InvCov_matrix_small.at(i))( l_no, l_no ) = 0.;
                    for(UInt_t t=0; t<nToys; t++ ){
                       (* m_InvCov_matrix_small.at(i))( l_no, l_no ) += TMath::Power( ((*toyValues.at(t))(j,i)-mean_bin_meas.at(l_no)), 2 ) /nToys;
                    }
                    
                    UInt_t c_no = l_no +1;
                    for( UInt_t k=j+1; k<m_contributionSmall.size(); k++ )
                      {
                         if( (m_contributionSmall.at(k)).at(i) )
                          {
                             (* m_InvCov_matrix_small.at(i))( l_no, c_no ) = 0.;
                            if( (_mCorrMeasurementsChannel.at(j)).at(k) ){
                              for(UInt_t t=0; t<nToys; t++ ){
                                 (* m_InvCov_matrix_small.at(i))( l_no, c_no ) += ((*toyValues.at(t))(j,i)-mean_bin_meas.at(l_no))*((*toyValues.at(t))(k,i)-mean_bin_meas.at(c_no)) /nToys;
                              }
                            }
                            (* m_InvCov_matrix_small.at(i))( c_no, l_no ) = (* m_InvCov_matrix_small.at(i))( l_no, c_no );
                            
                            c_no++;
                          }
                      }
                    
                    l_no++;
                  }
              }
            if( m_print_details ) m_logger << Endl;
            
            // sanity check
            if( !( m_InvCov_matrix_small.at(i)->IsSymmetric()) ){
              m_logger << kFATAL << "Problems in the computation of the covariance matrix" <<Endl;
            }
            
            // sanity check
            Double_t det_cov = m_InvCov_matrix_small.at(i)->Determinant();
            if( det_cov <0. ){ 
              m_logger << " Invalid value for the determinant of the covariance matrix: " << det_cov << Endl;
            }
            
            // invert the covariance matrix and compute the average coefficients 
            m_InvCov_matrix_small.at(i)->Invert();
            
            if( m_print_details || 0 ){
               m_logger << " det_invCov_small: " <<  m_InvCov_matrix_small.at(i)->Determinant() << "   " <<  m_InvCov_matrix_small.at(i)->GetNrows()  <<Endl;
               for( UInt_t L=0; L<m_nContributorsSmall.at(i); L++ ){
                  for( UInt_t C=0; C<m_nContributorsSmall.at(i); C++ ){
                   m_logger << " invCov_small(" << L << "," << C << ")=" <<(* m_InvCov_matrix_small.at(i))( L, C ) ;
                }
                m_logger << Endl;
              }
            }

            Double_t chi2 = 0.;
            if( m_nContributorsSmall.at(i)>1 )
              {
                // init 4 useful vectors
                 TMatrixD* lineMMeanBin = new TMatrixD( 1, m_nContributorsSmall.at(i) );
                 TMatrixD* lineMMeanBin_meas = new TMatrixD( 1, m_nContributorsSmall.at(i) );
                 TMatrixD* columnMMeanBin = new TMatrixD( m_nContributorsSmall.at(i), 1 );
                 TMatrixD* columnMMeanBin_meas = new TMatrixD( m_nContributorsSmall.at(i), 1 );
                 for( UInt_t a=0; a<m_nContributorsSmall.at(i); a++ )
                  {
                    (*lineMMeanBin)( 0, a ) = (*mean_bin)[i];
                    (*lineMMeanBin_meas)( 0, a ) = mean_bin_meas.at(a);
                    (*columnMMeanBin)( a, 0 ) = (*mean_bin)[i];
                    (*columnMMeanBin_meas)( a, 0 ) = mean_bin_meas.at(a);
                  }
                
                // compute chi2
                 TMatrixD *DC = new TMatrixD( m_nContributorsSmall.at(i), 1 );
                TMatrixD *D  = new TMatrixD( 1, 1 );
                ////DC->Mult( *m_InvCov_matrix_large.at(i), (*columnMMeanBin_meas)-(*columnMMeanBin) );
                DC->Mult( *m_InvCov_matrix_small.at(i), (*columnMMeanBin_meas)-(*columnMMeanBin) );
                D->Mult( (*lineMMeanBin_meas)-(*lineMMeanBin), *DC );
                
                // sanity check
                if( D->GetNcols()!=1 || D->GetNrows()!=1 ){
                  m_logger << kFATAL << "Problems in the computation of chi2" << Endl;
                }
                
                chi2 = (*D)( 0, 0 )/(m_nContributorsSmall.at(i)-1);
                
                delete D;
                delete DC;
                delete lineMMeanBin;
                delete lineMMeanBin_meas;
                delete columnMMeanBin;
                delete columnMMeanBin_meas;
              }
            else
              { // here there is only one contribution to the bin
                chi2 = 0.;
              }
            
            m_Chi2_graph->SetPoint( i, (GetBinningSmall().at(i))->GetMean(), TMath::Sqrt( chi2 ) );
            
            // rescaling error factor: using chi2 
            (*errorCorrection_bin)[i] = TMath::Max( 1.0, TMath::Sqrt( chi2 ) );
            if( m_print_details ) m_logger << " chi2Bin: " << *GetBinningSmall().at(i) << " chi2/( nContributors-1 )= "<< chi2 <<Endl;
          }
          (*error_bin)[i] *= (*errorCorrection_bin)[i];
          mean_bin_meas.clear();

          if( (!activeStat) && (!activeSyst) && (activMeasurementSyst==-1) ){
             std::vector<Double_t> dummy;
             dummy.clear();
             // add a new data corresponding to the bin(the error will be updated when fluctuating all data errors)
             AddData( new Data( (GetBinningSmall().at(i))->GetMin(), (GetBinningSmall().at(i))->GetMax(), (*mean_bin)[i], 0., dummy ) ); 
          } 
          if( activMeasurementSyst != -1 ){ 
             fprintf( f1, "%E \n", ((*mean_bin)[i] - (GetDataVec().at(iFilled))->Xsec())*((*errorCorrection_bin)[i]) ); 
             // switch to the standard average: for further syst estimation on the integral (with rescalling) 
             (*mean_bin)[i] = (GetDataVec().at(iFilled))->Xsec(); 
          } 
          if( activeStat && activeSyst ){
             // update Complex Merger diagonal error
             (GetDataVec().at(iFilled))->SeteXsecStatError( (*error_bin)[i] );
          }

        }

    }

  if( activMeasurementSyst!=-1 ){ 
     fclose( f1 ); 
  } 

  m_logger << Endl;
  m_logger << "Done! Elapsed time: " << timer.GetElapsedTime() << Endl;

    if( activeStat && activeSyst ){
      m_logger << "Computing the global covariance matrix of the Complex Merger..." << Endl;
    }
    else{
      if( activeStat ){
        m_logger << "Computing the statistical covariance matrix of the Complex Merger..." << Endl;
      }
      else{
        if( activeSyst ){
          m_logger << "Computing the systematic covariance matrix of the Complex Merger..." << Endl;
        }
        else{
          m_logger << "No active fluctuation for covariance matrix" << Endl;
        }
      }
    }


    // compute the covariance matrix of the ComplexMerger result
    TMatrixD* covMat = new TMatrixD( m_NfilledBinsSmall, m_NfilledBinsSmall );
    //iterate on the bins
    Int_t if1=-1;
    if( activeStat || activeSyst ){
      // time the computation of the covariance matrix
      Timer timeM( GetBinningSmall().size(), " TimerM" );
      for( UInt_t ib1=0; ib1 < GetBinningSmall().size(); ib1++ ){
         if( m_nContributorsSmall.at(ib1)>0 ){
          if1++;
          Int_t if2=-1;
          for( UInt_t ib2=0; ib2 < GetBinningSmall().size(); ib2++ ){
             if( m_nContributorsSmall.at(ib2)>0 ){
              if2++;
              (*covMat)[if1][if2] = 0.;
              for(UInt_t t=0; t<nToys; t++ ){
                (*covMat)[if1][if2] += ((*bin_toy_mean)[ib1][t] - (*mean_bin)[ib1])*((*errorCorrection_bin)[ib1]) * ((*bin_toy_mean)[ib2][t] - (*mean_bin)[ib2])*((*errorCorrection_bin)[ib2])/nToys;
              }
              //sanity check
              if( ((if1==if2)&&(ib1!=ib2)) || ((if1!=if2)&&(ib1==ib2)) ){
                m_logger << kFATAL << "Problem in the index of the bins !!!" << Endl;
              }
              if( (if1==if2) && (fabs(TMath::Sqrt((*covMat)[if1][if2]) - (*error_bin)[ib1])) > 1e-7 ){
                m_logger << kFATAL << "Error in the covariance matrix or diagonal error: " << (*covMat)[if1][if2] << " " << (*error_bin)[ib1] << Endl;
              }
            }
          }
          //sanity check
          if( (if1 > if2) && ((*covMat)[if1][if2] != (*covMat)[if2][if1]) ){
            m_logger << kFATAL << "Error in the covariance matrix(not symmetric::): " << (*covMat)[if1][if2] << " " << (*covMat)[if2][if1] << Endl;        
          }
        }
        timeM.DrawProgressBar( ib1 );
      }
      m_logger << Endl;
      m_logger << "Done! Elapsed time: " << timeM.GetElapsedTime() << Endl;
    }
    if( activeStat && activeSyst ){
      if( m_covMatXSec != NULL ){ delete m_covMatXSec; }
      m_covMatXSec = new TMatrixD( *covMat );
    }
    else{
      if( activeStat ){
        if( m_covMatXSecStat != NULL ){ delete m_covMatXSecStat; }
        m_covMatXSecStat = new TMatrixD( *covMat );
      }
      else{
        if( activeSyst ){
          if( m_covMatXSecSyst != NULL ){ delete m_covMatXSecSyst; }
          m_covMatXSecSyst = new TMatrixD( *covMat );
        }
        else{
          m_logger << "No active fluctuation for covariance matrix" << Endl;
        }
      }
    }
    delete covMat;


    delete bin_toy_mean;
    delete mean_bin;
    delete error_bin;
}


/* F1 */
static Int_t nMinNeg = 0;
static Int_t nMaxPos = 0;
static TString fitFunctionName;

/*
static TString namesParJES[5] = { "A0", "A1", "A2", "A3", "pT0der" };
static Double_t vstartParJES[5] = { 1., 0., 0., 0., 3000 };
static Double_t stepParJES[5] = { 0.001, 0.001, 0.001, 0.001, 0.00 };
static TString fixedParJES[5] = { "FREE", "FREE", "FREE", "FREE", "FIXED" };
*/

/* F2 */
/*
static TString namesParJES[2] = { "a", "b" };
static Double_t vstartParJES[2] = { 0, 0 };
static Double_t stepParJES[2] = { 0.001, 0.001 };
static TString fixedParJES[2] = { "FREE", "FREE" };
*/

/* F JER */
/*
static TString namesParJES[3] = { "N", "S", "C" };
static Double_t vstartParJES[3] = { 5., 0, 0.05 };
static Double_t stepParJES[3] = { 0.001, 0.001, 0.001 };
static TString fixedParJES[3] = { "FREE", "FREE", "FREE" };
*/

// fuction for JES fit
Double_t JESfitFunction( Double_t pT, Double_t *Par, Int_t nPar ){ // [s] = GeV^2

  /* F1 JES */
  if( fitFunctionName.Contains("F1JES") ){
    Double_t pT0 = Par[ nMaxPos - nMinNeg ];

    Double_t parLast = 0.;
    for( Int_t i=nMinNeg; i<nMaxPos; i++ ){
      parLast -= Par[i-nMinNeg] * pow(log(pT0),i-1) * i / pT0;
    }
    parLast /= nMaxPos * pow(log(pT0),nMaxPos-1) / pT0;

    Double_t JESfit = 0.;
    for( Int_t i=nMinNeg; i<nMaxPos; i++ ){
      JESfit += Par[i-nMinNeg] * pow(log(pT),i);
    }
    JESfit += parLast * pow(log(pT),nMaxPos);
    return JESfit;
  }

  /*
    Double_t pT0 = Par[4];
    Double_t par4 = - ( Par[1]/pT0 + Par[2] * pow(log(pT0),1)*2/pT0 + Par[3] * pow(log(pT0),2)*3/pT0 ) / (4*pow(log(pT0),3)/pT0);

    Double_t JESfit = Par[0] * pow(log(pT),0) + Par[1] * pow(log(pT),1) + Par[2] * pow(log(pT),2) + Par[3] * pow(log(pT),3) + par4 * pow(log(pT),4);
  */

  /* F2 JES */
  if( fitFunctionName.Contains("F2JES") ){

    Double_t JESfit = 1. + Par[0] + Par[1] * pow(log(pT)-log(20.),1);
    return JESfit;
  }

   /* F JER */
  if( fitFunctionName.Contains("FJER") ){
    Double_t JERfit = sqrt( pow(Par[0]/pT,2) + pow(Par[1],2)/pT + pow(Par[2],2) );
    return JERfit;
  }

  cout << "Unknown fitting function: " << fitFunctionName << endl;
  exit(1);

  return 0.;
}

static void fcnFitJES(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
   // sanity check
   if( (m_toyXsecValuesFit_EminEmax_st.size() != m_toyXsecBinCenters_EminEmax_st.size()) || (m_toyXsecValuesFit_EminEmax_st.size() != (UInt_t)m_invCovMatrixXsecValues_NOcorrel_st->GetNcols()) || (m_toyXsecValuesFit_EminEmax_st.size() != (UInt_t)m_invCovMatrixXsecValues_WITHcorrel_st->GetNcols()) ){
      cout << "Problem in vector and/or matrix size !!!" << endl;
      exit(1);
   }

   TMatrixDSym *m_invCovMatrixXsecValues_ = 0;
   if( useCorrelInChi2 ){
      m_invCovMatrixXsecValues_ = m_invCovMatrixXsecValues_WITHcorrel_st;
   }
   else{
      m_invCovMatrixXsecValues_ = m_invCovMatrixXsecValues_NOcorrel_st;
   }
   //compute chisquare
   Double_t chisq = 0.;
   for( UInt_t i=0; i<m_toyXsecValuesFit_EminEmax_st.size(); i++ ){
      for( UInt_t j=0; j<m_toyXsecValuesFit_EminEmax_st.size(); j++ ){

         chisq += ( JESfitFunction( m_toyXsecBinCenters_EminEmax_st.at(i), par, npar ) - m_toyXsecValuesFit_EminEmax_st.at(i) ) *
                  (( *m_invCovMatrixXsecValues_ )[i][j]) *
                  ( JESfitFunction( m_toyXsecBinCenters_EminEmax_st.at(j), par, npar ) - m_toyXsecValuesFit_EminEmax_st.at(j) );
      }
   }
   f = chisq;
}

TH1D* BuildHistogram_f( Double_t (*func_)( Double_t, Double_t*, Int_t ), Double_t *Par_, Int_t nPar_, Int_t N_, Double_t binLimits_[] ){

   TVectorD *func_v = new TVectorD(N_);
   for( Int_t i=0; i<N_; i++){
      (*func_v)[i] = func_( (binLimits_[i]+binLimits_[i+1])/2, Par_, nPar_ );
   }

   TH1D *func_vH = new TH1D( *func_v );
   (func_vH->GetXaxis())->Set( N_, binLimits_ );
   return func_vH;
}

void ComplexMergedMeasurement::fitJES( TH1D* &fitH_JES_, TVectorD* &parFit_ ){

   gStore().AssertVariable( "ComplexMerger::UseCorrelInWeightDetermination" );
   Bool_t useCorrelInWeightDetermination = gStore().GetVariable( "ComplexMerger::UseCorrelInWeightDetermination" )->GetBoolValue();
   useCorrelInChi2 = useCorrelInWeightDetermination;

   // get the type of function to be used in the fit
   gStore().AssertVariable( "ComplexMerger::FitFunctionName" );
   fitFunctionName = gStore().GetVariable( "ComplexMerger::FitFunctionName" )->GetStringValue();
   // get min & max exponents defining the polynomial function used in the fit
   gStore().AssertVariable( "ComplexMerger::nMinNegFit" );
   nMinNeg = gStore().GetVariable( "ComplexMerger::nMinNegFit" )->GetIntValue();
   gStore().AssertVariable( "ComplexMerger::nMaxPosFit" );
   nMaxPos = gStore().GetVariable( "ComplexMerger::nMaxPosFit" )->GetIntValue();
   // get parameter names
   gStore().AssertVariable( "ComplexMerger::namesParameters" );
   const StringList& parNames = gStore().GetVariable( "ComplexMerger::namesParameters" )->GetStringList();
   // get parameter starting value in the fit
   gStore().AssertVariable( "ComplexMerger::vstartParameters" );
   const StringList& parVstart = gStore().GetVariable( "ComplexMerger::vstartParameters" )->GetStringList();
   // get parameter step in the fit
   gStore().AssertVariable( "ComplexMerger::stepParameters" );
   const StringList& parStep = gStore().GetVariable( "ComplexMerger::stepParameters" )->GetStringList();
   // get parameter status FREE / FIXED
   gStore().AssertVariable( "ComplexMerger::FreeFixedParameters" );
   const StringList& parStatusList = gStore().GetVariable( "ComplexMerger::FreeFixedParameters" )->GetStringList();

   Int_t nPar = nMaxPos - nMinNeg + 1; // parameters for: x^nMinNeg, .. x^-1, x^0, x^1, .. x^(nMaxPos-1) AND for the pT at which the derivative cancels. The coefficient for x^nMaxPos is fixed by the constraint on the derivative.

   if( (parNames.GetEntries() != parVstart.GetEntries()) || (parNames.GetEntries() != parStep.GetEntries()) || (parNames.GetEntries() != parStatusList.GetEntries()) || (parNames.GetEntries() != nPar) ){
     m_logger << kFATAL << "Problem in the number of entries in the fit conditions !!!" << Endl;
   }

   m_namesParFit = new TString[ parNames.GetEntries() ];
   for (Int_t i=0; i<parStatusList.GetEntries(); i++) {
     m_namesParFit[i] = TString( parNames[i] );
   }

   Double_t arglist[ 2 * nPar ];
   Int_t ierflg = 0;
   Int_t nParFree = nPar;

   TMinuit *gMinuit_ = new TMinuit(nPar);  //initialize TMinuit with a maximum of nPar parameters
   gMinuit_->SetFCN( &(fcnFitJES) );

   arglist[0] = 1;
   gMinuit_->mnexcm("SET ERR", arglist ,1,ierflg);

   for( Int_t i=0; i<nPar; i++){
      gMinuit_->mnparm(i, m_namesParFit[i], (parVstart[i]).Atof(), (parStep[i]).Atof(), 0,0,ierflg);
   }
   for( Int_t i=0; i<nPar; i++ ){
      if( parStatusList[i] == "FIXED" ){
         gMinuit_->FixParameter(i);
         nParFree--; // update the number of effective parameters
         ////gMinuit_->mnfixp(1, ierflg);
      }
      else{ // sanity check
         if( parStatusList[i] != "FREE" ){
            cout << "Problem in parStatusList[" << i << "]" << endl;
            exit(1);
         }
      }
   }

   arglist[0] = 30000;
   arglist[1] = 0.001;
   gMinuit_->mnexcm("MIGRAD", arglist, 2, ierflg);
   //  gMinuit_->mnexcm("MINOS", arglist, 2, ierflg);  // --> calls function with nPar-1 parameters?

   if( ierflg ){ m_logger << kWARNING << "Problem in fit convergence --> Flag: " << ierflg << Endl; }

   // results of the fit
   Double_t *_par = new Double_t[nPar], _parErr[nPar];
   for( Int_t i=0; i<nPar; i++){
      gMinuit_->GetParameter( i, _par[i], _parErr[i] );
   }
   Double_t chi2, chi2WithCorrel, *gin=0;
   Int_t Ndof = m_toyXsecValuesFit_EminEmax_st.size() - nParFree;

   fcnFitJES( nPar, gin, chi2, _par, ierflg );
   useCorrelInChi2 = 1;
   fcnFitJES( nPar, gin, chi2WithCorrel, _par, ierflg );
   useCorrelInChi2 = 0;

   //  cout << "The JES fit was performed in the energy interval: [ " << eMinJESfit << " ; " << eMaxJESfit << " ] " << endl;
   cout << "Fit status: " << ierflg << endl;
   parFit_ = new TVectorD(nPar);
   for( Int_t i=0; i<nPar; i++){
      cout << " _par[" << i << "] : " << m_namesParFit[i] << " :  " << _par[i] << endl;
      (*parFit_)[i] = _par[i];
   }
   cout << "  Chi2 : " << chi2 << " / " << Ndof << "(DOF)"<< endl;
   cout << "  Chi2 With Correlations : " << chi2WithCorrel << " / " << Ndof << "(DOF)"<< endl;

   Int_t Nbins_ = m_smallBinsEnergies.size();
   Double_t binLimits_[Nbins_+1];
   binLimits_[0] = m_smallBinsEnergies.at(0)->GetMin();
   for( Int_t i=0; i < Nbins_; i++){
      binLimits_[i+1] = m_smallBinsEnergies.at(i)->GetMax();
   }

   fitH_JES_ = BuildHistogram_f( &(JESfitFunction), _par, nPar, Nbins_, binLimits_ );

   delete gMinuit_;
}


void ComplexMergedMeasurement::ComputeComplexAverage( UInt_t nToys, Channel* channel, const std::vector<TString*>& syst_names, TRandom3* RG, const std::vector< std::vector<Bool_t> >& _mCorrMeasurementsChannel ){

  // contributions of the toy measurements to the bins
  std::vector<TMatrixD*> toyValuesNOf, toyValues, toyValuesStat, toyValuesSyst;
  std::vector< std::vector<Double_t> > toyXsecValuesNOf, toyXsecValues, toyXsecValuesStat, toyXsecValuesSyst;
  TVectorD *errorCorrection_bin = new TVectorD( GetBinningSmall().size() );

  // time the toy generating loop
  Timer timeNOf( nToys, " Timer" );
  // generate toys again
  m_logger << kINFO << " The complex Averaging will compute the complex merged measurement for each bin: NO ERROR( for central value )" << Endl;
  // generate the toy contributions to the bins (with NO fluctuations, NO intermediate large splines)
  GenerateToys( toyValuesNOf, toyXsecValuesNOf, 15., 10000., 1, channel, syst_names, RG, timeNOf, 0, 0, 0 );
  // compute the averages
  ComputeComplexAverage( 1, toyValuesNOf, timeNOf, errorCorrection_bin, 0, 0, _mCorrMeasurementsChannel, syst_names ); // errorCorrection_bin is dummy here
  m_logger << Endl;

  TH1D *fitH_JES=0;
  TVectorD *parFitJES=0;
  for( UInt_t i=0; i<toyXsecValuesNOf.size(); i++ ){
     m_toyXsecValuesFit_EminEmax_st = toyXsecValuesNOf.at(i);

     /*
   FILE* f1 = fopen( "../data/ComplexMergerResults_JES/correctionInsituPoints.txt", "w" );
   FILE* f2 = fopen( "../data/ComplexMergerResults_JES/totalCovMat.txt", "w" );
   for(UInt_t i=0; i<m_toyXsecValuesFit_EminEmax_st.size(); i++ ){
      fprintf( f1, "%E  %E\n", m_toyXsecBinCenters_EminEmax_st.at(i), m_toyXsecValuesFit_EminEmax_st.at(i) );
      for(UInt_t j=0; j<m_toyXsecValuesFit_EminEmax_st.size(); j++ ){
         fprintf( f2, "%E\n", (*m_covMatrixXsecValues)[i][j] );
      }
   }
   fclose( f1 );
   fclose( f2 );
     */

     if( m_performFit ){
       fitJES( fitH_JES, parFitJES );

       FILE* f1 = fopen( "../data/ComplexMergerResults_JES/JESfitResult.txt", "w" );
       for( Int_t i=0; i<fitH_JES->GetNbinsX(); i++ ){
	 fprintf( f1, "%E  %E   %E\n", fitH_JES->GetBinLowEdge(i+1), fitH_JES->GetBinLowEdge(i+1)+fitH_JES->GetBinWidth(i+1), fitH_JES->GetBinContent(i+1) );
       }
       fclose( f1 );
     }
  }

  // delete the toyValuesNOf matrix
  for( UInt_t i=0; i<toyValuesNOf.size(); i++ ){
     delete toyValuesNOf.at(i);
     toyXsecValuesNOf.at(i).clear();
  }
  toyValuesNOf.clear();
  toyXsecValuesNOf.clear();


  // time the toy generating loop
  Timer time( nToys, " Timer" );
  // generate toys again
  m_logger << Endl << Endl << kINFO << " The complex Averaging will compute the complex merged measurement for each bin: TOTAL ERROR" << Endl;
  // generate the toy contributions to the bins (with complete fluctuations, NO intermediate large splines)
  GenerateToys( toyValues, toyXsecValues, 15., 10000., nToys, channel, syst_names, RG, time, 1, 0, 1 ); 
  // compute the averages; as well as the errorCorrection_bin vector
  ComputeComplexAverage( nToys, toyValues, time, errorCorrection_bin, 1, 1, _mCorrMeasurementsChannel, syst_names ); 
  m_logger << Endl;

  if( m_performFit ){
    TMatrixD* fitCovMat = new TMatrixD( fitH_JES->GetNbinsX(), fitH_JES->GetNbinsX() );
    for( Int_t i=0; i < fitH_JES->GetNbinsX(); i++ ){
      for( Int_t j=0; j < fitH_JES->GetNbinsX(); j++ ){
        (*fitCovMat)[i][j] = 0.;
      }
    }
    TMatrixD* parFitJES_CovMat_toys = new TMatrixD( parFitJES->GetNoElements(), parFitJES->GetNoElements() );
    for( Int_t i=0; i < parFitJES->GetNoElements(); i++ ){
      for( Int_t j=0; j < parFitJES->GetNoElements(); j++ ){
        (*parFitJES_CovMat_toys)[i][j] = 0.;
      }
    }
    std::vector<TVectorD*> parFitJES_toysV;
    for( Int_t i=0; i<parFitJES->GetNoElements(); i++){
      parFitJES_toysV.push_back( new TVectorD(nToys) );
    }
    for( UInt_t iToy=0; iToy<toyXsecValues.size(); iToy++ ){
      m_toyXsecValuesFit_EminEmax_st = toyXsecValues.at(iToy);

      TH1D *fitH_JES_toy=0;
      TVectorD *parFitJES_toy=0;
      fitJES( fitH_JES_toy, parFitJES_toy );

      for( Int_t i=0; i < fitH_JES->GetNbinsX(); i++ ){
        for( Int_t j=0; j < fitH_JES->GetNbinsX(); j++ ){
	  (*fitCovMat)[i][j] += (fitH_JES_toy->GetBinContent(i+1) - fitH_JES->GetBinContent(i+1)) * (fitH_JES_toy->GetBinContent(j+1) - fitH_JES->GetBinContent(j+1)) /nToys;
        }
      }
      for( Int_t i=0; i < parFitJES->GetNoElements(); i++ ){
        for( Int_t j=0; j < parFitJES->GetNoElements(); j++ ){
	  (*parFitJES_CovMat_toys)[i][j] += ((*parFitJES_toy)[i] - (*parFitJES)[i]) * ((*parFitJES_toy)[j] - (*parFitJES)[j]) /nToys;
        }
        (*(parFitJES_toysV.at(i)))[iToy] = (*parFitJES_toy)[i];
      }
      delete fitH_JES_toy;
      delete parFitJES_toy;
    }
    FILE* fCM = fopen( "../data/ComplexMergerResults_JES/JESfitResult_CovMat.txt", "w" );
    for( Int_t i=0; i < fitH_JES->GetNbinsX(); i++ ){
      for( Int_t j=0; j < fitH_JES->GetNbinsX(); j++ ){
        fprintf( fCM, "%E\n", (*fitCovMat)[i][j] );
      }
    }
    fclose( fCM );
    for( Int_t i=0; i<parFitJES->GetNoElements(); i++){
      Int_t Npos_i_=0, Nneg_i_=0;
      Double_t sigmaPos_i_=0., sigmaNeg_i_=0.;
      Utils::ComputeAsymmetricUncertainties( nToys, parFitJES_toysV.at(i), (*parFitJES)[i], Npos_i_, Nneg_i_, sigmaPos_i_, sigmaNeg_i_ );
      m_logger << kINFO << " _par[" << i << "] : " << m_namesParFit[i] << " :  " << (*parFitJES)[i] << " +- " << sqrt( (*parFitJES_CovMat_toys)[i][i] ) << " (toys)  ( + " 
	       << sigmaPos_i_ << " (Npos_i_ = " << Npos_i_ << ")  - " << sigmaNeg_i_ << " (Nneg_i_ = " << Nneg_i_ << ") ) "<< Endl;
      delete parFitJES_toysV.at(i);
    }
    m_logger << kINFO << "Covariance matrix(toys) of JES fit parameters: " << Endl;
    parFitJES_CovMat_toys->Print("all");
    const TMatrixD* parFitJES_CorrMat_toys = Utils::GetCorrelationMatrix( *parFitJES_CovMat_toys, m_logger );
    m_logger << kINFO << "Correlation matrix(toys) of JES fit parameters: " << Endl;
    parFitJES_CorrMat_toys->Print("all");
  }

  // delete the toyValues matrix
  for( UInt_t i=0; i<toyValues.size(); i++ ){
     delete toyValues.at(i);
     toyXsecValues.clear();
  }
  toyValues.clear();
  toyXsecValues.clear();

  // time the toy generating loop
  Timer timeStat( nToys, " Timer" );
  m_logger << Endl << Endl << " The complex Averaging will compute the complex merged measurement for each bin: STATISTICAL ERRORS ONLY" << Endl;
  // generate the toy contributions to the bins (with stat error fluctuations, NO intermediate large splines)
  GenerateToys( toyValuesStat, toyXsecValuesStat, 15., 10000., nToys, channel, syst_names, RG, timeStat, 1, 0, 0 );
  // compute the averages
  ComputeComplexAverage( nToys, toyValuesStat, timeStat, errorCorrection_bin, 1, 0, _mCorrMeasurementsChannel, syst_names );
  m_logger << Endl;

  // delete the toyValuesStat matrix
  for( UInt_t i=0; i<toyValuesStat.size(); i++ ){
     delete toyValuesStat.at(i);
     toyXsecValuesStat.clear();
  }
  toyValuesStat.clear();
  toyXsecValuesStat.clear();


  // time the toy generating loop
  Timer timeSyst( nToys, " Timer" );
  m_logger << Endl << Endl << " The complex Averaging will compute the complex merged measurement for each bin: SYSTEMATIC ERRORS ONLY" << Endl;
  // generate the toy contributions to the bins (with syst error fluctuations, NO intermediate large splines)
  GenerateToys( toyValuesSyst, toyXsecValuesSyst, 15., 10000., nToys, channel, syst_names, RG, timeSyst, 0, 0, 1 );
  // compute the averages
  ComputeComplexAverage( nToys, toyValuesSyst, timeSyst, errorCorrection_bin, 0, 1, _mCorrMeasurementsChannel, syst_names );
  m_logger << Endl;

  // delete the toyValuesSyst matrix
  for( UInt_t i=0; i<toyValuesSyst.size(); i++ ){
     delete toyValuesSyst.at(i);
     toyXsecValuesSyst.clear();
  }
  toyValuesSyst.clear();
  toyXsecValuesSyst.clear();
  m_logger << Endl << Endl;

  gStore().AssertVariable( "ComplexMerger::SaveAllSystNuisanceParameters" );
  Bool_t SaveAllSystNuisanceParameters = gStore().GetVariable( "ComplexMerger::SaveAllSystNuisanceParameters" )->GetBoolValue();
  if(SaveAllSystNuisanceParameters){
     m_logger << kINFO << "Saving nuisance parameters for ALL the systematics..." << Endl;

     // time the toy generating loop
     Timer timeSystMeasurement( syst_names.size(), " Timer" );
     TMatrixD* parFitJES_CovMat_shifts = 0;
     if( m_performFit ){
       parFitJES_CovMat_shifts = new TMatrixD( parFitJES->GetNoElements(), parFitJES->GetNoElements() );
       for( Int_t i=0; i < parFitJES->GetNoElements(); i++ ){
	 for( Int_t j=0; j < parFitJES->GetNoElements(); j++ ){
	   (*parFitJES_CovMat_shifts)[i][j] = 0.;
	 }
       }
     }
     for( UInt_t is=0; is<syst_names.size(); is++ ){ 
        // generate toys again
        std::vector<TMatrixD*> toyValuesSystMeasurement;
        std::vector< std::vector<Double_t> > toyXsecValuesSystMeasurement;
        m_logger << kINFO << " The complex Averaging will compute the complex merged measurement for each bin: ONE systematic is fluctuated (to measure its effect on the final result): " << *(syst_names.at(is)) << Endl;
        // generate the toy contributions to the bins (with NO fluctuations, NO intermediate large splines)
        GenerateToys( toyValuesSystMeasurement, toyXsecValuesSystMeasurement, 15., 10000., 1, channel, syst_names, RG, timeSystMeasurement, 0, 0, 0, is );
        // compute the averages
        ComputeComplexAverage( 1, toyValuesSystMeasurement, timeSystMeasurement, errorCorrection_bin, 0, 0, _mCorrMeasurementsChannel, syst_names, is ); 

	if( m_performFit ){
	  for( UInt_t iToy=0; iToy<toyXsecValuesSystMeasurement.size(); iToy++ ){ // TODO: replace loop by sanity check (only one toy was generated here)
	    m_toyXsecValuesFit_EminEmax_st = toyXsecValuesSystMeasurement.at(iToy);

	    TH1D *fitH_JES_shifted;
	    TVectorD *parFitJES_shifted=0;
	    fitJES( fitH_JES_shifted, parFitJES_shifted );

	    FILE* f1 = fopen( "../data/ComplexMergerResults_JES/JESfitResult_NP_" + (*(syst_names.at(is))) + ".txt", "w" );
	    for( Int_t i=0; i<fitH_JES_shifted->GetNbinsX(); i++ ){
              fprintf( f1, "%E  %E   %E\n", fitH_JES_shifted->GetBinLowEdge(i+1), fitH_JES_shifted->GetBinLowEdge(i+1)+fitH_JES_shifted->GetBinWidth(i+1), fitH_JES_shifted->GetBinContent(i+1) - fitH_JES->GetBinContent(i+1) );
	    }
	    fclose( f1 );
	    cout << "Nuisance parameter " << *(syst_names.at(is)) << ": (  ";
	    for( Int_t i=0; i < parFitJES->GetNoElements(); i++ ){
              for( Int_t j=0; j < parFitJES->GetNoElements(); j++ ){
		(*parFitJES_CovMat_shifts)[i][j] += ((*parFitJES_shifted)[i] - (*parFitJES)[i]) * ((*parFitJES_shifted)[j] - (*parFitJES)[j]);
              }
              cout << m_namesParFit[i] << " : " << (*parFitJES_shifted)[i] - (*parFitJES)[i] << " ;  ";
	    }
	    cout << "  ) "<< endl;
	    delete fitH_JES_shifted;
	    delete parFitJES_shifted;
	  }
	}

        // delete the toyValuesSystMeasurement matrix
        for( UInt_t i=0; i<toyValuesSystMeasurement.size(); i++ ){
           delete toyValuesSystMeasurement.at(i);
           toyXsecValuesSystMeasurement.clear();
        }
        toyValuesSystMeasurement.clear();
        toyXsecValuesSystMeasurement.clear();
     }
     if( m_performFit ){
       for( Int_t i=0; i<parFitJES->GetNoElements(); i++){
	 m_logger << kINFO << " _par[" << i << "] : " << m_namesParFit[i] << " :  " << (*parFitJES)[i] << " +- " << sqrt( (*parFitJES_CovMat_shifts)[i][i] ) << " (shifts) " << Endl;
       }
       m_logger << kINFO << "Covariance matrix(shifts) of JES fit parameters: " << Endl;
       parFitJES_CovMat_shifts->Print("all");
       const TMatrixD* parFitJES_CorrMat_shifts = Utils::GetCorrelationMatrix( *parFitJES_CovMat_shifts, m_logger );
       m_logger << kINFO << "Correlation matrix(shifts) of JES fit parameters: " << Endl;
       parFitJES_CorrMat_shifts->Print("all");
     }
  }
  if( m_performFit ){
    delete fitH_JES;
    delete parFitJES;
  }

  m_logger << kINFO << "Done! for saving nuisance parameters for the systematics." << Endl;

  FILE *f1, *f2, *f3, *f4, *f5, *f6, *f7;
  f1 = fopen( "../data/ComplexMergerResults_JES/SpectralFunction.txt", "w" );
  f2 = fopen( "../data/ComplexMergerResults_JES/Binning.txt", "w" );
  f3 = fopen( "../data/ComplexMergerResults_JES/CovMatrix.txt", "w" );
  f4 = fopen( "../data/ComplexMergerResults_JES/DiagError.txt", "w" );
  f5 = fopen( "../data/ComplexMergerResults_JES/CovMatrixStat.txt", "w" );
  f6 = fopen( "../data/ComplexMergerResults_JES/CovMatrixSyst.txt", "w" );
  f7 = fopen( "../data/ComplexMergerResults_JES/SqrtChiDdof.txt", "w" );

  for(UInt_t i=0; i<GetDataVec().size(); i++ ){ 
    if( i>0 && fabs( (GetDataVec().at(i))->Emin() - (GetDataVec().at(i-1))->Emax() )>1.e-5 ){ 
      m_logger << kWARNING << " NOT COVERED ENERGY INTERVAL: " << (GetDataVec().at(i-1))->Emax() << " - " << (GetDataVec().at(i))->Emin() << Endl; 
    }
    fprintf( f1, "%E \n", (GetDataVec().at(i))->Xsec() );
    fprintf( f2, "%E      %E\n", (GetDataVec().at(i))->Emin(), (GetDataVec().at(i))->Emax() );
    for(UInt_t j=0; j<GetDataVec().size(); j++ ){
      fprintf( f3, "%E\n", (*m_covMatXSec)[i][j] );
      fprintf( f5, "%E\n", (*m_covMatXSecStat)[i][j] );
      fprintf( f6, "%E\n", (*m_covMatXSecSyst)[i][j] );
    }
    fprintf( f4, "%E\n", TMath::Sqrt( (*m_covMatXSec)[i][i] ) );
  }
  for(UInt_t i=0; i<GetBinningSmall().size(); i++ ){
    fprintf( f7, "%E\n", (*errorCorrection_bin)[i] );
  }
  fclose( f1 );
  fclose( f2 );
  fclose( f3 );
  fclose( f4 );
  fclose( f5 );
  fclose( f6 );
  fclose( f7 );

  delete errorCorrection_bin;
  m_logger << "DONE for the ComplexMerger!" << Endl;

}

void ComplexMergedMeasurement::CreateHBDHistogram( Channel* channel )
{
  // move to channel's directory
  //channel->GetLocalDir().cd();

  gStore().AssertVariable( "ComplexMerger::AveNPointsPFinalBin" );
  UInt_t nPointsPFinalBin = gStore().GetVariable( "ComplexMerger::AveNPointsPFinalBin" )->GetIntValue();

  TH1 *refH, *aveH, *rmsH;

  // create graph for this measurement (graph is owned (and destroyed) by measurement)
  TGraph* graph = this->CreateGraph( kFALSE );

  rmsH = new TH1F( "_ComplexMerger", "_ComplexMerger", nPointsPFinalBin*graph->GetN(), graph->GetXaxis()->GetXmin(), graph->GetXaxis()->GetXmax() );
  aveH = Utils::GetHistogramFromGraph( "_ComplexMerger", "_ComplexMerger", *graph, nPointsPFinalBin, rmsH );
  refH = aveH;
  
  // create high-binned histogram 
  AddHist( new GenHist( channel->GetRegexpName()+"_ComplexMerger", channel->GetName()+" ComplexMerger", *refH, *aveH, *rmsH ) );
  //genHist->Write();

}
