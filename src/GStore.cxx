// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iomanip>

#include "TMath.h"

#include "GStore.h"
#include "Utils.h"
#include "Action.h"
#include "Variable.h"


GStore& gStore() 
{ 
   return *GStore::Instance(); 
}

GStore* GStore::m_instance = 0;

GStore* GStore::Instance()
{
   if (m_instance == 0) m_instance = new GStore();
   return m_instance;
}

GStore* GStore::InitialInstance()
{
   return m_instance;
}

GStore::GStore()
   : GBase( "GStore" ),
     m_msgLevel( kINFO )
{
   m_variables.clear();
   m_actions  .clear();
}

GStore::~GStore()
{
   // need to du full cleanup !
   this->Clear();
}

void GStore::Clear()
{
   m_variables.clear();
}

void GStore::AddAction( TString action, std::vector<TString>& args ) 
{ 
   TString str = TString("\"") + action + "\"";
   TString btr = "";
   for (UInt_t i=0; i<args.size(); i++) { 
      if (i>0) btr += ":";
      btr += args[i];
   }
   m_logger << kINFO <<std::left << "Add action: " << std::setw(TMath::Max(20,str.Sizeof())) << str
            << "(argument(s): \"" << btr << "\")" << Endl;

   m_actions.push_back( new Action( action, args ) );
}

void GStore::AddAction( const Action* action )
{ 
   m_logger << kINFO <<std::left << "Add action: " 
            << std::setw(TMath::Max(20,action->GetName().Sizeof())) << action->GetName()
            << "(argument(s): \"" << action->GetArgStr() << "\")" << Endl;

   m_actions.push_back( action );
}

const Action* GStore::GetAction( const TString& action )
{
   std::vector<const Action*>::const_iterator it = GetActions().begin();
   for (; it != gStore().GetActions().end(); it++) {
      if ((*it)->GetName() == action) return *it;
   }
   m_logger << kFATAL << "<GetAction> Could not find action \"" << action << "\"" << Endl;

   return 0;
}

void GStore::AssertVariable( const TString& key )
{
   if (!ExistVariable( key )) {
      m_logger << kFATAL << "<AssertVariable> Variable \"" << key 
               << "\" is not defined in driving card" << Endl;
   }
}

Bool_t GStore::ExistVariable( const TString& key )
{
   return (m_variables[key] != 0) ? kTRUE : kFALSE;
}

const Variable* GStore::GetVariable( const TString& key ) 
{
   // slow search; if this is critical would need hash table
   // don't allow to retrieve unknown object
   const Variable* f = m_variables[key];
   if (f == 0) {
      m_logger << kFATAL << "<GetVariable> Variable for key : \"" << key 
               << "\" does not exist" << Endl;
   }
   return f;
}

const Variable* GStore::GetVariableIfExist( const TString& key ) 
{
   // slow search; if this is critical would need hash table
   return m_variables[key];
}

void GStore::AddVariable( const Variable* variable ) 
{ 
   if (variable == 0) m_logger << kFATAL << "<AddVariable> Cannot add zero pointer"  << Endl;

   TString str = TString("\"") + variable->GetVariableName() + "\"";
   m_logger << kINFO <<std::left << "Add \"" << variable->GetStrType() 
            << "\" variable: " << std::setw(TMath::Max(25,str.Sizeof())) << str
            << " =  \"" << variable->GetValue() << "\"" << Endl;

   m_variables[variable->GetVariableName()] = variable;
}

void GStore::AddVariable( TString key, TString value ) 
{ 
   // no blanks in string
   Utils::CleanString( key );
   Utils::CleanString( value );

   this->AddVariable( new const Variable( key, value ) );
}

void GStore::Print( std::ostream& o ) const
{
   o << "GStore contains the following variables:" << std::endl
     << "----------------------------------------" << std::endl;      

   for (std::map<const TString, const Variable*>::const_iterator varIt = GetVariables().begin(); 
        varIt != GetVariables().end(); varIt++) varIt->second->Print( o );
   o << std::endl;
}
