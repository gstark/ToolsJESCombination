// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iostream>
#include <iomanip>

#include "TObjArray.h"
#include "TObjString.h"
#include "TString.h"
#include "TMath.h"
#include "TComplex.h"
#include "TList.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TH1.h"
#include "TGraph.h"

#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"

#include "Utils.h"
#include "GConfig.h"
#include "MsgLogger.h"
#include "Interval.h"

#include "TSplineBase.h"
#include "TSplineFactory.h"

using std::cout;
using std::endl;

TList* Utils::ParseFormatLine( const TString& formatString, const TString& separator )
{
   // Parse the string and cut into labels separated by "separator"
   // NOTE: if separater==':', do not separate "::" occurrences 
   TList*   labelList = new TList();
   TString* label     = new TString();
   Int_t    nLabels   = 0;

   const Int_t n = (Int_t)formatString.Length();
   TObjString** label_obj = new TObjString*[n];  // array of labels

   for (Int_t i=0; i<n; i++) {
      label->Append(formatString(i));
      Bool_t found = (formatString(i) == separator);
      if (found && separator == ':') {
         if (i < n-1) found = found && formatString(i+1) != ':';
         if (i > 0  ) found = found && formatString(i-1) != ':';
      }
      if (found) {
         label->Chop();
         label_obj[nLabels] = new TObjString(label->Data());
         labelList->Add(label_obj[nLabels]);
         label->Resize(0);
         nLabels++;
      }
      if (i == n-1) {
         label_obj[nLabels] = new TObjString(label->Data());
         labelList->Add(label_obj[nLabels]);
         label->Resize(0);
         nLabels++;
      }
   }
   delete label;
   delete [] label_obj;
   return labelList;
}

void Utils::FormattedOutput( const std::vector<Double_t>& values, const std::vector<TString>& V, 
                             const TString titleVars, const TString titleValues, MsgLogger& logger,
                             TString format )
{
   // formatted output of simple table

   // sanity check
   UInt_t nvar = V.size();
   if ((UInt_t)values.size() != nvar) {
      logger << kFATAL << "<FormattedOutput> fatal error with dimensions: " 
             << values.size() << " OR " << " != " << nvar << Endl;
   }

   // find maximum length in V (and column title)
   UInt_t maxL = 7;
   std::vector<UInt_t> vLengths;
   for (UInt_t ivar=0; ivar<nvar; ivar++) maxL = TMath::Max( (UInt_t)V[ivar].Length(), maxL );
   maxL = TMath::Max( (UInt_t)titleVars.Length(), maxL );

   // column length
   UInt_t maxV = 7;
   maxV = TMath::Max( (UInt_t)titleValues.Length() + 1, maxL );

   // full column length
   UInt_t clen = maxL + maxV + 3;

   // bar line
   for (UInt_t i=0; i<clen; i++) logger << "-";
   logger << Endl;

   // title bar   
   logger << std::setw(maxL) << titleVars << ":";
   logger << std::setw(maxV+1) << titleValues << ":";
   logger << Endl;
   for (UInt_t i=0; i<clen; i++) logger << "-";
   logger << Endl;

   // the numbers
   for (UInt_t irow=0; irow<nvar; irow++) {
      logger << std::setw(maxL) << V[irow] << ":";
      logger << std::setw(maxV+1) << Form( format.Data(), values[irow] );
      logger << Endl;
   }

   // bar line
   for (UInt_t i=0; i<clen; i++) logger << "-";
   logger << Endl;
}

void Utils::FormattedOutput( const TMatrixD& M, const std::vector<TString>& V, MsgLogger& logger )
{
   // formatted output of matrix (with labels)

   // sanity check: matrix must be quadratic
   UInt_t nvar = V.size();
   if ((UInt_t)M.GetNcols() != nvar || (UInt_t)M.GetNrows() != nvar) {
      logger << kFATAL << "<FormattedOutput> fatal error with dimensions: " 
             << M.GetNcols() << " OR " << M.GetNrows() << " != " << nvar << " ==> abort" << Endl;
   }

   // get length of each variable, and maximum length  
   UInt_t minL = 7;
   UInt_t maxL = minL;
   std::vector<UInt_t> vLengths;
   for (UInt_t ivar=0; ivar<nvar; ivar++) {
      vLengths.push_back(TMath::Max( (UInt_t)V[ivar].Length(), minL ));
      maxL = TMath::Max( vLengths.back(), maxL );
   }
   
   // count column length
   UInt_t clen = maxL+1;
   for (UInt_t icol=0; icol<nvar; icol++) clen += vLengths[icol]+1;

   // bar line
   for (UInt_t i=0; i<clen; i++) logger << "-";
   logger << Endl;

   // title bar   
   logger << std::setw(maxL+1) << " ";
   for (UInt_t icol=0; icol<nvar; icol++) logger << std::setw(vLengths[icol]+1) << V[icol];
   logger << Endl;

   // the numbers
   for (UInt_t irow=0; irow<nvar; irow++) {
      logger << std::setw(maxL) << V[irow] << ":";
      for (UInt_t icol=0; icol<nvar; icol++) {
         logger << std::setw(vLengths[icol]+1) << Form( "%+1.3f", M(irow,icol) );
      }      
      logger << Endl;
   }

   // bar line
   for (UInt_t i=0; i<clen; i++) logger << "-";
   logger << Endl;
}

// computes asymmetric uncertainties from distribution of toys
void Utils::ComputeAsymmetricUncertainties( Int_t Ntoy_, TVectorD *toy_vec_, Double_t nominal_, Int_t &Npos_, Int_t &Nneg_, Double_t &sigmaPos_, Double_t &sigmaNeg_ ){

   if( toy_vec_->GetNoElements() != Ntoy_ ){
      cout << "Problem in the number of toys!!! " << toy_vec_->GetNoElements() << " " << Ntoy_ << endl;
      exit(1);
   }

   Npos_=0; Nneg_=0;
   sigmaPos_ = 0.; sigmaNeg_ = 0.;
   for( Int_t toy=0; toy<Ntoy_; toy++ ){
      if( (*toy_vec_)[toy] <= nominal_ ){
         Nneg_++;
         sigmaNeg_ += pow( (*toy_vec_)[toy] - nominal_, 2 );
      }
      else{
         Npos_++;
         sigmaPos_ += pow( (*toy_vec_)[toy] - nominal_, 2 );
      }
   }
   sigmaNeg_ = sqrt( sigmaNeg_ / Nneg_ );
   sigmaPos_ = sqrt( sigmaPos_ / Npos_ );
}

// turn covariance matrix into correlation matrix
const TMatrixD* Utils::GetCorrelationMatrix( const TMatrixD& covMat, MsgLogger& logger )
{
   // turns covariance into correlation matrix   

   // sanity check
   Int_t nvar = covMat.GetNrows();
   if (nvar != covMat.GetNcols()) 
      logger << kFATAL << "<GetCorrelationMatrix> input matrix not quadratic" << Endl;
   
   TMatrixD* corrMat = new TMatrixD( nvar, nvar );

   for (Int_t ivar=0; ivar<nvar; ivar++) {
      for (Int_t jvar=0; jvar<nvar; jvar++) {
         if (ivar != jvar) {
            Double_t d = covMat(ivar, ivar)*covMat(jvar, jvar);
            if (d > 0) (*corrMat)(ivar, jvar) = covMat(ivar, jvar)/TMath::Sqrt(d);
            else {
               logger << kWARNING << "<GetCorrelationMatrix> zero variances for variables "
                       << "(" << ivar << ", " << jvar << ")" << Endl;
               (*corrMat)(ivar, jvar) = 0;
            }
         }
         else (*corrMat)(ivar, ivar) = 1.0;
      }
   }

   return corrMat;
}

// generate vector of "size" uncorrelated Gaussian-distributed random numbers of mean = 0, sigma = 1
void Utils::GenGaussRnd( TArrayD& v, const Int_t size, TRandom3& R ) 
{
  // sanity check
  if (size != v.GetSize()) 
    {
      cout << "<GenGaussRnd2> Too short input vector: " << size << " " << v.GetSize() << endl;
      exit(1);
    }

  for (Int_t i=0; i<size; i++) {
       v[i] = R.Gaus( 0., 1. );
   }

}

void Utils::SelectCoeff( TArrayD& coeffMeasurement, std::vector<TString*>& systMeasurement, const TArrayD& coeff, const std::vector<TString*>& syst )
{
  // sanity check
  if( (UInt_t)coeffMeasurement.GetSize() != systMeasurement.size() )
    {
      cout << "ERROR: the size of the vector of the names of the systematics in the measurement " << systMeasurement.size() 
           << " is different of the size of the array for the coefficients of the measurement" << coeffMeasurement.GetSize() << endl;
      exit(1);
    }
  if( (UInt_t)coeff.GetSize() != syst.size() )
    {
      cout << "ERROR: the size of the vector of the names of the systematics in the channel " << syst.size() 
           << " is different of the size of the array for the coefficients " << coeff.GetSize() << endl;
      exit(1);
    }

  // iterate on the names of the systematic errors of the measurement and search them in the vector of the channel
  for( UInt_t i=0; i<systMeasurement.size(); i++ )
    {
      Bool_t found = kFALSE;
      for( UInt_t j=0; j<syst.size(); j++ )
        {
           if( ( *(systMeasurement.at(i)) ) == ( *(syst.at(j)) ) )
            {
              // sanity check
              if( found )
                {
                  cout << "ERROR: multiple occurence of " << systMeasurement[i] << "in the vector of names of the systematics in the channel " << endl;
                  exit(1);
                }

              found = kTRUE;
              coeffMeasurement[i] = coeff[j];
            }
        }

      // sanity check
      if( !found )
        {
          cout << "ERROR: " << systMeasurement[i] << " was not found in the vector of names of the systematics in the channel " << endl;
          exit(1);
        }
    }
}

Int_t Utils::BinarySrc(Int_t n, const Double_t *array, Double_t value)
{
   // Binary search in an array of n values to locate value.
   //
   // Array is supposed  to be sorted prior to this call.
   // If match is found within eps, function returns position of element.
   // If no match found, function gives nearest element smaller than value.

  Double_t eps = 1e-12;

   Int_t nabove, nbelow, middle;
   nabove = n+1;
   nbelow = 0;
   while(nabove-nbelow > 1) {
      middle = (nabove+nbelow)/2;
      if ( (value < array[middle-1] + eps) && (value > array[middle-1] - eps) ) return middle-1;
      if (value  < array[middle-1]) nabove = middle;
      else                          nbelow = middle;
   }
   return nbelow-1;
}

Int_t Utils::BinarySrc(Int_t n, const std::vector<Double_t>& array, Double_t value)
{
   // Binary search in an array of n values to locate value.
   //
   // Array is supposed  to be sorted prior to this call.
   // If match is found within eps, function returns position of element.
   // If no match found, function gives nearest element smaller than value.

  Double_t eps = 1e-12;

   Int_t nabove, nbelow, middle;
   nabove = n+1;
   nbelow = 0;
   while(nabove-nbelow > 1) {
      middle = (nabove+nbelow)/2;
      if ( (value < array.at(middle-1) + eps) && (value > array.at(middle-1) - eps) ) return middle-1;
      if (value  < array[middle-1]) nabove = middle;
      else                          nbelow = middle;
   }
   return nbelow-1;
}


TH1* Utils::GetHistogramFromGraph( const TString& name, const TString& title, const TGraph& graph, Int_t nbin )
{
   // create (possibly high-binned) histogram and fill from graph
   TH1* h = new TH1F( name, title, nbin, graph.GetXaxis()->GetXmin(), graph.GetXaxis()->GetXmax() );

   for (Int_t ibin=1; ibin<=nbin; ibin++) {
      
      Double_t xval = h->GetBinCenter( ibin );
      Double_t yval = graph.Eval( xval );

      h->SetBinContent( ibin, yval );
   }

   return h;
}

TH1* Utils::GetHistogramFromGraph( const TString& name, const TString& title, const TGraph& graph, const TH1& href )
{
   // create (possibly high-binned) histogram and fill from graph
   TH1* h = new TH1F( name, title, href.GetNbinsX(), href.GetXaxis()->GetXmin(), href.GetXaxis()->GetXmax() );

   for (Int_t ibin=1; ibin<=h->GetNbinsX(); ibin++) {
      
      Double_t xval = h->GetBinCenter( ibin );
      Double_t yval = graph.Eval( xval );

      h->SetBinContent( ibin, yval );
   }

   return h;
}

TH1* Utils::GetHistogramFromGraph( const TString& name, const TString& title, const TGraph& graph, Int_t nBinsPFinalBin, TH1* rms )
{
   Double_t *xArray = graph.GetX();
   // create (possibly high-binned) histogram and fill from graph
   // for the ComplexMergedMeasurement the histogram should cover only the points( anyway, here the graph is supposed to be "continuous" in X )
   TH1* h = new TH1F( name, title, nBinsPFinalBin*graph.GetN(), xArray[0], xArray[graph.GetN()-1] );

   for (Int_t ibin=1; ibin <= nBinsPFinalBin*graph.GetN(); ibin++) {
      
      Double_t xval = h->GetBinCenter( ibin );
      Double_t yval = graph.Eval( xval );

      h->SetBinContent( ibin, yval );

      Int_t pos=BinarySrc( graph.GetN(), xArray, xval );
      Double_t errY;
      if( pos == graph.GetN()-1 ){
        errY = graph.GetErrorY(pos-1) +( xval - xArray[pos-1] ) * (( graph.GetErrorY(pos) - graph.GetErrorY(pos-1) )/( xArray[pos] - xArray[pos-1] ));
      }
      else{
        errY = graph.GetErrorY(pos) +( xval - xArray[pos] ) * (( graph.GetErrorY(pos+1) - graph.GetErrorY(pos) )/( xArray[pos+1] - xArray[pos] ));
      }
      rms->SetBinContent( ibin, errY ); 
   }

   return h;
}

void Utils::CleanString( TString& str )
{
   str.ReplaceAll( " ", "" );   
}

// check if token is in token list
Bool_t Utils::HasToken( const TString& str, const TString& separator )
{
   Bool_t retval = kFALSE;
   TObjArray* toklist = str.Tokenize( separator );   
   for (Int_t i=0; i<toklist->GetEntries(); i++) {
      if (((TObjString*)toklist->At(0))->GetString() == str) { retval = kTRUE; break; }
   }
   delete toklist;

   return retval;
}

Interval* Utils::GetGraphLimits( const TGraph& graph )
{
   return new Interval( graph.GetX()[0], graph.GetX()[graph.GetN()-1] );
}

//_______________________________________________________________________
const TString& Utils::Color( const TString& c ) 
{
   // human readable color strings
   static TString gClr_none         = "" ;
   static TString gClr_white        = "\033[1;37m";  // white
   static TString gClr_black        = "\033[30m";    // black
   static TString gClr_blue         = "\033[34m";    // blue
   static TString gClr_red          = "\033[1;31m" ; // red
   static TString gClr_yellow       = "\033[1;33m";  // yellow
   static TString gClr_darkred      = "\033[31m";    // dark red
   static TString gClr_darkgreen    = "\033[32m";    // dark green
   static TString gClr_darkyellow   = "\033[33m";    // dark yellow
                                    
   static TString gClr_bold         = "\033[1m"    ; // bold 
   static TString gClr_black_b      = "\033[30m"   ; // bold black
   static TString gClr_lblue_b      = "\033[1;34m" ; // bold light blue
   static TString gClr_lgreen_b     = "\033[1;32m";  // bold light green
                                    
   static TString gClr_blue_bg      = "\033[44m";    // blue background
   static TString gClr_red_bg       = "\033[1;41m";  // white on red background
   static TString gClr_whiteonblue  = "\033[1;44m";  // white on blue background
   static TString gClr_whiteongreen = "\033[1;42m";  // white on green background
   static TString gClr_grey_bg      = "\033[47m";    // grey background

   static TString gClr_reset  = "\033[0m";     // reset

   if (!gConfig().UseColor()) return gClr_none;

   if (c == "white" )         return gClr_white; 
   if (c == "blue"  )         return gClr_blue; 
   if (c == "black"  )        return gClr_black; 
   if (c == "lightblue")      return gClr_lblue_b;
   if (c == "yellow")         return gClr_yellow; 
   if (c == "red"   )         return gClr_red; 
   if (c == "dred"  )         return gClr_darkred; 
   if (c == "dgreen")         return gClr_darkgreen; 
   if (c == "lgreenb")        return gClr_lgreen_b;
   if (c == "dyellow")        return gClr_darkyellow; 

   if (c == "bold")           return gClr_bold; 
   if (c == "bblack")         return gClr_black_b; 

   if (c == "blue_bgd")       return gClr_blue_bg; 
   if (c == "red_bgd" )       return gClr_red_bg; 
 
   if (c == "white_on_blue" ) return gClr_whiteonblue; 
   if (c == "white_on_green") return gClr_whiteongreen; 

   if (c == "reset") return gClr_reset; 

   cout << "Unknown color " << c << endl;
   exit(1);

   return gClr_none;
}

void Utils::WelcomeMessage()
{
   // direct output, eg, when starting ROOT session
   cout << endl;
   cout << Utils::Color("bold") << "ToolsJEScombination -- Tools for JES Combination --> derived from HVPTools " 
        << Utils::Color("reset") << endl;
   cout << "            " << "Copyright (C) 2007: CERN, LAL-Orsay, LAPP-Annecy, LPNHE-Paris" << endl;
   cout << "            " << "N. Berger (LAPP), A. Hoecker (CERN), M. Davier (LAL), B. Malaescu (LAL,LPNHE), Z. Zhang (LAL)" << endl;
   cout << endl;
}
