// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef MeasurementBase_h
#define MeasurementBase_h

#include <vector>

#include "TString.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TH1.h"

#include "GBase.h"
#include "Data.h"
#include "GenHist.h"

class TGraph;
class TArrayD;

class Interval;
class StringList;

class MeasurementBase : public GBase {

 public:
      
   MeasurementBase( const TString& channelName, 
                const TString& alias,
                const TString& experiment );
   virtual ~MeasurementBase();
      
   // type identifier
   virtual const TString GetType() const = 0;

   // add data point
   virtual void AddData( Data* ) = 0;

  // add systematic error name: checks if it is not already in the vector
  void AddSystErrName( TString* errName);

  // set or get the evaluation mode of the measurement
  void SetUseSplines( const Bool_t decision ) { m_useSplines = decision; }
  const Bool_t GetUseSplines() const { return m_useSplines; }

  // set or get the type of the measurement (graph or histogram)
  void SetOnlyBins( const Bool_t decision ) { m_onlyBins = decision; }
  const Bool_t GetOnlyBins() const { return m_onlyBins; }

   // accessors
  const TString& GetChannelName    () const { return m_channelName; }
  const TString& GetAlias          () const { return m_alias; }
  const TString& GetExperiment     () const { return m_experiment; }

  // GetCEnergyInterval() is special for the ComplexMerger
  const Interval& GetCEnergyInterval();
  const Double_t GetEmin           () const { return m_data.front()->Emin(); }
  const Double_t GetEmax           () const { return m_data.back()->Emax(); }
  // m_data should containe some data when those functions are called

   // experiment Identifier
   Int_t        GetExpId ()  const { return m_expId; }
   virtual void SetExpId( Int_t id ) { m_expId = id; UpdateData(); } // can be overloaded

   // accessor to data vector
   std::vector<Data*>& GetDataVec() { return m_data; }
  
  // accessor to systematic error names vector
  std::vector<TString*>& GetSystErrNamesAll() { return m_systErrNamesAll; }

   // create graph of data members
   TGraph* CreateGraph( Bool_t writeToFile = kFALSE );

  // add an histogram isued from an analysis
  void AddHist( GenObjectBase<TH1>* hist ) { m_Hist.push_back(hist); }

   // print the class content
   virtual void Print( std::ostream& os );

 protected:

   // update data identifiers
   void UpdateData();
   Int_t      m_expId;

  // get the maximal distance betweem points/bins for wich we can still use splines
  Double_t Get_dMax() { return m_dMax; }

  TMatrixD *m_covMatXSec /* created for each type of measurement or directly from stat and syst */, 
           *m_covMatXSecStat, *m_covMatXSecSyst;

  // vectors of histograms for program auto-analysis
  std::vector< TH1* > m_autoHist_large;

  // chi2 graph
  TGraph*    m_Chi2_graph;

 private:

   // meta data
   TString    m_channelName;
   TString    m_alias;
   TString    m_experiment;

  // method to be used when evaluating the contribution of a UnitMeasurement for a final bin in ComplexMergedMeasurement
  // parameter saying if we may do a splinning for the ComplexMergedMeasurement
  Bool_t m_useSplines;
  // parameter saying if we have only bins or we also have points
  Bool_t m_onlyBins;
  // maximal distance betweem points/bins for wich we can still use splines
  Double_t m_dMax;

   // measurement graphs
   TGraph*    m_XsecGraph;

  // vector of histograms for physical analysis ( ex: high binned histogram for the ComplexMerger etc.)
  std::vector< GenObjectBase<TH1>* > m_Hist; 

  // maximal spline extention beyond the energy limits
  Double_t m_maxRelExpansion;
  // energy interval to be used by the ComplexMerger
  Interval *m_CEnergyInterval;

   // the data points
   std::vector<Data*> m_data;   
 
   // names of ALL systematic errors
   std::vector<TString*> m_systErrNamesAll;

};

#endif
