// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TString.h"

#include "TSplineFactory.h"

#include "MsgLogger.h"
#include "RootClassFactory.h"

MsgLogger*                 TSplineFactory::m_logger = 0;

TSplineBase* TSplineFactory::CreateTSpline( TString title, TGraph* theGraph, const TString& spline_type, Bool_t isHistogram, const std::vector<Double_t>& energySize )
{
  // create output logger
  if (m_logger == 0) CreateMsgLogger();

  TSplineBase* spline = 0;
  if( spline_type == "TSpline1" )
    {
      //sanity check
      if( theGraph->GetN() < 2 )
        *m_logger << kFATAL << "Not enough points to use TSpline1: " << theGraph->GetN() << Endl;

      spline = new TSpline1( title, theGraph, isHistogram, energySize );
    }
  else
    { 
      if( spline_type == "TSpline2" )
        {
          //sanity check
          if( theGraph->GetN() == 2 ){
            spline = new TSpline1( title, theGraph, isHistogram, energySize );
          }
          if( theGraph->GetN() < 2 ){
            *m_logger << kFATAL << "Not enough points: " << theGraph->GetN() <<  " to use TSpline2 or TSpline1 for: "
                      << title << Endl;
          }

          spline = new TSpline2( title, theGraph, isHistogram, energySize );
        }
      else
        *m_logger << kFATAL << "Unknown TSpline type" << Endl;
    }

  return spline;
}

void TSplineFactory::CreateMsgLogger()
{
   m_logger = new MsgLogger( "TSplineFactory" );
}


