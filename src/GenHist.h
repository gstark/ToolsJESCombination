// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef GenHist_h
#define GenHist_h

#include "GenObjectBase.h"

class TString;
class TH1;
class TDirectory;

class GenHist : public GenObjectBase<TH1> {

 public:

  GenHist( const TString& name, const TString& title );
  // special, for the ComplexMerger
  GenHist( const TString& name, const TString& title, TH1& hRef, TH1& hAve, TH1& hRMS );
  ~GenHist();

   // toy cycle
   void Init();
   void Update( const TH1& );
   void Finalise();
   void Write( TDirectory* dir = 0 );
   void PrintResults() const {}

   // reference histogram
   void       SetReference( const TH1& );
   const TH1* GetReference() const { return m_refHist; }

   // accessors
   const TH1* GetAveHisto() const { return m_aveHist; }
   const TH1* GetRMSHisto() const { return m_rmsHist; }

 protected:

 private:

   void       SetAveHisto( const TH1& );
   void       SetRMSHisto( const TH1& );

   TString m_hname;
   TString m_htitle;

   TH1*    m_refHist;
   TH1*    m_aveHist;
   TH1*    m_rmsHist;
};

#endif
