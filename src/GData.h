// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef GData_h
#define GData_h

#include <vector>
#include "GBase.h"
#include "Channel.h"

class AveragerBase;

// global accessor function
class GData;
GData& gData();

class GData : public GBase {
   
 public:
   
   // singleton class
   static GData& Instance() {
      if (m_instance == 0) m_instance = new GData();
      return *m_instance;
   }
      
 public:
      
   GData();
   virtual ~GData();
      
   void     AddChannel ( Channel* );
   Channel* FindChannel( const TString& );

   // access to channel vector
   std::vector<Channel*>& GetChannelVec() { return m_channels; }

   // create graphs for all channels and measurements
   void CreateGraphs( TFile& );

   // merge all measurements belonging to a channel
   void MergeMeasurements();

   // print the class content
   void Print( std::ostream& os );

 private:

   static GData*         m_instance;
   std::vector<Channel*> m_channels;

};

#endif
