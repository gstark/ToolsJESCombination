// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//_______________________________________________________________________
//                                                                      
// Linear interpolation of a TGraph
//_______________________________________________________________________
//
#ifndef TSpline1_h
#define TSpline1_h

#include "TSplineBase.h"

#include "Utils.h"

class TGraph;
class TString;

class TSpline1 : public TSplineBase {
   
public:
   
  TSpline1( TString title, TGraph* theGraph, Bool_t isHistogram, const std::vector<Double_t>& energySize );
  virtual ~TSpline1( void );
   
  // evaluators
  //returns the value of the spline at x ( NO vertical displacement of splines is considered in this function )
  virtual Double_t Eval( Double_t x ) const; 

  //this function should consider vertical displacements of splines, if necessary (for histograms)
  virtual Double_t EvalIntegral( Double_t xmin, Double_t xmax ) const;

  // accessors
  const TString GetType() const { return "TSpline1"; }
  const TSplineBase::ESplineType  GetESplineType() const { return TSplineBase::kTSpline1; } 

   // dummy implementations of virtual base class functions
   virtual void BuildCoeff( void ) {}                                 // no coefficients
   virtual void GetKnot( Int_t i, Double_t& x, Double_t& y ) const {} // no knots
   
private:

  // this function should NOT consider vertical displacements of splines ( for a GRAPH of points )
  Double_t EvalIntegralPoints( Double_t xmin, Double_t xmax ) const;

  // this function should consider vertical displacements of splines ( for a HISTOGRAM ) 
  Double_t EvalIntegralBins( Double_t xmin, Double_t xmax ) const;

  ClassDef(TSpline1,0);

};

#endif 


