// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "Formula.h"

Formula::Formula( const TString& expression )
   : GBase( "Formula" ),
     m_expression( expression )
{}

Formula::~Formula()
{}

