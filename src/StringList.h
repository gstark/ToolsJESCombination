// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef StringList_h
#define StringList_h

#include <vector>

#include "GBase.h"

class TString;

class StringList : public GBase {

 public:

   StringList( const TString& argstr );
   StringList( std::vector<TString>& args );
   ~StringList();
   
   std::vector<TString>& GetArgs()   const { return m_args; }
   const TString         GetArgStr() const;
   const TString&        GetArg( Int_t iarg ) const;   
   Bool_t                HasArg( const TString& str )  const;
   Int_t                 GetEntries() const { return (Int_t)m_args.size(); }
   const TString&        operator[]( Int_t iarg ) const { return GetArg( iarg ); }

 private:

   std::vector<TString>& m_args;
};

#endif
