// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//
// Base class of everything
//

#ifndef GBase_h
#define GBase_h

#include "TString.h"

#include "MsgLogger.h"

class GBase {

 public:
   
   GBase( const TString& name = "Unknown" );
   GBase( const GBase& );
   virtual ~GBase();
   
   const TString GetName() const { return m_name; }
   void  SetName( const TString& name ) { 
      m_name = name; 
      m_logger.SetSource( name.Data() );
   }
   
   // initialisation
   virtual void Init() { SetInitialised(); }

   // already initialised ?
   Bool_t IsInitialised () const { return m_initialised; }
   void SetInitialised  () { m_initialised = kTRUE; }
   void UnsetInitialised() { m_initialised = kFALSE; }

 protected:
   
   // name of the derived class
   TString            m_name;
   
   // is initialised ? 
   Bool_t             m_initialised;   

   mutable MsgLogger  m_logger;

   ClassDef(GBase,0);
};

#endif
