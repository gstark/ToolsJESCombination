// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TString.h"
#include "TClass.h"
#include "TROOT.h"

#include "RootClassFactory.h"
#include "MsgLogger.h"

MsgLogger* RootClassFactory::m_logger = 0;

void* RootClassFactory::CreateObject( const TString& className, const TString& inheritsFrom )
{
   if (m_logger == 0) m_logger = new MsgLogger( "RootClassFactory" );

   void* object = 0;
   TClass* tmp = gROOT->GetClass( className );
   if (tmp) {
      if (inheritsFrom.Sizeof() == 0 || tmp->InheritsFrom( inheritsFrom )) {	
         object = tmp->New();
         if (!object) {
            *m_logger << kFATAL << "Class \"" << className 
                      << "\" could not be instantiated via TClass::New()" << Endl;
         }
      }  
      else {
         *m_logger << kFATAL << "Class \"" << className << "\" does not inherit from class \""
                   << inheritsFrom << "\"" << Endl;
      }
   }
   else {
      *m_logger << kFATAL << "Class \"" << className << "\" could not be instantiated" << Endl;
   }   

   return object;
}

