// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef SimpleConditionFormula_h
#define SimpleConditionFormula_h

#include <vector>

#include "TFormula.h"
#include "Formula.h"

class SimpleConditionFormula : public Formula {

 public:

   SimpleConditionFormula( const TString& theSimpleConditionFormulaExpression );
   virtual ~SimpleConditionFormula();
      
   // evaluate formula
   Double_t Eval( std::vector<Double_t>& pars ) const;

 protected:

   void ParseExpression();

 private:

   std::vector<Double_t>  m_condVec;
   std::vector<TFormula*> m_exprVec;
};

#endif
