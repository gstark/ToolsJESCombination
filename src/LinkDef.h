#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#pragma link C++ class Action+;
#pragma link C++ class Channel+;
#pragma link C++ class ComplexMergedMeasurement+;
#pragma link C++ class Controller+;
#pragma link C++ class Data+;
#pragma link C++ class Formula+;
#pragma link C++ class GBase+;
#pragma link C++ class GConfig+;
#pragma link C++ class GData+;
#pragma link C++ class GenHist+;
#pragma link C++ class GStore+;
#pragma link C++ class Interval+;
#pragma link C++ class MeasurementBase+;
#pragma link C++ class MsgLogger+;
#pragma link C++ class RootClassFactory+;
#pragma link C++ class SimpleConditionFormula+;
#pragma link C++ class StringList+;
#pragma link C++ class Timer+;
#pragma link C++ class TSpline1+;
#pragma link C++ class TSpline2+;
#pragma link C++ class TSplineBase+;
#pragma link C++ class TSplineFactory+;
#pragma link C++ class UnitMeasurement+;
#pragma link C++ class Utils+;
#pragma link C++ class Variable+;
#pragma link C++ class XMLDataInterpreter+;
#pragma link C++ class XMLDrivingInterpreter+;
#pragma link C++ class XMLInterpreter+;

#endif
