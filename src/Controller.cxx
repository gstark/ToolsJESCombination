// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iostream>
#include <vector>

#include "TFile.h"
#include "TObjArray.h"
#include "TObjString.h"

#include "Controller.h"
#include "GStore.h"
#include "GData.h"
#include "Action.h"
#include "XMLInterpreter.h"
#include "XMLDrivingInterpreter.h"
#include "XMLDataInterpreter.h"
#include "Interval.h"

using std::cout;
using std::endl;

ClassImp(Controller)

Controller::Controller( const TString& drivingCard )
   : GBase( "Controller" ),
     m_targetFile( 0 )
{
   m_xmlInterpreter["Driving"] = new XMLDrivingInterpreter( drivingCard );
}

Controller::~Controller()
{}

void Controller::Initialize()
{
   // first create global data object 
   std::map<const TString, XMLInterpreter*>::iterator it;
   for (it = m_xmlInterpreter.begin(); it != m_xmlInterpreter.end(); it++) it->second->Interpret();

   // interpret input data card
   gStore().AssertVariable( "Files::InputData" );
   m_xmlInterpreter["Data"] = new XMLDataInterpreter( gStore().GetVariable( "Files::InputData" )->GetStringValue() );
   m_xmlInterpreter["Data"]->Interpret();

   // create target file (default is "ToolsJEScombination.root")
   const TString& target = ( gStore().ExistVariable( "Files::GraphicsTarget" ) ?
                             gStore().GetVariable( "Files::GraphicsTarget" )->GetStringValue() : "ToolsJEScombination.root" );
   m_logger << kINFO << "Create target file: \"" << target << "\"" << Endl;
   m_targetFile = TFile::Open( target, "RECREATE" );      

   // print data object
   if (gStore().IsTrue( "Flags::PrintInputData" )) gData().Print( std::cout );
}

void Controller::ExecuteAllActions() 
{
   // loop over all actions in driving card
   std::vector<const Action*>::const_iterator action = gStore().GetActions().begin();
   for (; action != gStore().GetActions().end(); action++) {
      m_logger << kINFO << "Execute action: \"" << (*action)->GetName() << "\"" << Endl;
      
      std::vector<TString>::iterator par = (*action)->GetArgs().begin();
      if ((*action)->GetName() == "MergeMeasurements") {

        // do the Merging(s)
         gData().MergeMeasurements();
      }

      if ((*action)->GetName() == "CreateGraphs") {
         gData().CreateGraphs( GetTargetFile() );
      }

   }
}

void Controller::Finalize() 
{
   m_logger << kINFO << "Close target file: \"" << GetTargetFile().GetName() << "\"" << Endl;
   GetTargetFile().Close();
}
