// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Timer_h
#define Timer_h

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Timer                                                                //
//                                                                      //
// Timing information for training and evaluation of MVA methods        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "time.h"
#include "TStopwatch.h"

class TString;
class MsgLogger;

// ensure that clock_t is always defined
#if defined(__SUNPRO_CC) && defined(_XOPEN_SOURCE) && (_XOPEN_SOURCE - 0 == 500 )
#ifndef _CLOCK_T
#define _CLOCK_T
typedef long clock_t; // relative time in a specified resolution
#endif /* ifndef _CLOCK_T */

#endif // SUN and XOPENSOURCE=500

class Timer : public TStopwatch {
   
 public:
   
   Timer( const char* prefix = "", Bool_t colourfulOutput = kTRUE );
   Timer( Int_t ncounts, const char* prefix = "", Bool_t colourfulOutput = kTRUE );
   virtual ~Timer( void );
  
   void Init ( Int_t ncounts );
   void Reset( void );

   // when the "Scientific" flag set, time is returned with subdecimals
   // for algorithm timing measurement
   TString GetElapsedTime  ( Bool_t Scientific = kTRUE  );
   TString GetLeftTime     ( Int_t icounts );
   void    DrawProgressBar ( Int_t );
   void    DrawProgressBar ( TString );
   void    DrawProgressBar ( void );
                          
 private:

   Double_t  ElapsedSeconds( void );
   TString   SecToText     ( Double_t, Bool_t ) const;

   Int_t     fNcounts;               // reference number of "counts" 
   TString   fPrefix;                // prefix for outputs
   Bool_t    fColourfulOutput;       // flag for use of colors

   static const TString fgClassName; // used for output
   static const Int_t   fgNbins;     // number of bins in progress bar

   MsgLogger*           m_logger;     // the output logger

   ClassDef(Timer,0);

};

#endif
