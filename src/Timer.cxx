// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TString.h"
#include "Riostream.h"

#include "Timer.h"
#include "Utils.h"
#include "MsgLogger.h"

const TString Timer::fgClassName = "Timer";
const Int_t   Timer::fgNbins     = 24;  

using Utils::Color;

//_______________________________________________________________________
Timer::Timer( const char* prefix, Bool_t colourfulOutput )
   : fNcounts        ( 0 ),
     fPrefix         ( Timer::fgClassName ),
     fColourfulOutput( colourfulOutput )
{
   // constructor
   TString *prefixS = new TString(prefix);
   if ( prefixS->EqualTo("") ) { fPrefix = Timer::fgClassName; }
   else                        { fPrefix = prefix; }
   delete prefixS;

   m_logger = new MsgLogger( fPrefix.Data() );

   Reset();
}

//_______________________________________________________________________
Timer::Timer( Int_t ncounts, const char* prefix, Bool_t colourfulOutput  )
   : fNcounts        ( ncounts ),
     fColourfulOutput( colourfulOutput )
{
   // standard constructor: ncounts gives the total number of counts that 
   // the loop will iterate through. At each call of the timer, the current
   // number of counts is provided by the user, so that the timer can obtain
   // the due time from linearly interpolating the spent time.
   TString *prefixS = new TString(prefix);
   if ( prefixS->EqualTo("") ) { fPrefix = Timer::fgClassName; }
   else                        { fPrefix = prefix; }
   delete prefixS;

   m_logger = new MsgLogger( fPrefix.Data() );

   Reset();
}

//_______________________________________________________________________
Timer::~Timer( void )
{
   // destructor
   delete m_logger;
}

void Timer::Init( Int_t ncounts )
{
   // timer initialisation
   fNcounts = ncounts;  
   Reset();
}

//_______________________________________________________________________
void Timer::Reset( void )
{
   // resets timer
   TStopwatch::Start( kTRUE );
}

//_______________________________________________________________________
Double_t Timer::ElapsedSeconds( void ) 
{
   // computes elapsed tim in seconds
   Double_t rt = TStopwatch::RealTime(); TStopwatch::Start( kFALSE );
   return rt;
}
//_______________________________________________________________________

TString Timer::GetElapsedTime( Bool_t Scientific ) 
{
   // returns pretty string with elaplsed time
   return SecToText( ElapsedSeconds(), Scientific );
}

//_______________________________________________________________________
TString Timer::GetLeftTime( Int_t icounts ) 
{
   // returns pretty string with time left
   Double_t leftTime = ( icounts <= 0 ? -1 :
                         icounts > fNcounts ? -1 :
                         Double_t(fNcounts - icounts)/Double_t(icounts)*ElapsedSeconds() );

   return SecToText( leftTime, kFALSE );
}

//_______________________________________________________________________
void Timer::DrawProgressBar() 
{
   // draws the progressbar
   fNcounts++;
   if (fNcounts == 1) {
      std::clog << m_logger->GetPrintedSource();
      std::clog << "Please wait ";
   }

   std::clog << "." << std::flush;
}

//_______________________________________________________________________
void Timer::DrawProgressBar( TString theString ) 
{
   // draws a string in the progress bar
   std::clog << m_logger->GetPrintedSource();

   std::clog << Color("white_on_green") << Color("dyellow") << "[" << Color("reset");

   std::clog << Color("white_on_green") << Color("dyellow") << theString << Color("reset");

   std::clog << Color("white_on_green") << Color("dyellow") << "]" << Color("reset");

   std::clog << "\r" << std::flush; 
}

//_______________________________________________________________________
void Timer::DrawProgressBar( Int_t icounts ) 
{
   // draws progress bar in color or B&W
   // caution: 

   // sanity check:
   if (icounts > fNcounts-1) icounts = fNcounts-1;
   if (icounts < 0         ) icounts = 0;
   Int_t ic = Int_t(Float_t(icounts)/Float_t(fNcounts)*fgNbins);

   std::clog << m_logger->GetPrintedSource();
   if (fColourfulOutput) std::clog << Color("white_on_green") << Color("dyellow") << "[" << Color("reset");
   else                  std::clog << "[";
   for (Int_t i=0; i<ic; i++) {
      if (fColourfulOutput) std::clog << Color("white_on_green") << Color("dyellow") << ">" << Color("reset"); 
      else                  std::clog << ">";
   }
   for (Int_t i=ic+1; i<fgNbins; i++) {
      if (fColourfulOutput) std::clog << Color("white_on_green") << Color("dyellow") << "." << Color("reset"); 
      else                  std::clog << ".";
   }
   if (fColourfulOutput) std::clog << Color("white_on_green") << Color("dyellow") << "]" << Color("reset");
   else                  std::clog << "]" ;

   // timing information
   if (fColourfulOutput) {
      std::clog << Color("reset") << " " ;
      std::clog << "(" << Color("red") << Int_t((100*(icounts+1))/Float_t(fNcounts)) << "%" << Color("reset")
               << ", " 
               << "time left: "
               << this->GetLeftTime( icounts ) << Color("reset") << ") ";
   }
   else {
      std::clog << "] " ;
      std::clog << "(" << Int_t((100*(icounts+1))/Float_t(fNcounts)) << "%" 
               << ", " << "time left: " << this->GetLeftTime( icounts ) << ") ";
   }
   std::clog << "\r" << std::flush; 
}

//_______________________________________________________________________
TString Timer::SecToText( Double_t seconds, Bool_t Scientific ) const
{
   // pretty string output
   TString out = "";
   if      (Scientific    ) out = Form( "%.3g sec", seconds );
   else if (seconds <  0  ) out = "unknown";
   else if (seconds <= 300) out = Form( "%i sec", Int_t(seconds) );
   else {
      if (seconds > 3600) {
         Int_t h = Int_t(seconds/3600);
         if (h <= 1) out = Form( "%i hr : ", h );
         else        out = Form( "%i hrs : ", h );
      
         seconds = Int_t(seconds)%3600;
      }
      Int_t m = Int_t(seconds/60);
      if (m <= 1) out += Form( "%i min", m );
      else        out += Form( "%i mins", m );
   }

   return (fColourfulOutput) ? Color("red") + out + Color("reset") : out;
}
