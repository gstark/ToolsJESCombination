// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef ComplexMergedMeasurement_h
#define ComplexMergedMeasurement_h

#include <vector>

#include "TMatrixD.h"
#include "TVectorD.h"
#include "TString.h"

#include "MeasurementBase.h"
#include "Data.h"
#include "TSpline1.h"
#include "TSpline2.h"

#include "Channel.h"
#include "GData.h"
#include "Interval.h"
#include "TMatrixDSym.h"
#include "Timer.h"
#include "GenHist.h"
#include "TRandom3.h"

class ComplexMergedMeasurement : public MeasurementBase {

 public:
      
   ComplexMergedMeasurement( const Int_t splineType );
   ComplexMergedMeasurement( const TString& channelName, const Int_t splineType );
   virtual ~ComplexMergedMeasurement();
      
   // initialisation
   void Init();

  // does the merging of all the measurements in the channel
   void MergeChannel( TRandom3* RGen, Channel* channel );

   // add data point
   void AddData( Data* );

   // overload exp ID setter (no automatic propagation to data points)
   void SetExpId( Int_t id ) { m_expId = id; }

  //ACCESSORS
  // accessors to the binning vectors
  std::vector<Interval*>& GetBinningSmall() { return m_smallBinsEnergies;}
  std::vector<Interval*>& GetBinningLarge() { return m_largeBinsEnergies;}

  //accessor to the spline type
  const Int_t GetSplineType() const { return m_splineType;}

   // type identifier
   const TString GetType() const { return "ComplexMergedMeasurement"; }

  // print some statistics
  void PrintMergeStatistics() const;

 protected:

  // set the final binning and save it to file
  void SetBinnings( Channel* _channel );
  // test the consistency of the large binning with respect to the points/bins of the experiments
  ///no large bin should be contained by an interval of two consecutive points/bin centers of the measurements in the channel
  void TestLargeBinningConsistency( Channel* channel );
  
  // generates a matrix of toy correlated values. The Xsec values are returned only for the Emin-Emax range.
   void GenerateToys( std::vector<TMatrixD*>& toyMValues, std::vector< std::vector<Double_t> >& toyXsecValues_EminEmax_, Double_t Emin_, Double_t Emax_, UInt_t nToys, Channel* channel, const std::vector<TString*>& syst_names, TRandom3* RG, Timer& time, UInt_t activStat, UInt_t useIntermLargeBins, UInt_t activSyst, Int_t activMeasurementSyst ); 

   // compute covariance matrix of the Xsec values of all the measurements in the channel
   void ComputeXsecCovMat( UInt_t nToys, const std::vector< std::vector<Double_t> > toyXsecValues );

   // compute the covariance matrix between the measurements contributing at each bin and then compute the average coefficients
   void ComputeCoeff( UInt_t nToys, const std::vector<TMatrixD*>& toyValues, Timer& timer, const std::vector< std::vector<Bool_t> >& _mCorrMeasurementsChannel );

   // performs the fit of the JES response ratio
   void fitJES( TH1D* &fitH_JES_, TVectorD* &parFit_ );

  // computes the average of all the measurements in the channel
   void ComputeComplexAverage( UInt_t nToys, Channel* channel, const std::vector<TString*>& syst_names, TRandom3* RG, const std::vector< std::vector<Bool_t> >& _mCorrMeasurementsChannel );
   void ComputeComplexAverage( UInt_t nToys, const std::vector<TMatrixD*>& toyValues, Timer& timer, TVectorD *errorCorrection_bin, UInt_t activeStat, UInt_t activeSyst, const std::vector< std::vector<Bool_t> >& _mCorrMeasurementsChannel, const std::vector<TString*>& syst_names, Int_t activMeasurementSyst ); 

 private:

  // creates the high bin density histogram for the ComplexMergedMeasurement
  void  CreateHBDHistogram( Channel* channel );

  // vector giving the energy intervals of the bins
  std::vector<Interval*> m_smallBinsEnergies;
  std::vector<Interval*> m_largeBinsEnergies;

  // vector counting the measurements in the channel contributing to the small/large bin
  std::vector<UInt_t> m_nContributorsSmall;
  std::vector<UInt_t> m_nContributorsLarge;

  // matrix with the measurements contributing to each small/large bin
  std::vector< std::vector<Bool_t> > m_contributionSmall;
  std::vector< std::vector<Bool_t> > m_contributionLarge;

  // matrix with the contribution coefficient of each measurements to each small bin
  std::vector< std::vector<Double_t> > m_contributionSmallCoeff;

  // inverse of the covariance matrix for each small bin, computed with splines on large/small bins(this is the quantity that one can directly use in formulas)
  std::vector<TMatrixDSym*> m_InvCov_matrix_large;
  std::vector<TMatrixDSym*> m_InvCov_matrix_small;

   TMatrixDSym *m_covMatrixXsecValues;

  // type of the splinning we have to use
  Int_t m_splineType;

  // we don't need any other vector for the names of systematic errors (all the errors are given individually at this leves)

  // if this variable is "kTRUE" the details of the merging will be printed
  Bool_t m_print_details;

  // if this variable is "kTRUE" a fit will be performed, in addition to the spline-based combination
  Bool_t m_performFit;

  TString* m_namesParFit;

   // statistics
  //enum EMergeType { kAll = 0, kAllMerged, kInserted, kAveraged, kNumMTypes };
  //static const char* EMergeTypeStr[kNumMTypes];       
  UInt_t m_NfilledBinsSmall, m_NfilledBinsLarge;
  UInt_t m_NNeg_large;
  UInt_t m_NmostNeg_large;

};

#endif
