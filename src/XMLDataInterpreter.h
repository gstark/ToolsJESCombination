// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef XMLDataInterpreter_h
#define XMLDataInterpreter_h

#include "XMLInterpreter.h"
#include "TVectorD.h"
#include "TMatrixD.h"

class XMLDataInterpreter : public XMLInterpreter {
   
 public:
   
   XMLDataInterpreter( const TString& xmlFileName );
   ~XMLDataInterpreter();
   
   Bool_t Interpret() const; 
   
 private:

   void ReadMeasurements( TXMLNode* ) const;
   
};

#endif
