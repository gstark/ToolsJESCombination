// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "Data.h"
#include "TRandom3.h"
#include "GStore.h"

Data::Data(  Double_t Emin, Double_t Emax, 
             Double_t Xsec, Double_t eXsecStat, std::vector<Double_t> eXsecSystVec )
   : GBase( "Data" ),
     m_EInterval      (new Interval(Emin,Emax)),
     m_Xsec           ( Xsec ),     
     m_eXsecStat      ( eXsecStat ),    
     m_expId          ( 0 )
{
  std::vector<Double_t>::iterator it;
  for ( it =  eXsecSystVec.begin(); it !=  eXsecSystVec.end(); it++){
    m_eXsecSystVec.push_back( *it );
  }

   m_logger.SetMinType( kINFO );
}


// copy constructor
Data::Data( const Data& other )
   : GBase( "Data" ),
     m_EInterval      ( new Interval ( other.m_EInterval ) ),    
     m_Xsec           ( other.m_Xsec ),     
     m_eXsecStat      ( other.m_eXsecStat ),    
     m_expId          ( other.m_expId )
{
  std::vector<Double_t> copySyst = other.GeteXsecSyst();

  std::vector<Double_t>::iterator it;
  for ( it =  copySyst.begin(); it !=   copySyst.end(); it++){
    m_eXsecSystVec.push_back( *it );
  }
}


Data::~Data()
{
  delete m_EInterval;
  if( m_eXsecSystVec.size() ) m_eXsecSystVec.clear(); 
}

// yet another version
Double_t Data::GetEDistance( const Data& other ) const
{
   // overlapping ? --> return 0
  if( ( EInterval().GetEStatus( other.EInterval() ) != Interval::kSmaller) && ( EInterval().GetEStatus( other.EInterval() ) != Interval::kLarger ) ) return 0;
 
   // take minimum to limits
   else {
     const Double_t a[4] = {  TMath::Abs(Eave() - other.Emin()),
                              TMath::Abs(Eave() - other.Emax()),
                              TMath::Abs(other.Eave() - Emin()),
                              TMath::Abs(other.Eave() - Emax()) };
     return TMath::MinElement( 4, a );
   }

  return 0;
}


Double_t Data::GenerateValue( TRandom3& R, const TArrayD& systCoeff, UInt_t activStat, UInt_t activSyst )
{
  // sanity check: test the lenght of systCoeff
  if( (UInt_t)systCoeff.GetSize() != m_eXsecSystVec.size() )
    {
      m_logger << kFATAL << " The size of systCoeff is " << systCoeff.GetSize() 
               << " but the size of m_eXsecSystVec is" << m_eXsecSystVec.size() << std::endl;
    }

  // init the value to the mean value of the XSec
  Double_t value = m_Xsec;

  // add a random statistical error
  if( activStat ){
    value += R.Gaus( 0., m_eXsecStat );
  }

  // add the systematic errors
  if( activSyst ){
    for( Int_t i=0; i < systCoeff.GetSize(); i++ ){
      value += systCoeff[i]*m_eXsecSystVec[i];
    }
  }

  return value;
}

void Data::Print( std::ostream& os )
{   
   os << Form( "Data: [%8.4f : %8.4f] = (Xsec: %7.2f +- %6.2f +- %6.2f)  [ID: %i]",
               Emin(), Emax(), 
               Xsec(), eXsecStat(), eXsecSystTot(),
               GetExpId() ) << std::endl;
   os << "                                  EXsecSyst details: " ;
   for(UInt_t i=0; i<GeteXsecSyst().size(); i++){
     os << " +- " << GeteXsecSyst()[i] ;
   }
   os << std::endl;
}

void Data::Print()
{   
   m_logger << Form( "Data: [%8.4f : %8.4f] = (Xsec: %7.2f +- %6.2f +- %6.2f)  [ID: %i]",
               Emin(), Emax(), 
               Xsec(), eXsecStat(), eXsecSystTot(),
               GetExpId() ) << Endl;
   m_logger << "                                  EXsecSyst details: " ;
   for(UInt_t i=0; i<GeteXsecSyst().size(); i++){
     m_logger << " +- " << GeteXsecSyst()[i] ;
   }
   m_logger << Endl;
}

Double_t Data::eXsecSystTot () const {
	Double_t totalSystSqr = 0.0;

	std::vector<Double_t>::iterator it;
	std::vector<Double_t> vec =  GeteXsecSyst();
	for ( it = vec.begin(); it != vec.end(); it++){
      totalSystSqr += ( *it )*( *it );
	}

	return 	TMath::Sqrt(totalSystSqr);
}

Double_t Data::eXsecTot () const { 
	Double_t totalSyst = eXsecSystTot();
	return 	TMath::Sqrt(eXsecStat()*eXsecStat() + totalSyst*totalSyst);
}

