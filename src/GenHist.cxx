// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TMath.h"
#include "TDirectory.h"

#include "GenHist.h"

GenHist::GenHist( const TString& hname, const TString& htitle )
   : GenObjectBase<TH1>(), 
     m_hname( hname ),
     m_htitle( htitle ),
     m_refHist( 0 ),
     m_aveHist( 0 ),
     m_rmsHist( 0 )
{
   SetName( "GenHist" );
}

// special, for the ComplexMerger
GenHist::GenHist( const TString& hname, const TString& htitle, TH1& hRef, TH1& hAve, TH1& hRMS )
   : GenObjectBase<TH1>(), 
     m_hname( hname ),
     m_htitle( htitle ),
     m_refHist( 0 ),
     m_aveHist( 0 ),
     m_rmsHist( 0 )
{
   SetName( "GenHist" );
   GenHist::Init();

   SetReference( hRef );
   SetAveHisto( hAve );
   SetRMSHisto( hRMS );

   // increment toy counter ( dummy here, but has to be done at least once before using Finalise() )
   IncrementCounter();
   // must be called first for sanity checks
   GenObjectBase<TH1>::Finalise();
   
   for (Int_t ibin=1; ibin<=m_aveHist->GetNbinsX(); ibin++) {
      m_aveHist->SetBinError  ( ibin,  m_rmsHist->GetBinContent(ibin) );
   }
   
   SetFinalised();
}

GenHist::~GenHist()
{}

void GenHist::Init()
{
   // base class inits (reset flags and counter)
   GenObjectBase<TH1>::Init();

   if (m_refHist) { delete m_refHist; m_refHist = 0; }
   if (m_aveHist) { delete m_aveHist; m_aveHist = 0; }
   if (m_rmsHist) { delete m_rmsHist; m_rmsHist = 0; }
}

void GenHist::SetReference( const TH1& h )
{
   if (m_refHist) { delete m_refHist; m_refHist = 0; }
   m_refHist = (TH1*)h.Clone();
   m_refHist->SetNameTitle( m_hname + "_Reference", m_htitle + " (Reference)" );
}

void GenHist::SetAveHisto( const TH1& h )
{
  if (m_aveHist) { delete m_aveHist; m_aveHist = 0; }
  m_aveHist = (TH1*)h.Clone();
  m_aveHist->SetNameTitle( m_hname + "_Average", m_htitle + " (Average)" );
}

void GenHist::SetRMSHisto( const TH1& h )
{
   if (m_rmsHist) { delete m_rmsHist; m_rmsHist = 0; }
   m_rmsHist = (TH1*)h.Clone();
   m_rmsHist->SetNameTitle( m_hname + "_RMS", m_htitle + " (RMS)" );
}

void GenHist::Update( const TH1& h )
{
   if (!m_aveHist) {
      // create histograms as clones of test histogram
      m_aveHist = new TH1D( m_hname  + "_Average", m_htitle + " (Average)",
                            h.GetNbinsX(), h.GetXaxis()->GetXmin(), h.GetXaxis()->GetXmax() );
      m_rmsHist = new TH1D( m_hname  + "_RMS", m_htitle + " (RMS)",
                            h.GetNbinsX(), h.GetXaxis()->GetXmin(), h.GetXaxis()->GetXmax() );
   }

   m_aveHist->Add( &h, 1.0 );
   for (Int_t ibin=1; ibin<=h.GetNbinsX(); ibin++) {
      m_rmsHist->AddBinContent( ibin, h.GetBinContent(ibin)*h.GetBinContent(ibin) );
   }

   // increment toy counter
   IncrementCounter();
}

void GenHist::Finalise()
{
   // must be called first for sanity checks
   GenObjectBase<TH1>::Finalise();

   // compute average and RMS
   m_aveHist->Scale( 1.0/GetCounter() );
   
   for (Int_t ibin=1; ibin<=m_aveHist->GetNbinsX(); ibin++) {
      Double_t rad = m_rmsHist->GetBinContent(ibin)/GetCounter() - 
         m_aveHist->GetBinContent(ibin)*m_aveHist->GetBinContent(ibin);
      rad = TMath::Max(rad,0.0);
      m_rmsHist->SetBinContent( ibin, TMath::Sqrt( rad ) );
      m_aveHist->SetBinError  ( ibin, TMath::Sqrt( rad ) );
   }

   SetFinalised();
}

void GenHist::Write( TDirectory* dir )
{
   if (IsFinalised()) {
      if (dir != 0) dir->cd(); // else: write into current directory
      m_refHist->Write();
      m_aveHist->Write();
      m_rmsHist->Write();
   }
   else {
      m_logger << kFATAL << "Cannot write average and rms histograms w/o calling Finalise" << Endl;
   }
}
