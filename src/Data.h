// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Data_h
#define Data_h

#include <vector>

#include "TMath.h"
#include "TArrayD.h"
#include "TRandom3.h"

#include "GBase.h"
#include "Interval.h"

class Interval;//

class Data : public GBase {

 public:

   Data( Double_t Emin, Double_t Emax, 
         Double_t Xsec, Double_t eXsecStat, std::vector<Double_t> eXsecSystVec );
   
   Data( const Data& other );

   virtual ~Data();

   // accessors
  const Interval& EInterval () const { return *m_EInterval; }
  const Double_t Emin       () const { return m_EInterval->GetMin(); }
  const Double_t Emax       () const { return m_EInterval->GetMax(); }
  const Double_t Eave       () const { return m_EInterval->GetMean(); }
  const Double_t deltaE     () const { return m_EInterval->GetWidth(); }

   Double_t Xsec     () const { return m_Xsec; }
   Double_t eXsecStat() const { return m_eXsecStat; }

   // accessor to Xsection syst errors vector  ???
   std::vector<Double_t> GeteXsecSyst() const { return m_eXsecSystVec; }

   Double_t eXsecSystTot () const ;
   Double_t eXsecTot  () const ;

   // external setting of the errors ( + Xsec)
  void SetXsec( Double_t Xsec ) { m_Xsec = Xsec; }
  void SeteXsecStatError( Double_t statXsecError ) { m_eXsecStat = statXsecError; }//For Complex Merger Plot (set the diagonal error) & rescaling for R computation
  void AppendeXsecSystVec( Double_t syst ) { m_eXsecSystVec.push_back(syst); } 

  // generates 3 random values of the XSec (including statistics, systematics or both), using the vector of coefficients for the systematics
  Double_t GenerateValue( TRandom3& R, const TArrayD& systCoeff, UInt_t activStat_, UInt_t activSyst_ );
   
   // experiment Identifier
   Int_t    GetExpId ()  const { return m_expId; }
   void     SetExpId( Int_t id ) { m_expId = id; }

   // energy distance between data points
   Double_t GetEDistance( const Data& other ) const;

   // print the class content
   void Print( std::ostream& os );
   void Print();

 private:

  // the energy interval of the data
  Interval* m_EInterval;

   Double_t m_Xsec;
   Double_t m_eXsecStat;

   //Xsection systematic errors Values
   std::vector<Double_t> m_eXsecSystVec;   

   Int_t    m_expId;

};

#endif
