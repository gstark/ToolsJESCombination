// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TDirectory.h"
#include "TFile.h"

#include "Channel.h"
#include "GStore.h"
#include "ComplexMergedMeasurement.h"
#include "StringList.h"
#include "TRandom3.h"

class TRandom;

Channel::Channel( const TString& name )
   : GBase( "Channel" ),
     m_name( name ),
     m_regexpName( name ),
     m_active( kTRUE ),
     m_localDir( 0 )
{
   m_measurements.clear();
   m_Mmeasurements.clear(); 

   // create unix-compatible name (for directories and histograms, etc)
   m_regexpName.ReplaceAll( "-->", "_to_" ); 
   m_regexpName.ReplaceAll( "+", "P" ); 
   m_regexpName.ReplaceAll( "-", "M" ); 
   m_regexpName.ReplaceAll( "(", "_X_" ); 
   m_regexpName.ReplaceAll( ")", "_X_" ); 
   m_regexpName.ReplaceAll( " ", "" ); 

   // check if active: channels are active if not mentioned otherwise in driving data card
   const Variable* ch = gStore().GetVariableIfExist( "UseChannels::Name" );
   if (ch) {
      // check if channel name is part of "UseChannels" list
      const StringList& chlist = ch->GetStringList();

      // clean string names
      TString chname = GetName(); Utils::CleanString( chname );
      TString arg0 = chlist[0]; arg0.ToLower();

      if (chlist.HasArg( chname ) || arg0 == "all") m_active = kTRUE;
      else m_active = kFALSE;
   }  
   else m_active = kTRUE;

   if (IsActive()) m_logger << kINFO << "Instantiated active channel: \"" << GetName() << "\"" << Endl;
   else            m_logger << kINFO << "Ignoring inactive channel  : \"" << GetName() << "\"" << Endl;
}

Channel::~Channel()
{
  std::vector<MeasurementBase*>::iterator it;
  for(it=m_measurements.begin(); it!=m_measurements.end(); it++){
    delete *it;
  }
  m_measurements.clear();
  for(it=m_Mmeasurements.begin(); it!=m_Mmeasurements.end(); it++){
    delete *it;
  }
  m_Mmeasurements.clear();
}

void Channel::AddMeasurement( MeasurementBase* measurement )
{
   if (measurement == 0) m_logger << kFATAL << "<AddMeasurement> Zero pointer" << Endl;

   // update measurement (experiment) counter
   measurement->SetExpId( (Int_t)m_measurements.size() ); 

   // insert
   m_measurements.push_back( measurement );
}

void Channel::GetAllSystErrNamesChannel( std::vector<TString*>& allSystErrNamesChannel ) 
{
  // iterate over all measurements and add the names of their systerrors if not added yet
  std::vector<MeasurementBase*>::iterator it = GetMeasurementVec().begin();
  for (; it != GetMeasurementVec().end(); it++)
    {
      //iterate over the systematic error names of a measurement
      std::vector<TString*>::iterator it1 = (*it)->GetSystErrNamesAll().begin();
      for( ; it1!= (*it)->GetSystErrNamesAll().end(); it1++ )
        {
          //add the name of an error of the measurement to the vector of names of all systematic errors if not added yet
          if( allSystErrNamesChannel.size() > 0 )
            {
              // there are already some elements in allSystErrNamesChannel

              Bool_t found = kFALSE;
              std::vector<TString*>::iterator it2 = allSystErrNamesChannel.begin();
              for( ; it2 != allSystErrNamesChannel.end() ; it2++ )
                {
                  if( (**it1)==(**it2) )
                    {
                      // sanity check
                      if( found )
                        m_logger << kFATAL << "problems in the allSystErrNamesChannel: double added error name" << Endl;
                    
                      found = kTRUE;
                    }
                }

              if( !found )
                allSystErrNamesChannel.push_back( *it1 );

            }
          else
            {
              // *it1 should be the first element in allSystErrNamesChannel
              allSystErrNamesChannel.push_back( *it1 );         
            }
        }

    }

  // print allSystErrNamesChannel
  m_logger << kINFO << "A number of " << allSystErrNamesChannel.size() << " different names of systematic errors were found for the channel: " << GetName() << Endl;
  m_logger << kINFO << "Those errors are:  ";
  for( UInt_t i=0; i<allSystErrNamesChannel.size(); i++ ){
    m_logger << *allSystErrNamesChannel[i] << "  ";
  }
  m_logger << Endl;

}

void Channel::GetCorrMeasurementsChannel( std::vector< std::vector<Bool_t> >& corrMeasurementsChannel ){
  for (UInt_t iM1 = 0; iM1 < GetMeasurementVec().size(); iM1++){
    std::vector<Bool_t> tempV;
    corrMeasurementsChannel.push_back( tempV );
    for (UInt_t iM2 = 0; iM2 < GetMeasurementVec().size(); iM2++){
      (corrMeasurementsChannel[iM1]).push_back( kFALSE );
    }
  }

  // iterate over all measurements 
  for (UInt_t iM1 = 0; iM1 < GetMeasurementVec().size(); iM1++){
    for (UInt_t iM2 = 0; iM2 < GetMeasurementVec().size(); iM2++){
      //iterate over the systematic error names of the measurements
      std::vector<TString*>::iterator it1 = (GetMeasurementVec()[iM1])->GetSystErrNamesAll().begin();
      for( ; it1!= (GetMeasurementVec()[iM1])->GetSystErrNamesAll().end(); it1++ ){
        std::vector<TString*>::iterator it2 = (GetMeasurementVec()[iM2])->GetSystErrNamesAll().begin();
        for( ; it2!= (GetMeasurementVec()[iM2])->GetSystErrNamesAll().end(); it2++ ){
          if( **it1 == **it2 ){
            corrMeasurementsChannel[iM1][iM2] = kTRUE;
            break;
          }
        }
        if( corrMeasurementsChannel[iM1][iM2] ) break;
      }
    }
  }

  m_logger << " The experiments in this channel are: " ;
  for (UInt_t iM1 = 0; iM1 < GetMeasurementVec().size(); iM1++){
    m_logger << (GetMeasurementVec()[iM1])->GetExperiment() << " " ;
  }
  m_logger << Endl << " The matrix indicating possible correlations is: " << Endl;
  for (UInt_t iM1 = 0; iM1 < GetMeasurementVec().size(); iM1++){
    for (UInt_t iM2 = 0; iM2 < GetMeasurementVec().size(); iM2++){
      if( corrMeasurementsChannel[iM1][iM2] )
        m_logger << " 1 " ;
      else
        m_logger << " 0 " ;
    }
    m_logger << Endl;
  }
}

TDirectory& Channel::GetLocalDir( TFile* file ) 
{ 
   // create if not yet existing
   if (!m_localDir) {
      if (!file) m_logger << kFATAL << "<GetLocalDir> Cannot create directory without target file" << Endl;
      m_localDir = file->mkdir( GetRegexpName(), GetName() );
   }

   return *m_localDir; 
}

void Channel::CreateGraphs( TFile& file ) 
{
   GetLocalDir( &file ).cd();

   // unit measurements (write to file)
   std::vector<MeasurementBase*>::iterator it = GetMeasurementVec().begin();
   for (; it != GetMeasurementVec().end(); it++) (*it)->CreateGraph( kTRUE );  

   m_logger << "Found a number of " << GetMergedMeasurementVec().size() << " merged measurements" << Endl;

   // merged measurements (write to file)
   it = GetMergedMeasurementVec().begin();
   for (; it != GetMergedMeasurementVec().end(); it++) (*it)->CreateGraph( kTRUE );  
}

MeasurementBase* Channel::MergeMeasurements( Bool_t addToList, const TString& mergerName )
{
   // merge all measurements belonging to this channel into a single one
  
   // currently only one merger is implemented
   if ( mergerName != "ComplexMerger" ) {
     m_logger << kFATAL << "<MergeMeasurements> Unknown merger: \"" << mergerName << Endl;
   }

   if( mergerName == "ComplexMerger" ) 
     {
       gStore().AssertVariable( "ComplexMerger::SplineType" );
       UInt_t askedSpType = gStore().GetVariable( "ComplexMerger::SplineType" )->GetIntValue();

       // create a ComplexMergedMeasurement that will use the askedSpType
       ComplexMergedMeasurement* complexMM = 0; 

       // random numbers generator
       TRandom3* RGen_ = new TRandom3( /*(new TDatime())->GetTime()*/ 1 );

       complexMM = new ComplexMergedMeasurement( "AllMerged_"+GetName(), askedSpType );
       // merge the measurements in the channel
       complexMM->MergeChannel( RGen_, this );

       if(addToList)
         {
           m_Mmeasurements.push_back( complexMM );
         }

       return complexMM;
     }

   // one should never arrive here
   return 0;
}

void Channel::Print( std::ostream& os ) 
{   
   TString str = Form("Channel: Name \"%s\"", GetName().Data() );
   os << str << std::endl;
   for (Int_t i=0; i<str.Sizeof()-1; i++) os << "=";
   os << std::endl << std::endl;

   // iterate over all measurements and print them
   std::vector<MeasurementBase*>::iterator it = GetMeasurementVec().begin();
   for (; it != GetMeasurementVec().end(); it++) { os << "   "; (*it)->Print( os ); }
}


