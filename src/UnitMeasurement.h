// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef UnitMeasurement_h
#define UnitMeasurement_h

#include <vector>

#include "TObjString.h"

#include "TMatrixD.h"

#include "Interval.h"
#include "MeasurementBase.h"
#include "TSpline1.h"
#include "TSpline2.h"
#include "TVectorD.h"
#include "GStore.h"

class TSplineBase;
class TString;
class TGraph;
class TFormula;
class TArrayD;

//class Data;
class Interval;

class Formula;

class UnitMeasurement : public MeasurementBase {

 public:
      
   UnitMeasurement( const TString& channelName, 
                    const TString& alias,
                    const TString& experiment,
                    const TString& reference = "", 
                    const TString& comment = "", 
                    const TString& systError = "", 
                    Bool_t active = kTRUE );
   virtual ~UnitMeasurement();

   // initialisation
   void Init();

   // type identifier
   const TString GetType() const { return "UnitMeasurement"; }
      
   // add data point
   virtual void AddData( Data* );

  // accessors
  const TString& GetReference()   const { return m_reference; }
  const TString& GetComment()     const { return m_comment; }
  Bool_t         IsActive()       const { return m_active; }

  // returns kTRUE if the measurement has a "contribution" on the interval
  Bool_t HasContribution( const Interval* I);

  // generate random contribution of the measurement on all the bins where it contributes
  void GenerateMeasurementValues(  UInt_t askedSpline, TRandom3& R, const TArrayD& coeff_m, const std::vector<Bool_t>& contributionSmall, const std::vector<Interval*>& binningSmall, const std::vector<Bool_t>& contributionLarge, const std::vector<Interval*>& binningLarge, TMatrixD* matrix, UInt_t line, TVectorD* &toyXSec, UInt_t _activStat, UInt_t _useIntermLargeBins, UInt_t _activSyst );

   // print the class content
   virtual void Print( std::ostream& );

  // print the evaluation methods that can be used for the measurement
  void PrintMeasurementStatus();

 private:

   // systematic error interpretation
   TString GetSystError() const { return m_systError; }

   // meta data
   TString    m_reference;
   TString    m_comment;
   TString    m_systError;
   Bool_t     m_active;     

   //VECTORS FOR SYSTEMATIC ERRORS
   
   // names of systematic errors given by formulas
   std::vector<TString*> m_systErrNamesForm;  
   // systematic errors formulas
   std::vector<Formula*> m_systErrorFVec;

   // names of systematic errors given individually
   std::vector<TString*> m_systErrNamesInd;  

};

#endif
