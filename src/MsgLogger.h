// Authors: Attila Krasznahorkay

#ifndef MsgLogger_h
#define MsgLogger_h

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// MsgLogger                                                            //
//                                                                      //
// ostringstream derivative to redirect and format output               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

// STL include(s):
#include <string>
#include <sstream>
#include <map>

// ROOT include(s)
#ifndef ROOT_TObject
#include "TObject.h"
#endif

// Local include(s):

// define outside of class to facilite access
enum MsgLevel { 
   kDEBUG   = 1,
   kVERBOSE = 2, 
   kINFO    = 3,
   kWARNING = 4,
   kERROR   = 5,
   kFATAL   = 6,
   kALWAYS  = 7
};

class MsgLogger : public std::ostringstream, public TObject {

 public:

   MsgLogger( const TObject* source, MsgLevel minType = kINFO );
   MsgLogger( const std::string& source, MsgLevel minType = kINFO );
   MsgLogger( MsgLevel minType = kINFO );
   MsgLogger( const MsgLogger& parent );
   ~MsgLogger();

   // Accessors
   void        SetSource ( const std::string& source ) { fStrSource = source; }
   MsgLevel    GetMinType()                      const { return fMinType; }
   void        SetMinType( MsgLevel minType )          { fMinType = minType; }
   UInt_t      GetMaxSourceSize()   const              { return (UInt_t)fMaxSourceSize; }
   std::string GetSource()          const              { return fStrSource; }
   std::string GetPrintedSource()   const;
   std::string GetFormattedSource() const;
      
   // Needed for copying
   MsgLogger& operator= ( const MsgLogger& parent );

   // Stream modifier(s)
   static MsgLogger& Endmsg( MsgLogger& logger );
      
   // Accept stream modifiers
   MsgLogger& operator<< ( MsgLogger& ( *_f )( MsgLogger& ) );
   MsgLogger& operator<< ( std::ostream& ( *_f )( std::ostream& ) );
   MsgLogger& operator<< ( std::ios& ( *_f )( std::ios& ) );
      
   // Accept message type specification
   MsgLogger& operator<< ( MsgLevel type );
      
   // For all the "conventional" inputs
   template <class T> MsgLogger& operator<< ( T arg ) {
      *(std::ostringstream*)this << arg;
      return *this;
   }

 private:

   // private utility routines
   void Send();
   void InitMaps();
   void WriteMsg( MsgLevel type, const std::string& line ) const;

   const TObject*                  fObjSource;     // the source TObject (used for name)
   std::string                     fStrSource;     // alternative string source
   const std::string               fPrefix;        // the prefix of the source name
   const std::string               fSuffix;        // suffix following source name
   MsgLevel                        fActiveType;    // active type
   const std::string::size_type    fMaxSourceSize; // maximum length of source name

   std::map<MsgLevel, std::string> fTypeMap;       // matches output types with strings
   std::map<MsgLevel, std::string> fColorMap;      // matches output types with terminal colors
   MsgLevel                        fMinType;       // minimum type for output

   ClassDef(MsgLogger,0); // Ostringstream derivative to redirect and format logging output  

}; // class MsgLogger

inline MsgLogger& MsgLogger::operator<< ( MsgLogger& (*_f)( MsgLogger& ) ) 
{
   return (_f)(*this);
}

inline MsgLogger& MsgLogger::operator<< ( std::ostream& (*_f)( std::ostream& ) ) 
{
   (_f)(*this);
   return *this;
}

inline MsgLogger& MsgLogger::operator<< ( std::ios& ( *_f )( std::ios& ) ) 
{
   (_f)(*this);
   return *this;
}

inline MsgLogger& MsgLogger::operator<< ( MsgLevel type ) 
{
   fActiveType = type;
   return *this;
}

// Although the proper definition of "Endl" as a function pointer
// would be nicer C++-wise, it introduces some "unused variable"
// warnings so let's use the #define definition after all...
//   static MsgLogger& ( *Endl )( MsgLogger& ) = &MsgLogger::Endmsg;
#define Endl MsgLogger::Endmsg

#endif 
