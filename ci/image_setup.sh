#!/bin/bash

# Environment configuration file setting up the installed project.

# Set up the base environment using the base image's setup script:
source /home/atlas/release_setup.sh

# Set up the ToolsJESCombination installation:
export LD_LIBRARY_PATH=/code/lib:/code/src:$LD_LIBRARY_PATH
export PATH=/code/exe:$PATH
echo "Configured ToolsJESCombination from: /code"
