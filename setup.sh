#! /bin/sh

if [ ! -h include ]; then ln -s src include; fi;

# check Root environment setup
if [ ! $ROOTSYS ]; then
  echo "Warning: Root environment (ROOTSYS) not yet defined. Please do so! "
fi

if [ ! $LD_LIBRARY_PATH ]; then
  echo "Warning: so far you haven't setup your ROOT enviroment properly (no LD_LIBRARY_PATH): ToolsJEScombination, derived from HVPTools will not work"
fi

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$HOME/ToolsJEScombination/lib:$PWD/../lib:$PWD/lib
export PATH=${PATH}:$ROOTSYS/bin

