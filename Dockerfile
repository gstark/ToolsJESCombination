# analysis base version from which we start
FROM atlas/analysisbase:21.2.74

# put the code into the repo with the proper owner
ADD . /code/
COPY ci/image_setup.sh /home/atlas/image_setup.sh

# note that the build directory already exists in /code/src
RUN sudo chown -R atlas /code/ && \
    source /home/atlas/release_setup.sh && \
    cd /code/src && \
    make && \
    cd /code/exe && \
    make

# Lines needed to run with reana
#USER root
#RUN sudo usermod -G root atlas
#USER atlas
